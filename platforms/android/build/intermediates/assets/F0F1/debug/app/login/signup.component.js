"use strict";
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var auth_service_1 = require('./auth.service');
var SignUpComponent = (function () {
    function SignUpComponent(router, auth) {
        this.router = router;
        this.auth = auth;
        this.model = {};
        this.loading = false;
        this.passCheck = true;
        this.error = '';
    }
    SignUpComponent.prototype.onKey = function (value) {
        this.passCheck = (this.model.pass == value);
    };
    SignUpComponent.prototype.getUser = function () {
        this.auth.getUser().subscribe(function (result) {
            console.log(result);
        });
    };
    SignUpComponent.prototype.ngOnInit = function () {
        // reset login status
        //this.auth.logout();
        // document.getElementById('login').appendChild(document.getElementById('my-signin3'))
        // document.getElementById('my-signin3').style.display="inline";
    };
    SignUpComponent.prototype.signUp = function () {
        this.loading = true;
        // this.auth.signUp(this.model.username,this.model.email, this.model.pass)
        //     .subscribe(result => {
        //         if (result === true) {
        //             this.router.navigate(['/']);
        //         } else {
        //             this.error = 'Username or password is incorrect';
        //             this.loading = false;
        //         }
        //     },
        //     error => {
        //             this.error = 'Username or password is incorrect';
        //             this.loading = false;
        //         });
    };
    SignUpComponent = __decorate([
        core_1.Component({
            template: "<StackLayout>\n      <TextField hint=\"Email Address\" keyboardType=\"email\"\n        autocorrect=\"false\" autocapitalizationType=\"none\"></TextField>\n      <TextField hint=\"Password\" secure=\"true\"></TextField>\n\n      <Button text=\"Sign in\"></Button>\n      <Button text=\"Sign up for Groceries\"></Button>\n    </StackLayout>",
        }), 
        __metadata('design:paramtypes', [router_1.Router, auth_service_1.AuthService])
    ], SignUpComponent);
    return SignUpComponent;
}());
exports.SignUpComponent = SignUpComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lnbnVwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL2Rldi9sb2dpbi9zaWdudXAuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxxQkFBa0MsZUFBZSxDQUFDLENBQUE7QUFDbEQsdUJBQXVCLGlCQUFpQixDQUFDLENBQUE7QUFDekMsNkJBQTJCLGdCQUczQixDQUFDLENBSDBDO0FBZ0IzQztJQU1JLHlCQUFvQixNQUFjLEVBQVUsSUFBaUI7UUFBekMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUFVLFNBQUksR0FBSixJQUFJLENBQWE7UUFMN0QsVUFBSyxHQUFRLEVBQUUsQ0FBQztRQUNoQixZQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ1QsY0FBUyxHQUFXLElBQUksQ0FBQztRQUNoQyxVQUFLLEdBQUcsRUFBRSxDQUFDO0lBRXFELENBQUM7SUFDakUsK0JBQUssR0FBTCxVQUFNLEtBQWE7UUFDZixJQUFJLENBQUMsU0FBUyxHQUFFLENBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksS0FBSyxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUNELGlDQUFPLEdBQVA7UUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLE1BQU07WUFDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUN2QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDRCxrQ0FBUSxHQUFSO1FBQ0kscUJBQXFCO1FBQ2xCLHFCQUFxQjtRQUN4QixzRkFBc0Y7UUFDdEYsZ0VBQWdFO0lBQ3BFLENBQUM7SUFFRCxnQ0FBTSxHQUFOO1FBQ0ksSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsMEVBQTBFO1FBQzFFLDZCQUE2QjtRQUM3QixpQ0FBaUM7UUFDakMsMkNBQTJDO1FBQzNDLG1CQUFtQjtRQUNuQixnRUFBZ0U7UUFDaEUsb0NBQW9DO1FBQ3BDLFlBQVk7UUFDWixTQUFTO1FBQ1QsaUJBQWlCO1FBQ2pCLGdFQUFnRTtRQUNoRSxvQ0FBb0M7UUFDcEMsY0FBYztJQUNsQixDQUFDO0lBbERMO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxvVkFPSztTQUdsQixDQUFDOzt1QkFBQTtJQXlDRixzQkFBQztBQUFELENBQUMsQUF2Q0QsSUF1Q0M7QUF2Q1ksdUJBQWUsa0JBdUMzQixDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgQXV0aFNlcnZpY2V9IGZyb20gJy4vYXV0aC5zZXJ2aWNlJ1xyXG5cclxuIFxyXG5AQ29tcG9uZW50KHtcclxuICAgIHRlbXBsYXRlOiBgPFN0YWNrTGF5b3V0PlxyXG4gICAgICA8VGV4dEZpZWxkIGhpbnQ9XCJFbWFpbCBBZGRyZXNzXCIga2V5Ym9hcmRUeXBlPVwiZW1haWxcIlxyXG4gICAgICAgIGF1dG9jb3JyZWN0PVwiZmFsc2VcIiBhdXRvY2FwaXRhbGl6YXRpb25UeXBlPVwibm9uZVwiPjwvVGV4dEZpZWxkPlxyXG4gICAgICA8VGV4dEZpZWxkIGhpbnQ9XCJQYXNzd29yZFwiIHNlY3VyZT1cInRydWVcIj48L1RleHRGaWVsZD5cclxuXHJcbiAgICAgIDxCdXR0b24gdGV4dD1cIlNpZ24gaW5cIj48L0J1dHRvbj5cclxuICAgICAgPEJ1dHRvbiB0ZXh0PVwiU2lnbiB1cCBmb3IgR3JvY2VyaWVzXCI+PC9CdXR0b24+XHJcbiAgICA8L1N0YWNrTGF5b3V0PmAsXHJcbiAgICAvLyB0ZW1wbGF0ZVVybDogJy4uLy4uL3ZpZXcvc2lnbnVwLmNvbXBvbmVudC5odG1sJyxcclxuICAgIC8vIHN0eWxlVXJsczogWycuLi8uLi9jc3MvbG9naW4uY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbiBcclxuZXhwb3J0IGNsYXNzIFNpZ25VcENvbXBvbmVudHtcclxuICAgIG1vZGVsOiBhbnkgPSB7fTtcclxuICAgIGxvYWRpbmcgPSBmYWxzZTtcclxuICAgIHB1YmxpYyBwYXNzQ2hlY2s6Ym9vbGVhbiA9IHRydWU7XHJcbiAgICBlcnJvciA9ICcnO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsIHByaXZhdGUgYXV0aDogQXV0aFNlcnZpY2UpIHt9XHJcbiAgICBvbktleSh2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5wYXNzQ2hlY2sgPSggdGhpcy5tb2RlbC5wYXNzID09IHZhbHVlKTtcclxuICAgIH1cclxuICAgIGdldFVzZXIoKSB7XHJcbiAgICAgICAgdGhpcy5hdXRoLmdldFVzZXIoKS5zdWJzY3JpYmUocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2cocmVzdWx0KVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgLy8gcmVzZXQgbG9naW4gc3RhdHVzXHJcbiAgICAgICAgICAgLy90aGlzLmF1dGgubG9nb3V0KCk7XHJcbiAgICAgICAgLy8gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2xvZ2luJykuYXBwZW5kQ2hpbGQoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ215LXNpZ25pbjMnKSlcclxuICAgICAgICAvLyBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbXktc2lnbmluMycpLnN0eWxlLmRpc3BsYXk9XCJpbmxpbmVcIjtcclxuICAgIH1cclxuXHJcbiAgICBzaWduVXAoKSB7XHJcbiAgICAgICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcclxuICAgICAgICAvLyB0aGlzLmF1dGguc2lnblVwKHRoaXMubW9kZWwudXNlcm5hbWUsdGhpcy5tb2RlbC5lbWFpbCwgdGhpcy5tb2RlbC5wYXNzKVxyXG4gICAgICAgIC8vICAgICAuc3Vic2NyaWJlKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgLy8gICAgICAgICBpZiAocmVzdWx0ID09PSB0cnVlKSB7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvJ10pO1xyXG4gICAgICAgIC8vICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAvLyAgICAgICAgICAgICB0aGlzLmVycm9yID0gJ1VzZXJuYW1lIG9yIHBhc3N3b3JkIGlzIGluY29ycmVjdCc7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgICAgLy8gICAgICAgICB9XHJcbiAgICAgICAgLy8gICAgIH0sXHJcbiAgICAgICAgLy8gICAgIGVycm9yID0+IHtcclxuICAgICAgICAvLyAgICAgICAgICAgICB0aGlzLmVycm9yID0gJ1VzZXJuYW1lIG9yIHBhc3N3b3JkIGlzIGluY29ycmVjdCc7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgICAgLy8gICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbn0iXX0=