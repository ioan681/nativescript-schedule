"use strict";
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
// import { AuthGuard } from './login/auth.guard';
var login_component_1 = require('./login/login.component');
var signup_component_1 = require("./login/signup.component");
var appRoutes = [
    { path: 'login', component: login_component_1.LoginComponent },
    { path: 'signup', component: signup_component_1.SignUpComponent },
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.NativeScriptRouterModule.forRoot(appRoutes)
            ],
            exports: [
                router_1.NativeScriptRouterModule
            ],
            providers: []
        }), 
        __metadata('design:paramtypes', [])
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLXJvdXRpbmcubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vZGV2L2FwcC1yb3V0aW5nLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEscUJBQTJDLGVBQWUsQ0FBQyxDQUFBO0FBQzNELHVCQUF5Qyw2QkFBNkIsQ0FBQyxDQUFBO0FBQ3ZFLGtEQUFrRDtBQUNsRCxnQ0FBK0IseUJBQXlCLENBQUMsQ0FBQTtBQUN6RCxpQ0FBZ0MsMEJBQTBCLENBQUMsQ0FBQTtBQUUzRCxJQUFNLFNBQVMsR0FBRztJQUNqQixFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLGdDQUFjLEVBQUU7SUFDNUMsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxrQ0FBZSxFQUFFO0NBSzlDLENBQUM7QUFZRjtJQUFBO0lBQStCLENBQUM7SUFWaEM7UUFBQyxlQUFRLENBQUM7WUFDUixPQUFPLEVBQUU7Z0JBQ1YsaUNBQXdCLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQzthQUN6QztZQUNELE9BQU8sRUFBRTtnQkFDUCxpQ0FBd0I7YUFDekI7WUFDRCxTQUFTLEVBQUUsRUFDVjtTQUNGLENBQUM7O3dCQUFBO0lBQzZCLHVCQUFDO0FBQUQsQ0FBQyxBQUFoQyxJQUFnQztBQUFuQix3QkFBZ0IsbUJBQUcsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBOT19FUlJPUlNfU0NIRU1BIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG4vLyBpbXBvcnQgeyBBdXRoR3VhcmQgfSBmcm9tICcuL2xvZ2luL2F1dGguZ3VhcmQnO1xyXG5pbXBvcnQgeyBMb2dpbkNvbXBvbmVudCB9IGZyb20gJy4vbG9naW4vbG9naW4uY29tcG9uZW50JztcclxuaW1wb3J0IHsgU2lnblVwQ29tcG9uZW50IH0gZnJvbSBcIi4vbG9naW4vc2lnbnVwLmNvbXBvbmVudFwiO1xyXG5cclxuY29uc3QgYXBwUm91dGVzID0gW1xyXG5cdHsgcGF0aDogJ2xvZ2luJywgY29tcG9uZW50OiBMb2dpbkNvbXBvbmVudCB9LFxyXG5cdHsgcGF0aDogJ3NpZ251cCcsIGNvbXBvbmVudDogU2lnblVwQ29tcG9uZW50IH0sXHJcbiAgLy97IHBhdGg6ICcnLCBjb21wb25lbnQ6IE1hcENvbXBvbmVudCB9LFxyXG4gIC8vIHsgcGF0aDogJycsIGNvbXBvbmVudDogTG9naW5Db21wb25lbnQgfSxcclxuICAvL3sgcGF0aDogJyoqJywgcmVkaXJlY3RUbzogJy9sb2dpbicgfVxyXG4gIFxyXG5dO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBpbXBvcnRzOiBbXHJcblx0TmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlLmZvclJvb3QoYXBwUm91dGVzKVxyXG4gIF0sXHJcbiAgZXhwb3J0czogW1xyXG4gICAgTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlXHJcbiAgXSxcclxuICBwcm92aWRlcnM6IFtcclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBcHBSb3V0aW5nTW9kdWxlIHt9Il19