"use strict";
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var gestures_1 = require("ui/gestures");
var grid_layout_1 = require("ui/layouts/grid-layout");
var calendar_service_1 = require('./calendar.service');
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var modal_component_1 = require("./modal.component");
var page_1 = require("ui/page");
var label_1 = require("ui/label");
var enums_1 = require("ui/enums");
var WeekComponent = (function () {
    function WeekComponent(route, appoint, _modalService, vcRef, page) {
        this.route = route;
        this.appoint = appoint;
        this._modalService = _modalService;
        this.vcRef = vcRef;
        this.page = page;
        this.target = new label_1.Label;
        this.save = new label_1.Label;
        this.month2 = "January";
        this.state = 0;
    }
    WeekComponent.prototype.ngOnInit = function () {
        //console.log(document.getElementById("side-container"))
        this.cont = this.container.nativeElement;
        this.text = this.title.nativeElement;
        this.hour = new grid_layout_1.GridLayout();
        for (var i = 0; i < 7; i += 1) {
            var secondColumn = new grid_layout_1.ItemSpec(1, grid_layout_1.GridUnitType.star);
            this.hour.addColumn(secondColumn);
            var td2 = this.hours();
            grid_layout_1.GridLayout.setColumn(td2, i);
            this.hour.addChild(td2);
        }
        this.Create(0);
    };
    WeekComponent.prototype.Days = function () {
        var mon;
        var date = new Date();
        this.month = date.getMonth() + 1;
        this.year = date.getFullYear();
        var day = date.getDay();
        var today = date.getDate();
        if (today - day < 0) {
            if (this.month == 1) {
                this.month = 12;
                this.year -= 1;
            }
            else {
                this.month -= 1;
            }
            var obj = { mon: 0 };
            this.GetMonth(obj);
            this.lastday = obj.mon + today - day;
        }
        else
            this.lastday = today - day;
        return this.Week();
    };
    WeekComponent.prototype.Week = function () {
        var obj = { mon: 0 };
        this.GetMonth(obj);
        var j = this.lastday;
        var days = new Array;
        if (this.lastday - 7 < 0) {
            if (this.month == 1) {
                this.month = 12;
                this.year -= 1;
            }
            else
                this.month -= 1;
        }
        for (var i = 0; i < 7; i += 1) {
            if (j == obj.mon)
                j = 0;
            j += 1;
            days.push(j);
        }
        this.lastday = j;
        if (days[0] < days[6] && days[0] - 7 <= 0) {
            if (this.month == 12) {
                this.month = 1;
                this.year += 1;
            }
            else
                this.month += 1;
        }
        this.Month();
        if (days[0] > days[6] && days[6] - 7 <= 0) {
            if (this.month == 12) {
                this.month = 1;
                this.year += 1;
            }
            else
                this.month += 1;
        }
        return days;
    };
    WeekComponent.prototype.Week1 = function () {
        var obj = { mon: 0 };
        if (this.lastday - 14 >= 0) {
            this.lastday -= 14;
            this.GetMonth(obj);
        }
        else {
            if (this.month == 1) {
                this.month = 12;
                this.year -= 1;
                this.GetMonth(obj);
            }
            else {
                this.month -= 1;
                this.GetMonth(obj);
            }
            this.lastday = (this.lastday - 14 + obj.mon);
        }
        var days = new Array;
        var j = this.lastday;
        for (var i = 0; i < 7; i += 1) {
            if (j == obj.mon)
                j = 0;
            j += 1;
            days.push(j);
        }
        this.lastday = j;
        if (this.lastday - 7 < 0) {
            if (this.month == 12) {
                this.month = 1;
                this.year += 1;
            }
            else
                this.month += 1;
        }
        this.Month();
        return days;
    };
    WeekComponent.prototype.GetMonth = function (obj) {
        if (this.month <= 7) {
            if (this.month % 2 == 1)
                obj.mon = 31;
            else
                obj.mon = 30;
            if (this.month == 2) {
                if (((this.year % 4 == 0) && (this.year % 100 != 0))
                    || (this.year % 400 == 0))
                    obj.mon = 29;
                else
                    obj.mon = 28;
            }
        }
        else {
            if (this.month % 2 == 1)
                obj.mon = 30;
            else
                obj.mon = 31;
        }
    };
    WeekComponent.prototype.Create = function (n) {
        var _this = this;
        this.cont2 = new grid_layout_1.GridLayout();
        this.cont2.style.width = "100%";
        this.cont2.style.borderRightColor = "#e7e7e7";
        this.cont2.style.borderWidth = "1px";
        this.cont2.style.backgroundColor = "white";
        var week;
        switch (n) {
            case -1:
                week = this.Week1();
                break;
            case 0:
                week = this.Days();
                break;
            case 1:
                week = this.Week();
                break;
        }
        var grid = this.hour;
        grid.id = "contt";
        grid.style.marginTop = 40;
        var i = 0;
        var days = new grid_layout_1.GridLayout();
        week.forEach(function (element) {
            var secondColumn = new grid_layout_1.ItemSpec(1, grid_layout_1.GridUnitType.star);
            days.addColumn(secondColumn);
            var td = new label_1.Label();
            td.text = element;
            td.className = "day-week";
            grid_layout_1.GridLayout.setColumn(td, i);
            days.addChild(td);
            i += 1;
        });
        days.id = "days";
        this.cont2.addChild(days);
        this.cont2.addChild(grid);
        if (this.state == 1)
            this.cont2.translateX = -1000;
        if (this.state == 2)
            this.cont2.translateX = 1000;
        this.cont.addChild(this.cont2);
        this.showAttach(week[0]);
        this.cont2.animate({
            translate: { x: 0, y: 0 },
            duration: 800,
            curve: enums_1.AnimationCurve.easeOut
        });
        this.hour = new grid_layout_1.GridLayout();
        for (var i_1 = 0; i_1 < 7; i_1 += 1) {
            var secondColumn = new grid_layout_1.ItemSpec(1, grid_layout_1.GridUnitType.star);
            this.hour.addColumn(secondColumn);
            var td2 = this.hours();
            grid_layout_1.GridLayout.setColumn(td2, i_1);
            this.hour.addChild(td2);
        }
        this.cont.on(gestures_1.GestureTypes.swipe, function (args) {
            console.log("Swipe Direction: " + args.direction);
            if (args.direction == 2) {
                _this.Next();
            }
            if (args.direction == 1) {
                _this.Prev();
            }
        });
    };
    WeekComponent.prototype.showAttach = function (day) {
        var _this = this;
        var date = this.year + "-" + this.month + "-" + day;
        var arr = new Array;
        this.appoint.getAppointment(date).subscribe(function (result) {
            if (result.length > 0) {
                var grandParent = _this.cont2.getChildById('contt');
                // for(let da in grandParent){
                //    console.log(da)
                // }
                arr = result;
                var d = void 0, parent_1, children = void 0, start = void 0, end = void 0, i = void 0, label = void 0, time = void 0;
                for (var key in arr) {
                    d = arr[key]["date"].split("-");
                    d = parseInt(d[2]) - day;
                    parent_1 = grandParent.getChildAt(d);
                    start = arr[key]["start"].split(":");
                    start = parseInt(start[0]);
                    parent_1.removeChild(parent_1.getChildById(start));
                    label = new label_1.Label();
                    label.text = arr[key]["name"] + "\r\n" + arr[key]["start"].substring(0, 5);
                    label.textWrap = true;
                    label.backgroundColor = arr[key]["color"];
                    label.className = "hour-btn attachment-btn";
                    grid_layout_1.GridLayout.setRow(label, start);
                    parent_1.addChild(label);
                }
            }
            else
                console.log("not");
        }, function (error) {
            console.log(error);
        });
    };
    WeekComponent.prototype.hours = function () {
        var _this = this;
        var td = new grid_layout_1.GridLayout();
        var label;
        for (var i = 0; i < 24; i++) {
            var secondRow = new grid_layout_1.ItemSpec(1, grid_layout_1.GridUnitType.auto);
            td.addRow(secondRow);
            label = new label_1.Label();
            label.className = "hour-btn";
            if (i < 10)
                label.text = "0" + i + ":30";
            else
                label.text = i + ":30";
            label.id = i;
            label.on(gestures_1.GestureTypes.tap, function (args) {
                _this.clicked(args.object);
            });
            label.on(gestures_1.GestureTypes.swipe, function (args) {
                console.log("Swipe Direction: " + args.direction);
                if (args.direction == 2) {
                    _this.Next();
                }
                if (args.direction == 1) {
                    _this.Prev();
                }
            });
            //label.style.paddingTop = 20; 
            grid_layout_1.GridLayout.setRow(label, i);
            td.addChild(label);
        }
        return td;
    };
    WeekComponent.prototype.Prev = function () {
        this.state = 1;
        this.Create(-1);
    };
    WeekComponent.prototype.Next = function () {
        this.state = 2;
        this.Create(1);
    };
    WeekComponent.prototype.Month = function () {
        var _this = this;
        switch (this.month) {
            case 1:
                this.month2 = "January";
                break;
            case 2:
                this.month2 = "February";
                break;
            case 3:
                this.month2 = "March";
                break;
            case 4:
                this.month2 = "April";
                break;
            case 5:
                this.month2 = "May";
                break;
            case 6:
                this.month2 = "June";
                break;
            case 7:
                this.month2 = "July";
                break;
            case 8:
                this.month2 = "August";
                break;
            case 9:
                this.month2 = "September";
                break;
            case 10:
                this.month2 = "October";
                break;
            case 11:
                this.month2 = "November";
                break;
            case 12:
                this.month2 = "December";
                break;
        }
        this.year2 = this.year;
        setTimeout(function () {
            _this.text.text = _this.month2;
        }, 300);
    };
    WeekComponent.prototype.clicked = function (event) {
        var _this = this;
        if (event == this.target) {
            var obj = {
                start: this.save.text,
                vcRef: this.vcRef
            };
            var options = {
                viewContainerRef: this.vcRef,
                context: obj,
                fullscreen: true
            };
            this._modalService.showModal(modal_component_1.ModalViewComponent, options)
                .then(function (appointment) {
                if (appointment != undefined) {
                    _this.Appointment(appointment);
                }
            });
        }
        else {
            this.target.text = this.save.text;
            this.target.className = this.save.className;
            this.save.text = event.text;
            this.save.className = event.className;
            this.target = event;
            event.className = "hour-btn new-attach";
            event.text = "+";
        }
    };
    WeekComponent.prototype.Appointment = function (obj) {
        var _this = this;
        var grandParent = this.cont2.getChildById('contt');
        var parent = this.target.parent;
        var days = this.page.getViewById("days");
        var label = days.getChildAt(grandParent.getChildIndex(parent));
        var color = obj.color["_hex"];
        var date = this.year + "-" + this.month + "-" + label.text;
        var name = obj.name;
        if (obj.name != "") {
            var from = obj.from;
            var to_1 = obj.to;
            this.appoint.postAppointment(obj.name, obj.from, obj.to, date, color).subscribe(function (result) {
                if (result === true) {
                    var i = 1;
                    var val = parseInt(to_1.value);
                    var remove = void 0;
                    parent.removeChild(_this.target);
                    // while(i){
                    // 	remove = this.target.nextSibling;
                    // 	if(val > parseInt(remove.value)){
                    // 	i+=1; 
                    // 	this.target.parentElement.removeChild(remove);
                    // 	}
                    // 	else break;
                    // };
                    label = new label_1.Label();
                    label.text = obj["name"] + "\r\n" + obj["from"].substring(0, 5);
                    label.textWrap = true;
                    label.backgroundColor = obj["color"];
                    label.className = "hour-btn attachment-btn";
                    var val2 = obj["from"].split(":");
                    var start = (parseInt(val2[0]));
                    grid_layout_1.GridLayout.setRow(label, start);
                    parent.addChild(label);
                }
                else {
                    console.error("Something went wrong");
                }
            });
        }
    };
    __decorate([
        core_1.ViewChild("title"), 
        __metadata('design:type', core_1.ElementRef)
    ], WeekComponent.prototype, "title", void 0);
    __decorate([
        core_1.ViewChild("container"), 
        __metadata('design:type', core_1.ElementRef)
    ], WeekComponent.prototype, "container", void 0);
    WeekComponent = __decorate([
        core_1.Component({
            selector: 'year-calendar',
            template: "\n\t<ActionBar title=\"\">\n\t\t<Label text=\"tralala\" #title></Label>\n\t</ActionBar>\n\t<AbsoluteLayout>\n\t\t<ScrollView orientation=\"vertical\" width=\"100%\" height=\"99%\">\n\t\t\t<AbsoluteLayout style=\"width:100%;height:1200;\" #container></AbsoluteLayout>\n\t\t</ScrollView>\n\t</AbsoluteLayout>\n    ",
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, calendar_service_1.CalendarService, modal_dialog_1.ModalDialogService, core_1.ViewContainerRef, page_1.Page])
    ], WeekComponent);
    return WeekComponent;
}());
exports.WeekComponent = WeekComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2Vlay5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9kZXYvQ2FsZW5kYXIvd2Vlay5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHFCQUMyRCxlQUFlLENBQUMsQ0FBQTtBQUMzRSx1QkFBdUMsaUJBQWlCLENBQUMsQ0FBQTtBQUN6RCx5QkFBc0UsYUFBYSxDQUFDLENBQUE7QUFDcEYsNEJBQWtELHdCQUF3QixDQUFDLENBQUE7QUFFM0UsaUNBQWdDLG9CQUFvQixDQUFDLENBQUE7QUFDckQsNkJBQXVELG1DQUFtQyxDQUFDLENBQUE7QUFDM0YsZ0NBQW1DLG1CQUFtQixDQUFDLENBQUE7QUFDdkQscUJBQWlDLFNBQVMsQ0FBQyxDQUFBO0FBQzNDLHNCQUFzQixVQUFVLENBQUMsQ0FBQTtBQUNqQyxzQkFBK0IsVUFBVSxDQUFDLENBQUE7QUFpQjFDO0lBZ0JDLHVCQUFvQixLQUFxQixFQUFTLE9BQXdCLEVBQ2xFLGFBQWlDLEVBQVMsS0FBdUIsRUFBVSxJQUFVO1FBRHpFLFVBQUssR0FBTCxLQUFLLENBQWdCO1FBQVMsWUFBTyxHQUFQLE9BQU8sQ0FBaUI7UUFDbEUsa0JBQWEsR0FBYixhQUFhLENBQW9CO1FBQVMsVUFBSyxHQUFMLEtBQUssQ0FBa0I7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFNO1FBaEJyRixXQUFNLEdBQUcsSUFBSSxhQUFLLENBQUM7UUFDbkIsU0FBSSxHQUFHLElBQUksYUFBSyxDQUFDO1FBT2xCLFdBQU0sR0FBRyxTQUFTLENBQUM7UUFDbkIsVUFBSyxHQUFVLENBQUMsQ0FBQztJQU95RSxDQUFDO0lBQ2xHLGdDQUFRLEdBQVI7UUFDQyx3REFBd0Q7UUFDeEQsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQztRQUN2QyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSx3QkFBVSxFQUFFLENBQUM7UUFDL0IsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFDLENBQUMsR0FBQyxDQUFDLEVBQUMsQ0FBQyxJQUFFLENBQUMsRUFBQyxDQUFDO1lBQ2xCLElBQUksWUFBWSxHQUFHLElBQUksc0JBQVEsQ0FBQyxDQUFDLEVBQUUsMEJBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0RCxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUNyQyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDdkIsd0JBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZCLENBQUM7UUFFSCxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ2hCLENBQUM7SUFDRCw0QkFBSSxHQUFKO1FBQ0MsSUFBSSxHQUFVLENBQUM7UUFDZixJQUFJLElBQUksR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxHQUFDLENBQUMsQ0FBQztRQUMvQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUMvQixJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDeEIsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzNCLEVBQUUsQ0FBQSxDQUFDLEtBQUssR0FBQyxHQUFHLEdBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNoQixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFBLENBQUM7Z0JBQ25CLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO2dCQUNoQixJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQztZQUNoQixDQUFDO1lBQ0QsSUFBSSxDQUFBLENBQUM7Z0JBQ0osSUFBSSxDQUFDLEtBQUssSUFBRyxDQUFDLENBQUM7WUFDaEIsQ0FBQztZQUNELElBQUksR0FBRyxHQUFHLEVBQUMsR0FBRyxFQUFFLENBQUMsRUFBQyxDQUFBO1lBQ2xCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDbkIsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsR0FBRyxHQUFHLEtBQUssR0FBRyxHQUFHLENBQUM7UUFDdEMsQ0FBQztRQUNELElBQUk7WUFBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssR0FBQyxHQUFHLENBQUM7UUFDOUIsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBQ0QsNEJBQUksR0FBSjtRQUNDLElBQUksR0FBRyxHQUFHLEVBQUMsR0FBRyxFQUFFLENBQUMsRUFBQyxDQUFBO1FBQ2xCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbkIsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUNyQixJQUFJLElBQUksR0FBRyxJQUFJLEtBQUssQ0FBQztRQUNyQixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQ3RCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUEsQ0FBQztnQkFBQSxJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztnQkFBQyxJQUFJLENBQUMsSUFBSSxJQUFHLENBQUMsQ0FBQztZQUFBLENBQUM7WUFDcEQsSUFBSTtnQkFBQyxJQUFJLENBQUMsS0FBSyxJQUFHLENBQUMsQ0FBQztRQUNyQixDQUFDO1FBQ0QsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FBQyxJQUFFLENBQUMsRUFBRSxDQUFDO1lBQzVCLEVBQUUsQ0FBQSxDQUFDLENBQUMsSUFBSSxHQUFHLENBQUMsR0FBRyxDQUFDO2dCQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDdkIsQ0FBQyxJQUFHLENBQUMsQ0FBQztZQUNOLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDYixDQUFDO1FBQ0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFDakIsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUMsQ0FBQyxJQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDckMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUMsQ0FBQSxDQUFDO2dCQUFBLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO2dCQUFDLElBQUksQ0FBQyxJQUFJLElBQUcsQ0FBQyxDQUFDO1lBQUEsQ0FBQztZQUNwRCxJQUFJO2dCQUFDLElBQUksQ0FBQyxLQUFLLElBQUcsQ0FBQyxDQUFDO1FBQ3JCLENBQUM7UUFDRCxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDYixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBQyxDQUFDLElBQUUsQ0FBQyxDQUFDLENBQUEsQ0FBQztZQUNyQyxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQyxDQUFBLENBQUM7Z0JBQUEsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7Z0JBQUMsSUFBSSxDQUFDLElBQUksSUFBRyxDQUFDLENBQUM7WUFBQSxDQUFDO1lBQ3BELElBQUk7Z0JBQUMsSUFBSSxDQUFDLEtBQUssSUFBRyxDQUFDLENBQUM7UUFDckIsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDYixDQUFDO0lBQ0QsNkJBQUssR0FBTDtRQUNDLElBQUksR0FBRyxHQUFHLEVBQUMsR0FBRyxFQUFFLENBQUMsRUFBQyxDQUFBO1FBQ2xCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDMUIsSUFBSSxDQUFDLE9BQU8sSUFBRyxFQUFFLENBQUM7WUFDbEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNwQixDQUFDO1FBQ0QsSUFBSSxDQUFDLENBQUM7WUFDTCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFBLENBQUM7Z0JBQ25CLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO2dCQUNoQixJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQztnQkFDZixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BCLENBQUM7WUFDRCxJQUFJLENBQUEsQ0FBQztnQkFDSixJQUFJLENBQUMsS0FBSyxJQUFHLENBQUMsQ0FBQztnQkFDZixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BCLENBQUM7WUFDRCxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzlDLENBQUM7UUFDRCxJQUFJLElBQUksR0FBRyxJQUFJLEtBQUssQ0FBQztRQUNyQixJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ3JCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQUMsSUFBRSxDQUFDLEVBQUUsQ0FBQztZQUM1QixFQUFFLENBQUEsQ0FBQyxDQUFDLElBQUksR0FBRyxDQUFDLEdBQUcsQ0FBQztnQkFBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3ZCLENBQUMsSUFBRyxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ2IsQ0FBQztRQUNELElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDeEIsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUMsQ0FBQSxDQUFDO2dCQUNwQixJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztnQkFDZixJQUFJLENBQUMsSUFBSSxJQUFHLENBQUMsQ0FBQztZQUNmLENBQUM7WUFDRCxJQUFJO2dCQUFDLElBQUksQ0FBQyxLQUFLLElBQUcsQ0FBQyxDQUFDO1FBQ3JCLENBQUM7UUFDRCxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDYixNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2IsQ0FBQztJQUNELGdDQUFRLEdBQVIsVUFBUyxHQUFRO1FBQ2hCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUEsQ0FBQztZQUNuQixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUM7WUFDckMsSUFBSTtnQkFBQyxHQUFHLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQztZQUNsQixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFBLENBQUM7Z0JBQ25CLEVBQUUsQ0FBQSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDO3VCQUMvQyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUFBLEdBQUcsQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDO2dCQUN4QyxJQUFJO29CQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDO1lBQ25CLENBQUM7UUFDRixDQUFDO1FBQ0QsSUFBSSxDQUFBLENBQUM7WUFDSixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUM7WUFDckMsSUFBSTtnQkFBQyxHQUFHLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQztRQUNuQixDQUFDO0lBQ0YsQ0FBQztJQUNELDhCQUFNLEdBQU4sVUFBTyxDQUFNO1FBQWIsaUJBeURDO1FBeERBLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSx3QkFBVSxFQUFFLENBQUM7UUFDN0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQztRQUNqQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxTQUFTLENBQUM7UUFDOUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUNuQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsT0FBTyxDQUFBO1FBQzVDLElBQUksSUFBUSxDQUFDO1FBQ2IsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNYLEtBQUssQ0FBQyxDQUFDO2dCQUFDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQUEsS0FBSyxDQUFDO1lBQ2xDLEtBQUssQ0FBQztnQkFBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUFBLEtBQUssQ0FBQztZQUNoQyxLQUFLLENBQUM7Z0JBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFBQSxLQUFLLENBQUM7UUFDakMsQ0FBQztRQUNELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDbkIsSUFBSSxDQUFDLEVBQUUsR0FBRyxPQUFPLENBQUE7UUFDakIsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO1FBQzVCLElBQUksQ0FBQyxHQUFDLENBQUMsQ0FBQztRQUNOLElBQUksSUFBSSxHQUFHLElBQUksd0JBQVUsRUFBRSxDQUFDO1FBQzlCLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQyxPQUFXO1lBQ3JCLElBQUksWUFBWSxHQUFHLElBQUksc0JBQVEsQ0FBQyxDQUFDLEVBQUUsMEJBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0RCxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzdCLElBQUksRUFBRSxHQUFHLElBQUksYUFBSyxFQUFFLENBQUM7WUFDeEIsRUFBRSxDQUFDLElBQUksR0FBRyxPQUFPLENBQUM7WUFDZixFQUFFLENBQUMsU0FBUyxHQUFHLFVBQVUsQ0FBQztZQUMxQix3QkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDNUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNyQixDQUFDLElBQUUsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSixJQUFJLENBQUMsRUFBRSxHQUFDLE1BQU0sQ0FBQztRQUNiLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDO1lBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsQ0FBQyxJQUFJLENBQUM7UUFDbEQsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUM7WUFBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFDakQsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDekIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7WUFDakIsU0FBUyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFDO1lBQ3hCLFFBQVEsRUFBRSxHQUFHO1lBQ2IsS0FBSyxFQUFFLHNCQUFjLENBQUMsT0FBTztTQUM5QixDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksd0JBQVUsRUFBRSxDQUFDO1FBQy9CLEdBQUcsQ0FBQSxDQUFDLElBQUksR0FBQyxHQUFDLENBQUMsRUFBQyxHQUFDLEdBQUMsQ0FBQyxFQUFDLEdBQUMsSUFBRSxDQUFDLEVBQUMsQ0FBQztZQUNsQixJQUFJLFlBQVksR0FBRyxJQUFJLHNCQUFRLENBQUMsQ0FBQyxFQUFFLDBCQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdEQsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDckMsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ3ZCLHdCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRSxHQUFDLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN2QixDQUFDO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsdUJBQVksQ0FBQyxLQUFLLEVBQUMsVUFBQyxJQUEyQjtZQUM3RCxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNqRCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7Z0JBQ3JCLEtBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNiLENBQUM7WUFDRCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7Z0JBQ3JCLEtBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNiLENBQUM7UUFDRixDQUFDLENBQUMsQ0FBQztJQUVMLENBQUM7SUFDQSxrQ0FBVSxHQUFWLFVBQVcsR0FBVztRQUF0QixpQkFpQ0E7UUFoQ0EsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBQyxHQUFHLEdBQUMsSUFBSSxDQUFDLEtBQUssR0FBQyxHQUFHLEdBQUMsR0FBRyxDQUFDO1FBQzVDLElBQUksR0FBRyxHQUFHLElBQUksS0FBSyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FDMUMsVUFBQSxNQUFNO1lBQ0wsRUFBRSxDQUFBLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsQ0FBQSxDQUFDO2dCQUNkLElBQUksV0FBVyxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNuRCw4QkFBOEI7Z0JBQzlCLHFCQUFxQjtnQkFDckIsSUFBSTtnQkFDVCxHQUFHLEdBQUcsTUFBTSxDQUFDO2dCQUNiLElBQUksQ0FBQyxTQUFPLEVBQUMsUUFBVSxFQUFDLFFBQVEsU0FBSSxFQUFDLEtBQUssU0FBSSxFQUFDLEdBQUcsU0FBSSxFQUFDLENBQUMsU0FBSSxFQUFDLEtBQUssU0FBSSxFQUFDLElBQUksU0FBTyxDQUFDO2dCQUM5RSxHQUFHLENBQUEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsQ0FBQSxDQUFDO29CQUNsQixDQUFDLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDdEMsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBQyxHQUFHLENBQUM7b0JBQ2pCLFFBQU0sR0FBRyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFBO29CQUNsQyxLQUFLLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDM0MsS0FBSyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDckIsUUFBTSxDQUFDLFdBQVcsQ0FBQyxRQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7b0JBQy9DLEtBQUssR0FBRyxJQUFJLGFBQUssRUFBRSxDQUFDO29CQUNwQixLQUFLLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBQyxNQUFNLEdBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ3ZFLEtBQUssQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO29CQUN0QixLQUFLLENBQUMsZUFBZSxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDMUMsS0FBSyxDQUFDLFNBQVMsR0FBRyx5QkFBeUIsQ0FBQztvQkFDNUMsd0JBQVUsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUNuQyxRQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN0QixDQUFDO1lBQ1AsQ0FBQztZQUNHLElBQUk7Z0JBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUM1QixDQUFDLEVBQ0UsVUFBQSxLQUFLO1lBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUNwQixDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7SUFDQSw2QkFBSyxHQUFMO1FBQUEsaUJBNEJDO1FBM0JELElBQUksRUFBRSxHQUFHLElBQUksd0JBQVUsRUFBRSxDQUFDO1FBQzFCLElBQUksS0FBUyxDQUFDO1FBQ2QsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFDLENBQUMsR0FBQyxFQUFFLEVBQUMsQ0FBQyxFQUFFLEVBQUMsQ0FBQztZQUNyQixJQUFJLFNBQVMsR0FBRyxJQUFJLHNCQUFRLENBQUMsQ0FBQyxFQUFFLDBCQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbkQsRUFBRSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNyQixLQUFLLEdBQUcsSUFBSSxhQUFLLEVBQUUsQ0FBQztZQUNwQixLQUFLLENBQUMsU0FBUyxHQUFHLFVBQVUsQ0FBQztZQUM3QixFQUFFLENBQUEsQ0FBQyxDQUFDLEdBQUMsRUFBRSxDQUFDO2dCQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsR0FBRyxHQUFDLENBQUMsR0FBQyxLQUFLLENBQUM7WUFDbEMsSUFBSTtnQkFBQyxLQUFLLENBQUMsSUFBSSxHQUFHLENBQUMsR0FBQyxLQUFLLENBQUM7WUFDMUIsS0FBSyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDVixLQUFLLENBQUMsRUFBRSxDQUFDLHVCQUFZLENBQUMsR0FBRyxFQUFHLFVBQUMsSUFBc0I7Z0JBQ2pELEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzVCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsS0FBSyxDQUFDLEVBQUUsQ0FBQyx1QkFBWSxDQUFDLEtBQUssRUFBQyxVQUFDLElBQTJCO2dCQUMzRCxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDakQsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBRSxDQUFDLENBQUMsQ0FBQSxDQUFDO29CQUNyQixLQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ2IsQ0FBQztnQkFDRCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7b0JBQ3JCLEtBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDYixDQUFDO1lBQ0YsQ0FBQyxDQUFDLENBQUM7WUFDSCwrQkFBK0I7WUFDL0Isd0JBQVUsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzVCLEVBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDcEIsQ0FBQztRQUNELE1BQU0sQ0FBQyxFQUFFLENBQUM7SUFDVixDQUFDO0lBQ0YsNEJBQUksR0FBSjtRQUNHLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNqQixDQUFDO0lBQ0QsNEJBQUksR0FBSjtRQUNHLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDaEIsQ0FBQztJQUNELDZCQUFLLEdBQUw7UUFBQSxpQkFtQkM7UUFsQkEsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDcEIsS0FBSyxDQUFDO2dCQUFFLElBQUksQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFDO2dCQUFBLEtBQUssQ0FBQztZQUN0QyxLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUM7Z0JBQUEsS0FBSyxDQUFDO1lBQ3ZDLEtBQUssQ0FBQztnQkFBRSxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQztnQkFBQSxLQUFLLENBQUM7WUFDcEMsS0FBSyxDQUFDO2dCQUFFLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDO2dCQUFBLEtBQUssQ0FBQztZQUNwQyxLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7Z0JBQUEsS0FBSyxDQUFDO1lBQ2xDLEtBQUssQ0FBQztnQkFBRSxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztnQkFBQSxLQUFLLENBQUM7WUFDbkMsS0FBSyxDQUFDO2dCQUFFLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO2dCQUFBLEtBQUssQ0FBQztZQUNuQyxLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUM7Z0JBQUEsS0FBSyxDQUFDO1lBQ3JDLEtBQUssQ0FBQztnQkFBRSxJQUFJLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQztnQkFBQSxLQUFLLENBQUM7WUFDeEMsS0FBSyxFQUFFO2dCQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFDO2dCQUFBLEtBQUssQ0FBQztZQUN0QyxLQUFLLEVBQUU7Z0JBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUM7Z0JBQUEsS0FBSyxDQUFDO1lBQ3ZDLEtBQUssRUFBRTtnQkFBQyxJQUFJLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQztnQkFBQSxLQUFLLENBQUM7UUFDeEMsQ0FBQztRQUNELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztRQUNyQixVQUFVLENBQUM7WUFDVixLQUFJLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFBO1FBQzdCLENBQUMsRUFBQyxHQUFHLENBQUMsQ0FBQTtJQUNULENBQUM7SUFDQSwrQkFBTyxHQUFQLFVBQVEsS0FBVTtRQUFsQixpQkE0QkM7UUEzQkMsRUFBRSxDQUFBLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQSxDQUFDO1lBQzFCLElBQUksR0FBRyxHQUFHO2dCQUNULEtBQUssRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUk7Z0JBQ3JCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSzthQUNqQixDQUFBO1lBQ0QsSUFBSSxPQUFPLEdBQXVCO2dCQUM5QixnQkFBZ0IsRUFBRSxJQUFJLENBQUMsS0FBSztnQkFDNUIsT0FBTyxFQUFFLEdBQUc7Z0JBQ1osVUFBVSxFQUFFLElBQUk7YUFDakIsQ0FBQztZQUNKLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLG9DQUFrQixFQUFFLE9BQU8sQ0FBQztpQkFDeEQsSUFBSSxDQUFDLFVBQUMsV0FBZ0I7Z0JBQ3JCLEVBQUUsQ0FBQSxDQUFDLFdBQVcsSUFBSSxTQUFTLENBQUMsQ0FBQSxDQUFDO29CQUM1QixLQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUMvQixDQUFDO1lBQ0QsQ0FBQyxDQUFDLENBQUM7UUFFTixDQUFDO1FBQ0QsSUFBSSxDQUFBLENBQUM7WUFDSixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUNsQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQTtZQUMzQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO1lBQzVCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUM7WUFDdEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDcEIsS0FBSyxDQUFDLFNBQVMsR0FBRyxxQkFBcUIsQ0FBQztZQUN4QyxLQUFLLENBQUMsSUFBSSxHQUFDLEdBQUcsQ0FBQTtRQUNmLENBQUM7SUFDRCxDQUFDO0lBQ0QsbUNBQVcsR0FBWCxVQUFZLEdBQU87UUFBbkIsaUJBeUNDO1FBeENELElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFBO1FBQ2xELElBQUksTUFBTSxHQUFlLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO1FBQzVDLElBQUksSUFBSSxHQUFlLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFBO1FBQ3BELElBQUksS0FBSyxHQUFVLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQ3RFLElBQUksS0FBSyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDOUIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBQyxHQUFHLEdBQUMsSUFBSSxDQUFDLEtBQUssR0FBQyxHQUFHLEdBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztRQUNuRCxJQUFJLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDO1FBQ3BCLEVBQUUsQ0FBQSxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUcsRUFBRSxDQUFDLENBQUEsQ0FBQztZQUNqQixJQUFJLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDO1lBQ3BCLElBQUksSUFBRSxHQUFHLEdBQUcsQ0FBQyxFQUFFLENBQUM7WUFDaEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUksRUFBQyxHQUFHLENBQUMsSUFBSSxFQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUMsSUFBSSxFQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVMsQ0FDMUUsVUFBQSxNQUFNO2dCQUNMLEVBQUUsQ0FBQyxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUN0QixJQUFJLENBQUMsR0FBQyxDQUFDLENBQUM7b0JBQ1IsSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDLElBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDN0IsSUFBSSxNQUFNLFNBQUksQ0FBQztvQkFDZixNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDaEMsWUFBWTtvQkFDWixxQ0FBcUM7b0JBQ3JDLHFDQUFxQztvQkFDckMsVUFBVTtvQkFDVixrREFBa0Q7b0JBQ2xELEtBQUs7b0JBQ0wsZUFBZTtvQkFDZixLQUFLO29CQUNMLEtBQUssR0FBRyxJQUFJLGFBQUssRUFBRSxDQUFDO29CQUNwQixLQUFLLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBQyxNQUFNLEdBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQzVELEtBQUssQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO29CQUN0QixLQUFLLENBQUMsZUFBZSxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDckMsS0FBSyxDQUFDLFNBQVMsR0FBRyx5QkFBeUIsQ0FBQztvQkFFNUMsSUFBSSxJQUFJLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDbEMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDaEMsd0JBQVUsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUNoQyxNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN4QixDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNQLE9BQU8sQ0FBQyxLQUFLLENBQUMsc0JBQXNCLENBQUMsQ0FBQztnQkFDdkMsQ0FBQztZQUNGLENBQUMsQ0FBQyxDQUFDO1FBQ0osQ0FBQztJQUNELENBQUM7SUFqVkY7UUFBQyxnQkFBUyxDQUFDLE9BQU8sQ0FBQzs7Z0RBQUE7SUFDbEI7UUFBQyxnQkFBUyxDQUFDLFdBQVcsQ0FBQzs7b0RBQUE7SUE5QnpCO1FBQUMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxlQUFlO1lBQzFCLFFBQVEsRUFBRSwwVEFTTjtTQUdKLENBQUM7O3FCQUFBO0lBaVdGLG9CQUFDO0FBQUQsQ0FBQyxBQWhXRCxJQWdXQztBQWhXWSxxQkFBYSxnQkFnV3pCLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCxJbnB1dCx0cmFuc2l0aW9uLFxyXG4gICAgRWxlbWVudFJlZiwgT25Jbml0LCBWaWV3Q2hpbGQsIFZpZXdDb250YWluZXJSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUsIFBhcmFtcyB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IEdlc3R1cmVUeXBlcywgU3dpcGVHZXN0dXJlRXZlbnREYXRhLCBHZXN0dXJlRXZlbnREYXRhIH0gZnJvbSBcInVpL2dlc3R1cmVzXCI7XHJcbmltcG9ydCB7IEdyaWRMYXlvdXQsIEdyaWRVbml0VHlwZSwgSXRlbVNwZWN9IGZyb20gXCJ1aS9sYXlvdXRzL2dyaWQtbGF5b3V0XCI7XHJcbmltcG9ydCB7IFN0YWNrTGF5b3V0IH0gZnJvbSBcInVpL2xheW91dHMvc3RhY2stbGF5b3V0XCI7XHJcbmltcG9ydCB7IENhbGVuZGFyU2VydmljZSB9IGZyb20gJy4vY2FsZW5kYXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE1vZGFsRGlhbG9nU2VydmljZSwgTW9kYWxEaWFsb2dPcHRpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL21vZGFsLWRpYWxvZ1wiO1xyXG5pbXBvcnQgeyBNb2RhbFZpZXdDb21wb25lbnQgfSBmcm9tIFwiLi9tb2RhbC5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgUGFnZSB9ICAgICAgICAgICAgIGZyb20gXCJ1aS9wYWdlXCI7XHJcbmltcG9ydCB7IExhYmVsIH0gZnJvbSBcInVpL2xhYmVsXCI7XHJcbmltcG9ydCB7IEFuaW1hdGlvbkN1cnZlIH0gZnJvbSBcInVpL2VudW1zXCI7XHJcbmltcG9ydCB7IEFuaW1hdGlvbiB9IGZyb20gXCJ1aS9hbmltYXRpb25cIjtcclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICd5ZWFyLWNhbGVuZGFyJyxcclxuXHR0ZW1wbGF0ZTogYFxyXG5cdDxBY3Rpb25CYXIgdGl0bGU9XCJcIj5cclxuXHRcdDxMYWJlbCB0ZXh0PVwidHJhbGFsYVwiICN0aXRsZT48L0xhYmVsPlxyXG5cdDwvQWN0aW9uQmFyPlxyXG5cdDxBYnNvbHV0ZUxheW91dD5cclxuXHRcdDxTY3JvbGxWaWV3IG9yaWVudGF0aW9uPVwidmVydGljYWxcIiB3aWR0aD1cIjEwMCVcIiBoZWlnaHQ9XCI5OSVcIj5cclxuXHRcdFx0PEFic29sdXRlTGF5b3V0IHN0eWxlPVwid2lkdGg6MTAwJTtoZWlnaHQ6MTIwMDtcIiAjY29udGFpbmVyPjwvQWJzb2x1dGVMYXlvdXQ+XHJcblx0XHQ8L1Njcm9sbFZpZXc+XHJcblx0PC9BYnNvbHV0ZUxheW91dD5cclxuICAgIGAsXHJcbiAgLy90ZW1wbGF0ZVVybDogJ3ZpZXcvY2FsZW5kYXIvd2Vlay5jb21wb25lbnQuaHRtbCcsXHJcbiAgLy9zdHlsZVVybHM6IFsnY3NzL3dlZWsuY29tcG9uZW50LmNzcycsJ2Nzcy9tb250aC5jb21wb25lbnQuY3NzJ10sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBXZWVrQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBwdWJsaWMgdGFyZ2V0ID0gbmV3IExhYmVsO1xyXG4gIHB1YmxpYyBzYXZlID0gbmV3IExhYmVsO1xyXG5cdHB1YmxpYyB3ZWVrOm51bWJlcltdO1xyXG4gIHB1YmxpYyB0ZXh0OmFueTtcclxuXHRwdWJsaWMgeWVhcjpudW1iZXI7XHJcblx0cHVibGljIHllYXIyOm51bWJlcjtcclxuXHRwdWJsaWMgbGFzdGRheTpudW1iZXI7XHJcblx0cHVibGljIG1vbnRoOm51bWJlcjtcclxuXHRwdWJsaWMgbW9udGgyID0gXCJKYW51YXJ5XCI7ICBcclxuXHRwdWJsaWMgc3RhdGU6bnVtYmVyID0gMDtcclxuICBwdWJsaWMgaG91cjogR3JpZExheW91dDtcclxuXHRwdWJsaWMgY29udDogYW55O1xyXG5cdHB1YmxpYyBjb250MjogYW55O1xyXG5cdEBWaWV3Q2hpbGQoXCJ0aXRsZVwiKSB0aXRsZTogRWxlbWVudFJlZjtcclxuICBAVmlld0NoaWxkKFwiY29udGFpbmVyXCIpIGNvbnRhaW5lcjogRWxlbWVudFJlZjtcclxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxwcml2YXRlIGFwcG9pbnQ6IENhbGVuZGFyU2VydmljZSwgXHJcblx0cHJpdmF0ZSBfbW9kYWxTZXJ2aWNlOiBNb2RhbERpYWxvZ1NlcnZpY2UscHJpdmF0ZSB2Y1JlZjogVmlld0NvbnRhaW5lclJlZiwgcHJpdmF0ZSBwYWdlOiBQYWdlICkge31cclxuXHRuZ09uSW5pdCgpe1xyXG5cdFx0Ly9jb25zb2xlLmxvZyhkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInNpZGUtY29udGFpbmVyXCIpKVxyXG5cdFx0dGhpcy5jb250ID0gdGhpcy5jb250YWluZXIubmF0aXZlRWxlbWVudDtcclxuICAgIHRoaXMudGV4dCA9IHRoaXMudGl0bGUubmF0aXZlRWxlbWVudDtcclxuICAgIHRoaXMuaG91ciA9IG5ldyBHcmlkTGF5b3V0KCk7XHJcblx0XHRmb3IobGV0IGk9MDtpPDc7aSs9MSl7XHJcbiAgICAgIGxldCBzZWNvbmRDb2x1bW4gPSBuZXcgSXRlbVNwZWMoMSwgR3JpZFVuaXRUeXBlLnN0YXIpO1xyXG4gICAgICB0aGlzLmhvdXIuYWRkQ29sdW1uKHNlY29uZENvbHVtbik7XHJcblx0XHRcdGxldCB0ZDIgPSB0aGlzLmhvdXJzKCk7XHJcblx0XHRcdEdyaWRMYXlvdXQuc2V0Q29sdW1uKHRkMiwgaSk7XHJcblx0XHRcdHRoaXMuaG91ci5hZGRDaGlsZCh0ZDIpO1xyXG4gICAgfSBcclxuXHRcdFxyXG5cdFx0dGhpcy5DcmVhdGUoMCk7XHJcblx0fVxyXG5cdERheXMoKXtcclxuXHRcdGxldCBtb246bnVtYmVyO1xyXG5cdFx0bGV0IGRhdGUgPSBuZXcgRGF0ZSgpO1xyXG5cdFx0dGhpcy5tb250aCA9IGRhdGUuZ2V0TW9udGgoKSsxO1xyXG5cdFx0dGhpcy55ZWFyID0gZGF0ZS5nZXRGdWxsWWVhcigpO1xyXG5cdFx0bGV0IGRheSA9IGRhdGUuZ2V0RGF5KCk7XHJcblx0XHRsZXQgdG9kYXkgPSBkYXRlLmdldERhdGUoKTtcdFxyXG5cdFx0aWYodG9kYXktZGF5PDApIHtcclxuXHRcdFx0aWYodGhpcy5tb250aCA9PSAxKXtcclxuXHRcdFx0XHR0aGlzLm1vbnRoID0gMTI7XHJcblx0XHRcdFx0dGhpcy55ZWFyIC09IDE7XHJcblx0XHRcdH1cclxuXHRcdFx0ZWxzZXtcclxuXHRcdFx0XHR0aGlzLm1vbnRoIC09MTtcclxuXHRcdFx0fVxyXG5cdFx0XHRsZXQgb2JqID0ge21vbjogMH1cclxuXHRcdFx0dGhpcy5HZXRNb250aChvYmopO1xyXG5cdFx0XHR0aGlzLmxhc3RkYXkgPSBvYmoubW9uICsgdG9kYXkgLSBkYXk7XHJcblx0XHR9XHJcblx0XHRlbHNlIHRoaXMubGFzdGRheSA9IHRvZGF5LWRheTtcclxuXHRcdHJldHVybiB0aGlzLldlZWsoKTtcclxuXHR9XHJcblx0V2Vlaygpe1xyXG5cdFx0dmFyIG9iaiA9IHttb246IDB9XHJcblx0XHR0aGlzLkdldE1vbnRoKG9iaik7XHJcblx0XHRsZXQgaiA9IHRoaXMubGFzdGRheTtcclxuXHRcdGxldCBkYXlzID0gbmV3IEFycmF5O1xyXG5cdFx0aWYodGhpcy5sYXN0ZGF5LTcgPCAwKXtcclxuXHRcdFx0aWYodGhpcy5tb250aCA9PSAxKXt0aGlzLm1vbnRoID0gMTI7IHRoaXMueWVhciAtPTE7fVxyXG5cdFx0XHRlbHNlIHRoaXMubW9udGggLT0xO1xyXG5cdFx0fVxyXG5cdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCA3O2krPTEpIHtcclxuXHRcdFx0aWYoaiA9PSBvYmoubW9uKSBqID0gMDtcclxuXHRcdFx0aiArPTE7XHJcblx0XHRcdGRheXMucHVzaChqKVxyXG5cdFx0fVxyXG5cdFx0dGhpcy5sYXN0ZGF5ID0gajtcclxuXHRcdGlmKGRheXNbMF0gPCBkYXlzWzZdICYmIGRheXNbMF0tNzw9MCl7XHJcblx0XHRcdGlmKHRoaXMubW9udGggPT0gMTIpe3RoaXMubW9udGggPSAxOyB0aGlzLnllYXIgKz0xO31cclxuXHRcdFx0ZWxzZSB0aGlzLm1vbnRoICs9MTtcclxuXHRcdH1cclxuXHRcdHRoaXMuTW9udGgoKTtcclxuXHRcdGlmKGRheXNbMF0gPiBkYXlzWzZdICYmIGRheXNbNl0tNzw9MCl7XHJcblx0XHRcdGlmKHRoaXMubW9udGggPT0gMTIpe3RoaXMubW9udGggPSAxOyB0aGlzLnllYXIgKz0xO31cclxuXHRcdFx0ZWxzZSB0aGlzLm1vbnRoICs9MTtcclxuXHRcdH1cclxuXHRcdHJldHVybiBkYXlzO1xyXG5cdH1cclxuXHRXZWVrMSgpe1xyXG5cdFx0dmFyIG9iaiA9IHttb246IDB9XHJcblx0XHRpZih0aGlzLmxhc3RkYXkgLSAxNCA+PSAwKXtcclxuXHRcdFx0dGhpcy5sYXN0ZGF5IC09MTQ7XHJcblx0XHRcdHRoaXMuR2V0TW9udGgob2JqKTtcclxuXHRcdH1cclxuXHRcdGVsc2Uge1xyXG5cdFx0XHRpZih0aGlzLm1vbnRoID09IDEpe1xyXG5cdFx0XHRcdHRoaXMubW9udGggPSAxMjtcclxuXHRcdFx0XHR0aGlzLnllYXIgLT0gMTtcclxuXHRcdFx0XHR0aGlzLkdldE1vbnRoKG9iaik7XHJcblx0XHRcdH1cclxuXHRcdFx0ZWxzZXtcclxuXHRcdFx0XHR0aGlzLm1vbnRoIC09MTtcclxuXHRcdFx0XHR0aGlzLkdldE1vbnRoKG9iaik7XHJcblx0XHRcdH1cclxuXHRcdFx0dGhpcy5sYXN0ZGF5ID0gKHRoaXMubGFzdGRheSAtIDE0ICsgb2JqLm1vbik7XHJcblx0XHR9XHJcblx0XHRsZXQgZGF5cyA9IG5ldyBBcnJheTtcclxuXHRcdGxldCBqID0gdGhpcy5sYXN0ZGF5O1xyXG5cdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCA3O2krPTEpIHtcclxuXHRcdFx0aWYoaiA9PSBvYmoubW9uKSBqID0gMDtcclxuXHRcdFx0aiArPTE7XHJcblx0XHRcdGRheXMucHVzaChqKVxyXG5cdFx0fVxyXG5cdFx0dGhpcy5sYXN0ZGF5ID0gajtcclxuXHRcdGlmKHRoaXMubGFzdGRheSAtIDcgPCAwKXtcclxuXHRcdFx0aWYodGhpcy5tb250aCA9PSAxMil7XHJcblx0XHRcdFx0dGhpcy5tb250aCA9IDE7XHJcblx0XHRcdFx0dGhpcy55ZWFyICs9MTtcclxuXHRcdFx0fVxyXG5cdFx0XHRlbHNlIHRoaXMubW9udGggKz0xO1xyXG5cdFx0fVxyXG5cdFx0dGhpcy5Nb250aCgpO1xyXG5cdFx0cmV0dXJuIGRheXM7XHJcblx0fVxyXG5cdEdldE1vbnRoKG9iajogYW55KXtcclxuXHRcdGlmKHRoaXMubW9udGggPD0gNyl7XHJcblx0XHRcdGlmKHRoaXMubW9udGggJSAyID09IDEpIG9iai5tb24gPSAzMTtcclxuXHRcdFx0ZWxzZSBvYmoubW9uID0gMzA7XHJcblx0XHRcdGlmKHRoaXMubW9udGggPT0gMil7XHJcblx0XHRcdFx0aWYoKCh0aGlzLnllYXIgJSA0ID09IDApICYmICh0aGlzLnllYXIgJSAxMDAgIT0gMCkpIFxyXG5cdFx0XHRcdFx0fHwgKHRoaXMueWVhciAlIDQwMCA9PSAwKSlvYmoubW9uID0gMjk7XHJcblx0XHRcdFx0ZWxzZSBvYmoubW9uID0gMjg7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdGVsc2V7XHJcblx0XHRcdGlmKHRoaXMubW9udGggJSAyID09IDEpIG9iai5tb24gPSAzMDtcclxuXHRcdFx0ZWxzZSBvYmoubW9uID0gMzE7XHJcblx0XHR9XHJcblx0fVxyXG5cdENyZWF0ZShuOiBhbnkpe1xyXG5cdFx0dGhpcy5jb250MiA9IG5ldyBHcmlkTGF5b3V0KCk7XHJcbiBcdFx0dGhpcy5jb250Mi5zdHlsZS53aWR0aCA9IFwiMTAwJVwiO1xyXG5cdFx0dGhpcy5jb250Mi5zdHlsZS5ib3JkZXJSaWdodENvbG9yID0gXCIjZTdlN2U3XCI7XHJcblx0XHR0aGlzLmNvbnQyLnN0eWxlLmJvcmRlcldpZHRoID0gXCIxcHhcIjtcclxuICAgIHRoaXMuY29udDIuc3R5bGUuYmFja2dyb3VuZENvbG9yID0gXCJ3aGl0ZVwiXHJcblx0XHR2YXIgd2Vlazphbnk7XHJcblx0XHRzd2l0Y2ggKG4pIHtcclxuXHRcdFx0Y2FzZSAtMTp3ZWVrID0gdGhpcy5XZWVrMSgpO2JyZWFrO1xyXG5cdFx0XHRjYXNlIDA6d2VlayA9IHRoaXMuRGF5cygpO2JyZWFrO1xyXG5cdFx0XHRjYXNlIDE6d2VlayA9IHRoaXMuV2VlaygpO2JyZWFrO1xyXG5cdFx0fVxyXG5cdFx0bGV0IGdyaWQgPSB0aGlzLmhvdXI7XHJcbiAgICBncmlkLmlkID0gXCJjb250dFwiXHJcbiAgICBncmlkLnN0eWxlLm1hcmdpblRvcCA9IDQwO1xyXG5cdFx0bGV0IGk9MDtcclxuICAgIGxldCBkYXlzID0gbmV3IEdyaWRMYXlvdXQoKTtcclxuXHRcdHdlZWsuZm9yRWFjaCgoZWxlbWVudDphbnkpID0+IHtcclxuICAgICAgbGV0IHNlY29uZENvbHVtbiA9IG5ldyBJdGVtU3BlYygxLCBHcmlkVW5pdFR5cGUuc3Rhcik7XHJcbiAgICAgIGRheXMuYWRkQ29sdW1uKHNlY29uZENvbHVtbik7XHJcbiAgICAgIGxldCB0ZCA9IG5ldyBMYWJlbCgpO1xyXG5cdFx0XHR0ZC50ZXh0ID0gZWxlbWVudDtcclxuICAgICAgdGQuY2xhc3NOYW1lID0gXCJkYXktd2Vla1wiO1xyXG4gICAgICBHcmlkTGF5b3V0LnNldENvbHVtbih0ZCwgaSk7XHJcbiAgICAgIGRheXMuYWRkQ2hpbGQodGQpO1xyXG5cdFx0XHRpKz0xO1xyXG5cdCAgfSk7XHJcblx0XHRkYXlzLmlkPVwiZGF5c1wiO1xyXG4gICAgdGhpcy5jb250Mi5hZGRDaGlsZChkYXlzKTtcclxuXHQgIHRoaXMuY29udDIuYWRkQ2hpbGQoZ3JpZCk7XHJcbiAgICBpZih0aGlzLnN0YXRlID09IDEpIHRoaXMuY29udDIudHJhbnNsYXRlWCA9IC0xMDAwO1xyXG4gICAgaWYodGhpcy5zdGF0ZSA9PSAyKSB0aGlzLmNvbnQyLnRyYW5zbGF0ZVggPSAxMDAwO1xyXG4gICAgdGhpcy5jb250LmFkZENoaWxkKHRoaXMuY29udDIpO1xyXG4gICAgdGhpcy5zaG93QXR0YWNoKHdlZWtbMF0pO1xyXG4gICAgdGhpcy5jb250Mi5hbmltYXRlKHtcclxuICAgICAgdHJhbnNsYXRlOiB7IHg6IDAsIHk6IDB9LCAgICBcclxuICAgICAgZHVyYXRpb246IDgwMCxcclxuICAgICAgY3VydmU6IEFuaW1hdGlvbkN1cnZlLmVhc2VPdXRcclxuICAgIH0pO1xyXG4gICAgdGhpcy5ob3VyID0gbmV3IEdyaWRMYXlvdXQoKTtcclxuXHRcdGZvcihsZXQgaT0wO2k8NztpKz0xKXtcclxuICAgICAgbGV0IHNlY29uZENvbHVtbiA9IG5ldyBJdGVtU3BlYygxLCBHcmlkVW5pdFR5cGUuc3Rhcik7XHJcbiAgICAgIHRoaXMuaG91ci5hZGRDb2x1bW4oc2Vjb25kQ29sdW1uKTtcclxuXHRcdFx0bGV0IHRkMiA9IHRoaXMuaG91cnMoKTtcclxuXHRcdFx0R3JpZExheW91dC5zZXRDb2x1bW4odGQyLCBpKTtcclxuXHRcdFx0dGhpcy5ob3VyLmFkZENoaWxkKHRkMik7XHJcbiAgICB9XHJcbiAgICB0aGlzLmNvbnQub24oR2VzdHVyZVR5cGVzLnN3aXBlLChhcmdzOiBTd2lwZUdlc3R1cmVFdmVudERhdGEpPT57XHJcblx0XHRcdGNvbnNvbGUubG9nKFwiU3dpcGUgRGlyZWN0aW9uOiBcIiArIGFyZ3MuZGlyZWN0aW9uKTtcclxuXHRcdFx0XHRpZihhcmdzLmRpcmVjdGlvbj09Mil7XHJcblx0XHRcdFx0XHR0aGlzLk5leHQoKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0aWYoYXJncy5kaXJlY3Rpb249PTEpe1xyXG5cdFx0XHRcdFx0dGhpcy5QcmV2KCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KTtcclxuICAgXHJcblx0fVxyXG4gIHNob3dBdHRhY2goZGF5OiBudW1iZXIpe1xyXG5cdFx0bGV0IGRhdGUgPSB0aGlzLnllYXIrXCItXCIrdGhpcy5tb250aCtcIi1cIitkYXk7XHJcblx0XHRsZXQgYXJyID0gbmV3IEFycmF5O1xyXG5cdFx0dGhpcy5hcHBvaW50LmdldEFwcG9pbnRtZW50KGRhdGUpLnN1YnNjcmliZShcclxuXHRcdFx0cmVzdWx0ID0+IHtcclxuXHRcdFx0XHRpZihyZXN1bHQubGVuZ3RoPjApe1xyXG4gICAgICAgICAgbGV0IGdyYW5kUGFyZW50ID0gdGhpcy5jb250Mi5nZXRDaGlsZEJ5SWQoJ2NvbnR0Jyk7XHJcbiAgICAgICAgICAvLyBmb3IobGV0IGRhIGluIGdyYW5kUGFyZW50KXtcclxuICAgICAgICAgIC8vICAgIGNvbnNvbGUubG9nKGRhKVxyXG4gICAgICAgICAgLy8gfVxyXG5cdFx0XHRcdFx0YXJyID0gcmVzdWx0O1xyXG5cdFx0XHRcdFx0bGV0IGQ6bnVtYmVyLHBhcmVudDphbnksY2hpbGRyZW46YW55LHN0YXJ0OmFueSxlbmQ6YW55LGk6YW55LGxhYmVsOmFueSx0aW1lOnN0cmluZztcclxuICAgICAgICAgIGZvcihsZXQga2V5IGluIGFycil7XHJcbiAgICAgICAgICAgIGQgPSBhcnJba2V5XVtcImRhdGVcIl0uc3BsaXQoXCItXCIpO1xyXG5cdFx0XHRcdFx0XHRkID0gcGFyc2VJbnQoZFsyXSktZGF5O1xyXG4gICAgICAgICAgICBwYXJlbnQgPSBncmFuZFBhcmVudC5nZXRDaGlsZEF0KGQpXHJcbiAgICAgICAgICAgIHN0YXJ0ID0gYXJyW2tleV1bXCJzdGFydFwiXS5zcGxpdChcIjpcIik7XHJcblx0XHRcdFx0XHRcdHN0YXJ0ID0gcGFyc2VJbnQoc3RhcnRbMF0pO1xyXG4gICAgICAgICAgICBwYXJlbnQucmVtb3ZlQ2hpbGQocGFyZW50LmdldENoaWxkQnlJZChzdGFydCkpO1xyXG4gICAgICAgICAgICBsYWJlbCA9IG5ldyBMYWJlbCgpO1xyXG4gICAgICAgICAgICBsYWJlbC50ZXh0ID0gYXJyW2tleV1bXCJuYW1lXCJdK1wiXFxyXFxuXCIrYXJyW2tleV1bXCJzdGFydFwiXS5zdWJzdHJpbmcoMCwgNSk7XHJcbiAgICAgICAgICAgIGxhYmVsLnRleHRXcmFwID0gdHJ1ZTtcclxuICAgICAgICAgICAgbGFiZWwuYmFja2dyb3VuZENvbG9yXHQ9IGFycltrZXldW1wiY29sb3JcIl07XHJcbiAgICAgICAgICAgIGxhYmVsLmNsYXNzTmFtZSA9IFwiaG91ci1idG4gYXR0YWNobWVudC1idG5cIjtcclxuICAgICAgICAgICAgR3JpZExheW91dC5zZXRSb3cobGFiZWwsIHN0YXJ0KTtcclxuXHRcdFx0ICAgICAgcGFyZW50LmFkZENoaWxkKGxhYmVsKTtcclxuICAgICAgICAgIH1cclxuXHRcdFx0XHR9XHJcbiAgICAgICAgZWxzZSBjb25zb2xlLmxvZyhcIm5vdFwiKVxyXG5cdFx0XHR9LFxyXG4gICAgICBlcnJvciA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2coZXJyb3IpXHJcbiAgICAgIH0pOyBcclxuXHR9XHJcbiAgaG91cnMoKXtcclxuXHRcdGxldCB0ZCA9IG5ldyBHcmlkTGF5b3V0KCk7XHJcblx0XHRsZXQgbGFiZWw6YW55O1xyXG5cdFx0Zm9yKGxldCBpPTA7aTwyNDtpKyspe1xyXG5cdFx0XHR2YXIgc2Vjb25kUm93ID0gbmV3IEl0ZW1TcGVjKDEsIEdyaWRVbml0VHlwZS5hdXRvKTtcclxuXHRcdFx0dGQuYWRkUm93KHNlY29uZFJvdyk7XHJcblx0XHRcdGxhYmVsID0gbmV3IExhYmVsKCk7XHJcblx0XHRcdGxhYmVsLmNsYXNzTmFtZSA9IFwiaG91ci1idG5cIjtcclxuXHRcdFx0aWYoaTwxMCkgbGFiZWwudGV4dCA9IFwiMFwiK2krXCI6MzBcIjtcclxuXHRcdFx0ZWxzZSBsYWJlbC50ZXh0ID0gaStcIjozMFwiO1xyXG5cdFx0XHRsYWJlbC5pZCA9IGk7XHJcbiAgICAgIGxhYmVsLm9uKEdlc3R1cmVUeXBlcy50YXAsICAoYXJnczogR2VzdHVyZUV2ZW50RGF0YSk9PiB7XHJcbiAgICAgICAgdGhpcy5jbGlja2VkKGFyZ3Mub2JqZWN0KTtcclxuICAgICAgfSk7XHJcbiAgICAgIGxhYmVsLm9uKEdlc3R1cmVUeXBlcy5zd2lwZSwoYXJnczogU3dpcGVHZXN0dXJlRXZlbnREYXRhKT0+e1xyXG5cdFx0XHRjb25zb2xlLmxvZyhcIlN3aXBlIERpcmVjdGlvbjogXCIgKyBhcmdzLmRpcmVjdGlvbik7XHJcblx0XHRcdFx0aWYoYXJncy5kaXJlY3Rpb249PTIpe1xyXG5cdFx0XHRcdFx0dGhpcy5OZXh0KCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGlmKGFyZ3MuZGlyZWN0aW9uPT0xKXtcclxuXHRcdFx0XHRcdHRoaXMuUHJldigpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0XHRcdC8vbGFiZWwuc3R5bGUucGFkZGluZ1RvcCA9IDIwOyBcclxuXHRcdFx0R3JpZExheW91dC5zZXRSb3cobGFiZWwsIGkpO1xyXG5cdFx0XHR0ZC5hZGRDaGlsZChsYWJlbCk7XHJcblx0XHR9XHJcblx0XHRyZXR1cm4gdGQ7XHJcbiAgfVxyXG5cdFByZXYoKXtcclxuICAgIHRoaXMuc3RhdGUgPSAxO1xyXG5cdFx0dGhpcy5DcmVhdGUoLTEpO1xyXG5cdH1cclxuXHROZXh0KCl7XHJcbiAgICB0aGlzLnN0YXRlID0gMjtcclxuXHRcdHRoaXMuQ3JlYXRlKDEpO1xyXG5cdH1cclxuXHRNb250aCgpe1xyXG5cdFx0c3dpdGNoICh0aGlzLm1vbnRoKSB7XHJcblx0XHRcdGNhc2UgMTogdGhpcy5tb250aDIgPSBcIkphbnVhcnlcIjticmVhaztcclxuXHRcdFx0Y2FzZSAyOiB0aGlzLm1vbnRoMiA9IFwiRmVicnVhcnlcIjticmVhaztcclxuXHRcdFx0Y2FzZSAzOiB0aGlzLm1vbnRoMiA9IFwiTWFyY2hcIjticmVhaztcclxuXHRcdFx0Y2FzZSA0OiB0aGlzLm1vbnRoMiA9IFwiQXByaWxcIjticmVhaztcclxuXHRcdFx0Y2FzZSA1OiB0aGlzLm1vbnRoMiA9IFwiTWF5XCI7YnJlYWs7XHJcblx0XHRcdGNhc2UgNjogdGhpcy5tb250aDIgPSBcIkp1bmVcIjticmVhaztcclxuXHRcdFx0Y2FzZSA3OiB0aGlzLm1vbnRoMiA9IFwiSnVseVwiO2JyZWFrO1xyXG5cdFx0XHRjYXNlIDg6IHRoaXMubW9udGgyID0gXCJBdWd1c3RcIjticmVhaztcclxuXHRcdFx0Y2FzZSA5OiB0aGlzLm1vbnRoMiA9IFwiU2VwdGVtYmVyXCI7YnJlYWs7XHJcblx0XHRcdGNhc2UgMTA6dGhpcy5tb250aDIgPSBcIk9jdG9iZXJcIjticmVhaztcclxuXHRcdFx0Y2FzZSAxMTp0aGlzLm1vbnRoMiA9IFwiTm92ZW1iZXJcIjticmVhaztcclxuXHRcdFx0Y2FzZSAxMjp0aGlzLm1vbnRoMiA9IFwiRGVjZW1iZXJcIjticmVhaztcclxuXHRcdH1cclxuXHRcdHRoaXMueWVhcjIgPSB0aGlzLnllYXI7XHJcbiAgICBzZXRUaW1lb3V0KCgpPT57XHJcblx0XHRcdFx0XHR0aGlzLnRleHQudGV4dCA9IHRoaXMubW9udGgyXHJcblx0XHRcdFx0fSwzMDApXHJcblx0fVxyXG4gIGNsaWNrZWQoZXZlbnQ6IGFueSl7XHJcbiAgICBpZihldmVudCA9PSB0aGlzLnRhcmdldCl7XHJcblx0XHRcdGxldCBvYmogPSB7XHJcblx0XHRcdFx0c3RhcnQ6IHRoaXMuc2F2ZS50ZXh0LFxyXG5cdFx0XHRcdHZjUmVmOiB0aGlzLnZjUmVmXHJcblx0XHRcdH1cclxuXHRcdFx0bGV0IG9wdGlvbnM6IE1vZGFsRGlhbG9nT3B0aW9ucyA9IHtcclxuXHRcdFx0XHRcdFx0XHR2aWV3Q29udGFpbmVyUmVmOiB0aGlzLnZjUmVmLFxyXG5cdFx0XHRcdFx0XHRcdGNvbnRleHQ6IG9iaixcclxuXHRcdFx0XHRcdFx0XHRmdWxsc2NyZWVuOiB0cnVlXHJcblx0XHRcdFx0XHR9O1xyXG5cdFx0XHR0aGlzLl9tb2RhbFNlcnZpY2Uuc2hvd01vZGFsKE1vZGFsVmlld0NvbXBvbmVudCwgb3B0aW9ucylcclxuXHRcdFx0LnRoZW4oKGFwcG9pbnRtZW50OiBhbnkpID0+IHtcclxuXHRcdFx0XHRcdGlmKGFwcG9pbnRtZW50ICE9IHVuZGVmaW5lZCl7XHJcblx0XHRcdFx0XHRcdHRoaXMuQXBwb2ludG1lbnQoYXBwb2ludG1lbnQpO1xyXG5cdFx0XHRcdFx0fSBcclxuICAgIFx0fSk7XHJcblx0XHRcclxuXHRcdH1cclxuXHRcdGVsc2V7XHJcblx0XHRcdHRoaXMudGFyZ2V0LnRleHQgPSB0aGlzLnNhdmUudGV4dDtcclxuXHRcdFx0dGhpcy50YXJnZXQuY2xhc3NOYW1lID0gdGhpcy5zYXZlLmNsYXNzTmFtZVxyXG5cdFx0XHR0aGlzLnNhdmUudGV4dCA9IGV2ZW50LnRleHQ7XHJcblx0XHRcdHRoaXMuc2F2ZS5jbGFzc05hbWUgPSBldmVudC5jbGFzc05hbWU7XHJcblx0XHRcdHRoaXMudGFyZ2V0ID0gZXZlbnQ7XHJcblx0XHRcdGV2ZW50LmNsYXNzTmFtZSA9IFwiaG91ci1idG4gbmV3LWF0dGFjaFwiO1xyXG5cdFx0XHRldmVudC50ZXh0PVwiK1wiXHJcblx0XHR9XHJcbiAgfVxyXG4gIEFwcG9pbnRtZW50KG9iajphbnkpe1xyXG5cdFx0bGV0IGdyYW5kUGFyZW50ID0gdGhpcy5jb250Mi5nZXRDaGlsZEJ5SWQoJ2NvbnR0JylcclxuXHRcdGxldCBwYXJlbnQgPSA8R3JpZExheW91dD50aGlzLnRhcmdldC5wYXJlbnQ7XHJcblx0XHRsZXQgZGF5cyA9IDxHcmlkTGF5b3V0PnRoaXMucGFnZS5nZXRWaWV3QnlJZChcImRheXNcIilcclxuXHRcdGxldCBsYWJlbCA9IDxMYWJlbD5kYXlzLmdldENoaWxkQXQoZ3JhbmRQYXJlbnQuZ2V0Q2hpbGRJbmRleChwYXJlbnQpKTtcclxuXHRcdGxldCBjb2xvciA9IG9iai5jb2xvcltcIl9oZXhcIl07XHJcblx0XHRsZXQgZGF0ZSA9IHRoaXMueWVhcitcIi1cIit0aGlzLm1vbnRoK1wiLVwiK2xhYmVsLnRleHQ7XHJcblx0XHRsZXQgbmFtZSA9IG9iai5uYW1lO1xyXG5cdFx0aWYob2JqLm5hbWUgIT1cIlwiKXtcclxuXHRcdFx0bGV0IGZyb20gPSBvYmouZnJvbTtcclxuXHRcdFx0bGV0IHRvID0gb2JqLnRvO1xyXG5cdFx0XHR0aGlzLmFwcG9pbnQucG9zdEFwcG9pbnRtZW50KG9iai5uYW1lLG9iai5mcm9tLG9iai50byxkYXRlLGNvbG9yKS5zdWJzY3JpYmUoXHJcblx0XHRcdFx0cmVzdWx0ID0+IHtcclxuXHRcdFx0XHRcdGlmIChyZXN1bHQgPT09IHRydWUpIHtcclxuXHRcdFx0XHRcdGxldCBpPTE7XHJcblx0XHRcdFx0XHRsZXQgdmFsID0gcGFyc2VJbnQodG8udmFsdWUpO1xyXG5cdFx0XHRcdFx0bGV0IHJlbW92ZTphbnk7XHJcblx0XHRcdFx0XHRwYXJlbnQucmVtb3ZlQ2hpbGQodGhpcy50YXJnZXQpO1xyXG5cdFx0XHRcdFx0Ly8gd2hpbGUoaSl7XHJcblx0XHRcdFx0XHQvLyBcdHJlbW92ZSA9IHRoaXMudGFyZ2V0Lm5leHRTaWJsaW5nO1xyXG5cdFx0XHRcdFx0Ly8gXHRpZih2YWwgPiBwYXJzZUludChyZW1vdmUudmFsdWUpKXtcclxuXHRcdFx0XHRcdC8vIFx0aSs9MTsgXHJcblx0XHRcdFx0XHQvLyBcdHRoaXMudGFyZ2V0LnBhcmVudEVsZW1lbnQucmVtb3ZlQ2hpbGQocmVtb3ZlKTtcclxuXHRcdFx0XHRcdC8vIFx0fVxyXG5cdFx0XHRcdFx0Ly8gXHRlbHNlIGJyZWFrO1xyXG5cdFx0XHRcdFx0Ly8gfTtcclxuXHRcdFx0XHRcdGxhYmVsID0gbmV3IExhYmVsKCk7XHJcblx0XHRcdFx0XHRsYWJlbC50ZXh0ID0gb2JqW1wibmFtZVwiXStcIlxcclxcblwiK29ialtcImZyb21cIl0uc3Vic3RyaW5nKDAsIDUpO1xyXG5cdFx0XHRcdFx0bGFiZWwudGV4dFdyYXAgPSB0cnVlO1xyXG5cdFx0XHRcdFx0bGFiZWwuYmFja2dyb3VuZENvbG9yXHQ9IG9ialtcImNvbG9yXCJdO1xyXG5cdFx0XHRcdFx0bGFiZWwuY2xhc3NOYW1lID0gXCJob3VyLWJ0biBhdHRhY2htZW50LWJ0blwiO1xyXG5cdFx0XHRcdFx0IFxyXG5cdFx0XHRcdFx0bGV0IHZhbDIgPSBvYmpbXCJmcm9tXCJdLnNwbGl0KFwiOlwiKTtcclxuXHRcdFx0XHRcdGxldCBzdGFydCA9IChwYXJzZUludCh2YWwyWzBdKSk7XHJcblx0XHRcdFx0XHRHcmlkTGF5b3V0LnNldFJvdyhsYWJlbCwgc3RhcnQpO1xyXG5cdFx0XHRcdFx0cGFyZW50LmFkZENoaWxkKGxhYmVsKTtcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0Y29uc29sZS5lcnJvcihcIlNvbWV0aGluZyB3ZW50IHdyb25nXCIpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0XHR9XHJcbiAgfVxyXG59XHJcbiJdfQ==