"use strict";
var core_1 = require("@angular/core");
var timer_1 = require("timer");
var page_1 = require("ui/page");
var label_1 = require("ui/label");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var map_component_1 = require("../map.component");
var datePicker_component_1 = require("./datePicker.component");
var ModalViewComponent = (function () {
    function ModalViewComponent(_modalService, params, page) {
        var _this = this;
        this._modalService = _modalService;
        this.params = params;
        this.page = page;
        this.label = new label_1.Label;
        this.colour = "#0275d8";
        timer_1.setTimeout(function () {
            _this.from = _this.page.getViewById("from");
            _this.to = _this.page.getViewById("to");
            _this.from.text = _this.params.context.start;
            var val = _this.params.context.start.split(":");
            var val2 = val[1];
            val = (parseInt(val[0]) + 1);
            if (val < 10)
                val = "0" + val;
            val = val + ":" + val2;
            _this.to.text = val;
            _this.label = _this.title.nativeElement;
        });
    }
    ModalViewComponent.prototype.time = function () {
        var _this = this;
        var options = {
            viewContainerRef: this.params.context.vcRef,
            context: this.from.text,
            fullscreen: false
        };
        this._modalService.showModal(datePicker_component_1.DatePickerComponent, options)
            .then(function (time) {
            if (time != undefined)
                if (time.getHours() < 10)
                    _this.from.text = "0" + time.getHours() + ":" + time.getMinutes();
                else
                    _this.from.text = time.getHours() + ":" + time.getMinutes();
        });
    };
    ModalViewComponent.prototype.time2 = function () {
        var _this = this;
        var options = {
            viewContainerRef: this.params.context.vcRef,
            context: this.to.text,
            fullscreen: false
        };
        this._modalService.showModal(datePicker_component_1.DatePickerComponent, options)
            .then(function (time) {
            if (time != undefined) {
                if (time.getHours() < 10)
                    _this.to.text = "0" + time.getHours() + ":" + time.getMinutes();
                else
                    _this.to.text = time.getHours() + ":" + time.getMinutes();
            }
        });
    };
    ModalViewComponent.prototype.map = function () {
        var options = {
            viewContainerRef: this.params.context.vcRef,
            //context: this.to.text,
            fullscreen: true
        };
        this._modalService.showModal(map_component_1.MapComponent, options)
            .then(function (time) {
            // if(time != undefined){
            //     if(time.getHours()<10)
            //         this.to.text = "0"+time.getHours()+":"+time.getMinutes();
            //     else this.to.text = time.getHours()+":"+time.getMinutes();
            // }
        });
    };
    ModalViewComponent.prototype.submit = function () {
        var obj = {
            from: this.from.text,
            to: this.from.text,
            name: this.name,
            color: this.colour
        };
        this.params.closeCallback(obj);
    };
    ModalViewComponent.prototype.Color = function (element) {
        this.label.className = "color";
        this.label = element.object;
        this.colour = this.label.style.backgroundColor;
        this.label.className = "selected";
    };
    __decorate([
        core_1.ViewChild("start"), 
        __metadata('design:type', core_1.ElementRef)
    ], ModalViewComponent.prototype, "title", void 0);
    ModalViewComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            templateUrl: './../view/calendar/modal.component.html',
        }), 
        __metadata('design:paramtypes', [modal_dialog_1.ModalDialogService, modal_dialog_1.ModalDialogParams, page_1.Page])
    ], ModalViewComponent);
    return ModalViewComponent;
}());
exports.ModalViewComponent = ModalViewComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vZGV2L0NhbGVuZGFyL21vZGFsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEscUJBQTBFLGVBQWUsQ0FBQyxDQUFBO0FBQzFGLHNCQUF1RCxPQUFPLENBQUMsQ0FBQTtBQUUvRCxxQkFBaUMsU0FBUyxDQUFDLENBQUE7QUFDM0Msc0JBQXNCLFVBQVUsQ0FBQyxDQUFBO0FBQ2pDLDZCQUEwRSxtQ0FBbUMsQ0FBQyxDQUFBO0FBQzlHLDhCQUE2QixrQkFBa0IsQ0FBQyxDQUFBO0FBQ2hELHFDQUFvQyx3QkFBd0IsQ0FBQyxDQUFBO0FBSzdEO0lBT0ksNEJBQW9CLGFBQWlDLEVBQzdDLE1BQXlCLEVBQVUsSUFBVTtRQVJ6RCxpQkFpRkM7UUExRXVCLGtCQUFhLEdBQWIsYUFBYSxDQUFvQjtRQUM3QyxXQUFNLEdBQU4sTUFBTSxDQUFtQjtRQUFVLFNBQUksR0FBSixJQUFJLENBQU07UUFKOUMsVUFBSyxHQUFHLElBQUksYUFBSyxDQUFDO1FBQ2xCLFdBQU0sR0FBTyxTQUFTLENBQUM7UUFJMUIsa0JBQVUsQ0FBQztZQUNQLEtBQUksQ0FBQyxJQUFJLEdBQVcsS0FBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDbEQsS0FBSSxDQUFDLEVBQUUsR0FBVyxLQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5QyxLQUFJLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7WUFDM0MsSUFBSSxHQUFHLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUMvQyxJQUFJLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEIsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzNCLEVBQUUsQ0FBQSxDQUFDLEdBQUcsR0FBQyxFQUFFLENBQUM7Z0JBQUMsR0FBRyxHQUFHLEdBQUcsR0FBQyxHQUFHLENBQUM7WUFDekIsR0FBRyxHQUFHLEdBQUcsR0FBRSxHQUFHLEdBQUMsSUFBSSxDQUFDO1lBQ3BCLEtBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQztZQUNuQixLQUFJLENBQUMsS0FBSyxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDO1FBQzFDLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUNELGlDQUFJLEdBQUo7UUFBQSxpQkFhQztRQVpHLElBQUksT0FBTyxHQUF1QjtZQUM5QixnQkFBZ0IsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1lBQzNDLE9BQU8sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUk7WUFDdkIsVUFBVSxFQUFFLEtBQUs7U0FDcEIsQ0FBQztRQUNGLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLDBDQUFtQixFQUFFLE9BQU8sQ0FBQzthQUN6RCxJQUFJLENBQUMsVUFBQyxJQUFTO1lBQ1osRUFBRSxDQUFBLENBQUMsSUFBSSxJQUFJLFNBQVMsQ0FBQztnQkFDakIsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxHQUFDLEVBQUUsQ0FBQztvQkFDbEIsS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxHQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBQyxHQUFHLEdBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO2dCQUMvRCxJQUFJO29CQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBQyxHQUFHLEdBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQ3ZFLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUNELGtDQUFLLEdBQUw7UUFBQSxpQkFjQztRQWJHLElBQUksT0FBTyxHQUF1QjtZQUM5QixnQkFBZ0IsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1lBQzNDLE9BQU8sRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUk7WUFDckIsVUFBVSxFQUFFLEtBQUs7U0FDcEIsQ0FBQztRQUNGLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLDBDQUFtQixFQUFFLE9BQU8sQ0FBQzthQUN6RCxJQUFJLENBQUMsVUFBQyxJQUFTO1lBQ1osRUFBRSxDQUFBLENBQUMsSUFBSSxJQUFJLFNBQVMsQ0FBQyxDQUFBLENBQUM7Z0JBQ2xCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBQyxFQUFFLENBQUM7b0JBQ2xCLEtBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxHQUFHLEdBQUcsR0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUMsR0FBRyxHQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztnQkFDN0QsSUFBSTtvQkFBQyxLQUFJLENBQUMsRUFBRSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUMsR0FBRyxHQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUM5RCxDQUFDO1FBQ1IsQ0FBQyxDQUFDLENBQUM7SUFDSixDQUFDO0lBQ0QsZ0NBQUcsR0FBSDtRQUNJLElBQUksT0FBTyxHQUF1QjtZQUM5QixnQkFBZ0IsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1lBQzNDLHdCQUF3QjtZQUN4QixVQUFVLEVBQUUsSUFBSTtTQUNuQixDQUFDO1FBQ0YsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsNEJBQVksRUFBRSxPQUFPLENBQUM7YUFDbEQsSUFBSSxDQUFDLFVBQUMsSUFBUztZQUNaLHlCQUF5QjtZQUN6Qiw2QkFBNkI7WUFDN0Isb0VBQW9FO1lBQ3BFLGlFQUFpRTtZQUNqRSxJQUFJO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDSixDQUFDO0lBQ00sbUNBQU0sR0FBYjtRQUNJLElBQUksR0FBRyxHQUFHO1lBQ04sSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSTtZQUNwQixFQUFFLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJO1lBQ2xCLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtZQUNmLEtBQUssRUFBRSxJQUFJLENBQUMsTUFBTTtTQUNyQixDQUFBO1FBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUNELGtDQUFLLEdBQUwsVUFBTSxPQUFZO1FBQ2QsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDO1FBQy9CLElBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQztRQUM1QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQztRQUMvQyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxVQUFVLENBQUM7SUFDekMsQ0FBQztJQTFFRTtRQUFDLGdCQUFTLENBQUMsT0FBTyxDQUFDOztxREFBQTtJQVZ2QjtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLHlDQUF5QztTQUN6RCxDQUFDOzswQkFBQTtJQWtGRix5QkFBQztBQUFELENBQUMsQUFqRkQsSUFpRkM7QUFqRlksMEJBQWtCLHFCQWlGOUIsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRWxlbWVudFJlZiwgT25Jbml0LCBWaWV3Q2hpbGQsIFZpZXdDb250YWluZXJSZWZ9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IHNldEludGVydmFsLCBzZXRUaW1lb3V0LCBjbGVhckludGVydmFsIH0gZnJvbSBcInRpbWVyXCI7XHJcbmltcG9ydCB7IENvbG9yIH0gZnJvbSBcImNvbG9yXCI7XHJcbmltcG9ydCB7IFBhZ2UgfSAgICAgICAgICAgICBmcm9tIFwidWkvcGFnZVwiO1xyXG5pbXBvcnQgeyBMYWJlbCB9IGZyb20gXCJ1aS9sYWJlbFwiO1xyXG5pbXBvcnQgeyBNb2RhbERpYWxvZ1NlcnZpY2UsIE1vZGFsRGlhbG9nT3B0aW9ucywgTW9kYWxEaWFsb2dQYXJhbXMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvbW9kYWwtZGlhbG9nXCI7XHJcbmltcG9ydCB7IE1hcENvbXBvbmVudCB9IGZyb20gXCIuLi9tYXAuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IERhdGVQaWNrZXJDb21wb25lbnQgfSBmcm9tIFwiLi9kYXRlUGlja2VyLmNvbXBvbmVudFwiO1xyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vLi4vdmlldy9jYWxlbmRhci9tb2RhbC5jb21wb25lbnQuaHRtbCcsXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNb2RhbFZpZXdDb21wb25lbnQge1xyXG4gICAgcHVibGljIGZyb206IExhYmVsO1xyXG4gICAgcHVibGljIHRvOiBMYWJlbDtcclxuICAgIHB1YmxpYyBuYW1lOiBzdHJpbmc7XHJcbiAgICBwdWJsaWMgbGFiZWwgPSBuZXcgTGFiZWw7XHJcbiAgICBwdWJsaWMgY29sb3VyOmFueSA9IFwiIzAyNzVkOFwiO1xyXG4gICAgQFZpZXdDaGlsZChcInN0YXJ0XCIpIHRpdGxlOiBFbGVtZW50UmVmO1xyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfbW9kYWxTZXJ2aWNlOiBNb2RhbERpYWxvZ1NlcnZpY2UsIFxyXG4gICAgcHJpdmF0ZSBwYXJhbXM6IE1vZGFsRGlhbG9nUGFyYW1zLCBwcml2YXRlIHBhZ2U6IFBhZ2Upe1xyXG4gICAgICAgIHNldFRpbWVvdXQoKCk9PntcclxuICAgICAgICAgICAgdGhpcy5mcm9tID0gPExhYmVsPiB0aGlzLnBhZ2UuZ2V0Vmlld0J5SWQoXCJmcm9tXCIpO1xyXG4gICAgICAgICAgICB0aGlzLnRvID0gPExhYmVsPiB0aGlzLnBhZ2UuZ2V0Vmlld0J5SWQoXCJ0b1wiKTtcclxuICAgICAgICAgICAgdGhpcy5mcm9tLnRleHQgPSB0aGlzLnBhcmFtcy5jb250ZXh0LnN0YXJ0O1xyXG4gICAgICAgICAgICBsZXQgdmFsID0gdGhpcy5wYXJhbXMuY29udGV4dC5zdGFydC5zcGxpdChcIjpcIik7XHJcbiAgICAgICAgICAgIGxldCB2YWwyID0gdmFsWzFdO1xyXG4gICAgICAgICAgICB2YWwgPSAocGFyc2VJbnQodmFsWzBdKSsxKTtcclxuICAgICAgICAgICAgaWYodmFsPDEwKSB2YWwgPSBcIjBcIit2YWw7XHJcbiAgICAgICAgICAgIHZhbCA9IHZhbCArXCI6XCIrdmFsMjtcclxuICAgICAgICAgICAgdGhpcy50by50ZXh0ID0gdmFsO1xyXG4gICAgICAgICAgICB0aGlzLmxhYmVsID0gdGhpcy50aXRsZS5uYXRpdmVFbGVtZW50O1xyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcbiAgICB0aW1lKCl7XHJcbiAgICAgICAgbGV0IG9wdGlvbnM6IE1vZGFsRGlhbG9nT3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgdmlld0NvbnRhaW5lclJlZjogdGhpcy5wYXJhbXMuY29udGV4dC52Y1JlZixcclxuICAgICAgICAgICAgY29udGV4dDogdGhpcy5mcm9tLnRleHQsXHJcbiAgICAgICAgICAgIGZ1bGxzY3JlZW46IGZhbHNlXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLl9tb2RhbFNlcnZpY2Uuc2hvd01vZGFsKERhdGVQaWNrZXJDb21wb25lbnQsIG9wdGlvbnMpXHJcbiAgICAgICAgLnRoZW4oKHRpbWU6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICBpZih0aW1lICE9IHVuZGVmaW5lZClcclxuICAgICAgICAgICAgICAgIGlmKHRpbWUuZ2V0SG91cnMoKTwxMClcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZyb20udGV4dCA9IFwiMFwiK3RpbWUuZ2V0SG91cnMoKStcIjpcIit0aW1lLmdldE1pbnV0ZXMoKTtcclxuICAgICAgICAgICAgICAgIGVsc2UgdGhpcy5mcm9tLnRleHQgPSB0aW1lLmdldEhvdXJzKCkrXCI6XCIrdGltZS5nZXRNaW51dGVzKCk7XHJcbiAgICBcdH0pO1xyXG4gICAgfVxyXG4gICAgdGltZTIoKXtcclxuICAgICAgICBsZXQgb3B0aW9uczogTW9kYWxEaWFsb2dPcHRpb25zID0ge1xyXG4gICAgICAgICAgICB2aWV3Q29udGFpbmVyUmVmOiB0aGlzLnBhcmFtcy5jb250ZXh0LnZjUmVmLFxyXG4gICAgICAgICAgICBjb250ZXh0OiB0aGlzLnRvLnRleHQsXHJcbiAgICAgICAgICAgIGZ1bGxzY3JlZW46IGZhbHNlXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLl9tb2RhbFNlcnZpY2Uuc2hvd01vZGFsKERhdGVQaWNrZXJDb21wb25lbnQsIG9wdGlvbnMpXHJcbiAgICAgICAgLnRoZW4oKHRpbWU6IGFueSkgPT4geyBcclxuICAgICAgICAgICAgaWYodGltZSAhPSB1bmRlZmluZWQpe1xyXG4gICAgICAgICAgICAgICAgaWYodGltZS5nZXRIb3VycygpPDEwKVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudG8udGV4dCA9IFwiMFwiK3RpbWUuZ2V0SG91cnMoKStcIjpcIit0aW1lLmdldE1pbnV0ZXMoKTtcclxuICAgICAgICAgICAgICAgIGVsc2UgdGhpcy50by50ZXh0ID0gdGltZS5nZXRIb3VycygpK1wiOlwiK3RpbWUuZ2V0TWludXRlcygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICBcdH0pO1xyXG4gICAgfVxyXG4gICAgbWFwKCl7XHJcbiAgICAgICAgbGV0IG9wdGlvbnM6IE1vZGFsRGlhbG9nT3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgdmlld0NvbnRhaW5lclJlZjogdGhpcy5wYXJhbXMuY29udGV4dC52Y1JlZixcclxuICAgICAgICAgICAgLy9jb250ZXh0OiB0aGlzLnRvLnRleHQsXHJcbiAgICAgICAgICAgIGZ1bGxzY3JlZW46IHRydWVcclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMuX21vZGFsU2VydmljZS5zaG93TW9kYWwoTWFwQ29tcG9uZW50LCBvcHRpb25zKVxyXG4gICAgICAgIC50aGVuKCh0aW1lOiBhbnkpID0+IHsgXHJcbiAgICAgICAgICAgIC8vIGlmKHRpbWUgIT0gdW5kZWZpbmVkKXtcclxuICAgICAgICAgICAgLy8gICAgIGlmKHRpbWUuZ2V0SG91cnMoKTwxMClcclxuICAgICAgICAgICAgLy8gICAgICAgICB0aGlzLnRvLnRleHQgPSBcIjBcIit0aW1lLmdldEhvdXJzKCkrXCI6XCIrdGltZS5nZXRNaW51dGVzKCk7XHJcbiAgICAgICAgICAgIC8vICAgICBlbHNlIHRoaXMudG8udGV4dCA9IHRpbWUuZ2V0SG91cnMoKStcIjpcIit0aW1lLmdldE1pbnV0ZXMoKTtcclxuICAgICAgICAgICAgLy8gfVxyXG4gICAgXHR9KTtcclxuICAgIH1cclxuICAgIHB1YmxpYyBzdWJtaXQoKSB7XHJcbiAgICAgICAgbGV0IG9iaiA9IHtcclxuICAgICAgICAgICAgZnJvbTogdGhpcy5mcm9tLnRleHQsXHJcbiAgICAgICAgICAgIHRvOiB0aGlzLmZyb20udGV4dCxcclxuICAgICAgICAgICAgbmFtZTogdGhpcy5uYW1lLFxyXG4gICAgICAgICAgICBjb2xvcjogdGhpcy5jb2xvdXJcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5wYXJhbXMuY2xvc2VDYWxsYmFjayhvYmopO1xyXG4gICAgfVxyXG4gICAgQ29sb3IoZWxlbWVudDogYW55KXtcclxuICAgICAgICB0aGlzLmxhYmVsLmNsYXNzTmFtZSA9IFwiY29sb3JcIjtcclxuICAgICAgICB0aGlzLmxhYmVsID0gZWxlbWVudC5vYmplY3Q7XHJcbiAgICAgICAgdGhpcy5jb2xvdXIgPSB0aGlzLmxhYmVsLnN0eWxlLmJhY2tncm91bmRDb2xvcjtcclxuICAgICAgICB0aGlzLmxhYmVsLmNsYXNzTmFtZSA9IFwic2VsZWN0ZWRcIjtcclxuXHR9XHJcbn0iXX0=