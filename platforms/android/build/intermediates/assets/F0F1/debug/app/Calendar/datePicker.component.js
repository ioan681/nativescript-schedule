"use strict";
var core_1 = require("@angular/core");
var timer_1 = require("timer");
var page_1 = require("ui/page");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var DatePickerComponent = (function () {
    function DatePickerComponent(params, page) {
        var _this = this;
        this.params = params;
        this.page = page;
        timer_1.setTimeout(function () {
            var val = params.context.split(":");
            var from = _this.page.getViewById("from");
            from.hour = parseInt(val[0]);
            from.minute = parseInt(val[1]);
        });
    }
    DatePickerComponent.prototype.submit = function () {
        this.params.closeCallback(this.time);
    };
    DatePickerComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            template: "\n    <StackLayout sdkToggleNavButton id=\"date\">\n        <TimePicker id=\"from\" [(ngModel)]='time'></TimePicker>\n        <GridLayout columns=\"130 130\" rows=\"auto\">\n            <button text=\"tap\" col=\"0\" (tap)=\"submit()\"></button>  \n            <button text=\"tap\" col=\"1\" (tap)=\"submit()\"></button>  \n        </GridLayout>\n    </StackLayout>",
        }), 
        __metadata('design:paramtypes', [modal_dialog_1.ModalDialogParams, page_1.Page])
    ], DatePickerComponent);
    return DatePickerComponent;
}());
exports.DatePickerComponent = DatePickerComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZVBpY2tlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9kZXYvQ2FsZW5kYXIvZGF0ZVBpY2tlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHFCQUFrRCxlQUFlLENBQUMsQ0FBQTtBQUNsRSxzQkFBdUQsT0FBTyxDQUFDLENBQUE7QUFFL0QscUJBQXFCLFNBQVMsQ0FBQyxDQUFBO0FBRS9CLDZCQUFrQyxtQ0FBbUMsQ0FBQyxDQUFBO0FBWXRFO0lBRUksNkJBQW9CLE1BQXlCLEVBQVUsSUFBVTtRQUZyRSxpQkFhQztRQVh1QixXQUFNLEdBQU4sTUFBTSxDQUFtQjtRQUFVLFNBQUksR0FBSixJQUFJLENBQU07UUFDN0Qsa0JBQVUsQ0FBQztZQUNQLElBQUksR0FBRyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BDLElBQUksSUFBSSxHQUFnQixLQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN0RCxJQUFJLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNuQyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDTSxvQ0FBTSxHQUFiO1FBQ0ksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUF2Qkw7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSwrV0FPSztTQUNsQixDQUFDOzsyQkFBQTtJQWNGLDBCQUFDO0FBQUQsQ0FBQyxBQWJELElBYUM7QUFiWSwyQkFBbUIsc0JBYS9CLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEVsZW1lbnRSZWYsIFZpZXdDaGlsZCwgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBzZXRJbnRlcnZhbCwgc2V0VGltZW91dCwgY2xlYXJJbnRlcnZhbCB9IGZyb20gXCJ0aW1lclwiO1xyXG5pbXBvcnQgeyBDb2xvciB9IGZyb20gXCJjb2xvclwiO1xyXG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInVpL3BhZ2VcIjtcclxuaW1wb3J0IHsgVGltZVBpY2tlciB9IGZyb20gXCJ1aS90aW1lLXBpY2tlclwiO1xyXG5pbXBvcnQgeyBNb2RhbERpYWxvZ1BhcmFtcyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9tb2RhbC1kaWFsb2dcIjtcclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgdGVtcGxhdGU6IGBcclxuICAgIDxTdGFja0xheW91dCBzZGtUb2dnbGVOYXZCdXR0b24gaWQ9XCJkYXRlXCI+XHJcbiAgICAgICAgPFRpbWVQaWNrZXIgaWQ9XCJmcm9tXCIgWyhuZ01vZGVsKV09J3RpbWUnPjwvVGltZVBpY2tlcj5cclxuICAgICAgICA8R3JpZExheW91dCBjb2x1bW5zPVwiMTMwIDEzMFwiIHJvd3M9XCJhdXRvXCI+XHJcbiAgICAgICAgICAgIDxidXR0b24gdGV4dD1cInRhcFwiIGNvbD1cIjBcIiAodGFwKT1cInN1Ym1pdCgpXCI+PC9idXR0b24+ICBcclxuICAgICAgICAgICAgPGJ1dHRvbiB0ZXh0PVwidGFwXCIgY29sPVwiMVwiICh0YXApPVwic3VibWl0KClcIj48L2J1dHRvbj4gIFxyXG4gICAgICAgIDwvR3JpZExheW91dD5cclxuICAgIDwvU3RhY2tMYXlvdXQ+YCxcclxufSlcclxuZXhwb3J0IGNsYXNzIERhdGVQaWNrZXJDb21wb25lbnQge1xyXG4gICAgcHVibGljIHRpbWU6RGF0ZTtcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcGFyYW1zOiBNb2RhbERpYWxvZ1BhcmFtcywgcHJpdmF0ZSBwYWdlOiBQYWdlKSB7XHJcbiAgICAgICAgc2V0VGltZW91dCgoKT0+e1xyXG4gICAgICAgICAgICBsZXQgdmFsID0gcGFyYW1zLmNvbnRleHQuc3BsaXQoXCI6XCIpO1xyXG4gICAgICAgICAgICBsZXQgZnJvbSA9IDxUaW1lUGlja2VyPiB0aGlzLnBhZ2UuZ2V0Vmlld0J5SWQoXCJmcm9tXCIpO1xyXG4gICAgICAgICAgICBmcm9tLmhvdXIgPSBwYXJzZUludCh2YWxbMF0pO1xyXG4gICAgICAgICAgICBmcm9tLm1pbnV0ZSA9IHBhcnNlSW50KHZhbFsxXSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBwdWJsaWMgc3VibWl0KCkge1xyXG4gICAgICAgIHRoaXMucGFyYW1zLmNsb3NlQ2FsbGJhY2sodGhpcy50aW1lKTtcclxuICAgIH1cclxufSJdfQ==