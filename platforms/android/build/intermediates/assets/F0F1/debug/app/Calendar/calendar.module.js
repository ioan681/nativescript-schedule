"use strict";
var core_1 = require("@angular/core");
var forms_1 = require("nativescript-angular/forms");
var platform_1 = require("nativescript-angular/platform");
var datePicker_component_1 = require("./datePicker.component");
var modal_component_1 = require("./modal.component");
var year_component_1 = require('./year.component');
var month_component_1 = require('./month.component');
var week_component_1 = require('./week.component');
var calendar_service_1 = require('./calendar.service');
var calendar_routing_module_1 = require('./calendar-routing.module');
var CalendarModule = (function () {
    function CalendarModule() {
    }
    CalendarModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_1.NativeScriptModule,
                forms_1.NativeScriptFormsModule,
                calendar_routing_module_1.CalendarRoutingModule,
            ],
            declarations: [
                modal_component_1.ModalViewComponent,
                datePicker_component_1.DatePickerComponent,
                year_component_1.YearComponent,
                month_component_1.MonthComponent,
                week_component_1.WeekComponent,
            ],
            providers: [
                calendar_service_1.CalendarService
            ],
            entryComponents: [
                modal_component_1.ModalViewComponent,
                datePicker_component_1.DatePickerComponent
            ],
        }), 
        __metadata('design:paramtypes', [])
    ], CalendarModule);
    return CalendarModule;
}());
exports.CalendarModule = CalendarModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FsZW5kYXIubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vZGV2L0NhbGVuZGFyL2NhbGVuZGFyLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEscUJBQTJDLGVBQWUsQ0FBQyxDQUFBO0FBQzNELHNCQUF3Qyw0QkFBNEIsQ0FBQyxDQUFBO0FBQ3JFLHlCQUFtQywrQkFBK0IsQ0FBQyxDQUFBO0FBRW5FLHFDQUFvQyx3QkFBd0IsQ0FBQyxDQUFBO0FBQzdELGdDQUFtQyxtQkFBbUIsQ0FBQyxDQUFBO0FBQ3ZELCtCQUFnQyxrQkFBa0IsQ0FBQyxDQUFBO0FBQ25ELGdDQUFpQyxtQkFBbUIsQ0FBQyxDQUFBO0FBQ3JELCtCQUFnQyxrQkFBa0IsQ0FBQyxDQUFBO0FBQ25ELGlDQUErQixvQkFBb0IsQ0FBQyxDQUFBO0FBQ3BELHdDQUFxQywyQkFBMkIsQ0FBQyxDQUFBO0FBc0JqRTtJQUFBO0lBQTZCLENBQUM7SUFyQjlCO1FBQUMsZUFBUSxDQUFDO1lBQ1IsT0FBTyxFQUFFO2dCQUNQLDZCQUFrQjtnQkFDbEIsK0JBQXVCO2dCQUN2QiwrQ0FBcUI7YUFDdEI7WUFDRCxZQUFZLEVBQUU7Z0JBQ1osb0NBQWtCO2dCQUNsQiwwQ0FBbUI7Z0JBQ3BCLDhCQUFhO2dCQUNaLGdDQUFjO2dCQUNkLDhCQUFhO2FBQ2Q7WUFDRCxTQUFTLEVBQUU7Z0JBQ1Qsa0NBQWU7YUFDaEI7WUFDRCxlQUFlLEVBQUU7Z0JBQ2Ysb0NBQWtCO2dCQUNsQiwwQ0FBbUI7YUFDcEI7U0FDRixDQUFDOztzQkFBQTtJQUMyQixxQkFBQztBQUFELENBQUMsQUFBOUIsSUFBOEI7QUFBakIsc0JBQWMsaUJBQUcsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBOT19FUlJPUlNfU0NIRU1BIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZm9ybXNcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0TW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3BsYXRmb3JtXCI7XHJcblxyXG5pbXBvcnQgeyBEYXRlUGlja2VyQ29tcG9uZW50IH0gZnJvbSBcIi4vZGF0ZVBpY2tlci5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgTW9kYWxWaWV3Q29tcG9uZW50IH0gZnJvbSBcIi4vbW9kYWwuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IFllYXJDb21wb25lbnQgfSAgIGZyb20gJy4veWVhci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNb250aENvbXBvbmVudCB9ICAgZnJvbSAnLi9tb250aC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBXZWVrQ29tcG9uZW50IH0gICBmcm9tICcuL3dlZWsuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ2FsZW5kYXJTZXJ2aWNlfSBmcm9tICcuL2NhbGVuZGFyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDYWxlbmRhclJvdXRpbmdNb2R1bGV9IGZyb20gJy4vY2FsZW5kYXItcm91dGluZy5tb2R1bGUnO1xyXG5ATmdNb2R1bGUoe1xyXG4gIGltcG9ydHM6IFtcclxuICAgIE5hdGl2ZVNjcmlwdE1vZHVsZSxcclxuICAgIE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlLFxyXG4gICAgQ2FsZW5kYXJSb3V0aW5nTW9kdWxlLFxyXG4gIF0sXHJcbiAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICBNb2RhbFZpZXdDb21wb25lbnQsXHJcbiAgICBEYXRlUGlja2VyQ29tcG9uZW50LFxyXG5cdCAgWWVhckNvbXBvbmVudCxcclxuICAgIE1vbnRoQ29tcG9uZW50LFxyXG4gICAgV2Vla0NvbXBvbmVudCxcclxuICBdLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAgQ2FsZW5kYXJTZXJ2aWNlXHJcbiAgXSxcclxuICBlbnRyeUNvbXBvbmVudHM6IFtcclxuICAgIE1vZGFsVmlld0NvbXBvbmVudCxcclxuICAgIERhdGVQaWNrZXJDb21wb25lbnRcclxuICBdLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ2FsZW5kYXJNb2R1bGUge30iXX0=