"use strict";
var core_1 = require('@angular/core');
var element_registry_1 = require("nativescript-angular/element-registry");
var stack_layout_1 = require("ui/layouts/stack-layout");
var label_1 = require("ui/label");
var mapsModule = require("nativescript-google-maps-sdk");
var GPlaces = require("./google-places");
var searchBarModule = require("ui/search-bar");
// Important - must register MapView plugin in order to use in Angular templates
element_registry_1.registerElement("MapView", function () { return require("nativescript-google-maps-sdk").MapView; });
require('./rxjs-operators');
var MapComponent = (function () {
    function MapComponent() {
        //Map event
        this.onMapReady = function (args) {
            console.log("Map Ready");
            var mapView = args.object;
            console.log("Setting a marker...");
            var marker = new mapsModule.Marker();
            marker.position = mapsModule.Position.positionFromLatLng(-33.86, 151.20);
            marker.title = "Sydney";
            marker.snippet = "Australia";
            marker.userData = { index: 1 };
            mapView.addMarker(marker);
        };
    }
    MapComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.search = this.searchEl.nativeElement;
        this.conatiner = this.cont.nativeElement;
        GPlaces.init({
            googleServerApiKey: 'AIzaSyBDV_BgWWAwcsgAl5YnYVTujE_oELsPtmM',
            language: 'es',
            radius: '500',
            location: '42.663840,23.314392',
            errorCallback: function (text) { console.log(text); }
        });
        var searchBar = new searchBarModule.SearchBar();
        this.search.on(searchBarModule.SearchBar.submitEvent, function (args) {
            _this.keyup(args.object.text);
        });
        this.search.on(searchBarModule.SearchBar.clearEvent, function (args) {
            console.log("Clear");
        });
    };
    MapComponent.prototype.keyup = function (text) {
        var _this = this;
        if (this.conatiner.getChildAt(0) != undefined)
            this.conatiner.removeChild(this.conatiner.getChildAt(0));
        GPlaces.search(text).then(function (result) {
            result = JSON.parse(result);
            var stack = new stack_layout_1.StackLayout();
            for (var _i = 0, result_1 = result; _i < result_1.length; _i++) {
                var item = result_1[_i];
                var label = new label_1.Label();
                label.className = "google-place";
                label.text = item.description;
                label.textWrap = true;
                // label.style.width =    ;
                stack.addChild(label);
                console.log(item.description);
            }
            _this.conatiner.addChild(stack);
        });
    };
    __decorate([
        core_1.ViewChild("MapView"), 
        __metadata('design:type', core_1.ElementRef)
    ], MapComponent.prototype, "mapView", void 0);
    __decorate([
        core_1.ViewChild("search"), 
        __metadata('design:type', core_1.ElementRef)
    ], MapComponent.prototype, "searchEl", void 0);
    __decorate([
        core_1.ViewChild("container"), 
        __metadata('design:type', core_1.ElementRef)
    ], MapComponent.prototype, "cont", void 0);
    MapComponent = __decorate([
        core_1.Component({
            selector: 'map-component',
            template: "\n    <StackLayout id=\"map-modal\">\n        <AbsoluteLayout>\n            <GridLayout rows=\"50,*\">\n                <SearchBar row=\"0\" style=\"height:50px;\" #search (tap)=\"keyup()\">Da</SearchBar>\n                <MapView row=\"1\" (mapReady)=\"onMapReady($event)\" style=\"height:500px;\"></MapView>\n            </GridLayout>\n            <StackLayout #container top=\"50\" width=\"100%\">\n            </StackLayout>\n        </AbsoluteLayout>\n    </StackLayout>\n    "
        }), 
        __metadata('design:paramtypes', [])
    ], MapComponent);
    return MapComponent;
}());
exports.MapComponent = MapComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL2Rldi9tYXAuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxxQkFBK0MsZUFBZSxDQUFDLENBQUE7QUFDL0QsaUNBQThCLHVDQUF1QyxDQUFDLENBQUE7QUFDdEUsNkJBQTRCLHlCQUF5QixDQUFDLENBQUE7QUFDdEQsc0JBQXNCLFVBQVUsQ0FBQyxDQUFBO0FBQ2pDLElBQUksVUFBVSxHQUFHLE9BQU8sQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO0FBQ3pELElBQUksT0FBTyxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0FBQ3pDLElBQU8sZUFBZSxXQUFXLGVBQWUsQ0FBQyxDQUFDO0FBQ2xELGdGQUFnRjtBQUNoRixrQ0FBZSxDQUFDLFNBQVMsRUFBRSxjQUFNLE9BQUEsT0FBTyxDQUFDLDhCQUE4QixDQUFDLENBQUMsT0FBTyxFQUEvQyxDQUErQyxDQUFDLENBQUM7QUFDakYsUUFBTyxrQkFBa0IsQ0FBQyxDQUFBO0FBZ0IzQjtJQUFBO1FBNENJLFdBQVc7UUFDWCxlQUFVLEdBQUcsVUFBQyxJQUFJO1lBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN6QixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUNuQyxJQUFJLE1BQU0sR0FBRyxJQUFJLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNyQyxNQUFNLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDekUsTUFBTSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUM7WUFDeEIsTUFBTSxDQUFDLE9BQU8sR0FBRyxXQUFXLENBQUM7WUFDN0IsTUFBTSxDQUFDLFFBQVEsR0FBRyxFQUFFLEtBQUssRUFBRyxDQUFDLEVBQUMsQ0FBQztZQUMvQixPQUFPLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzlCLENBQUMsQ0FBQztJQUNOLENBQUM7SUFqREcsK0JBQVEsR0FBUjtRQUFBLGlCQWtCQztRQWhCRyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDO1FBQzFDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUM7UUFDekMsT0FBTyxDQUFDLElBQUksQ0FBQztZQUNULGtCQUFrQixFQUFFLHlDQUF5QztZQUM3RCxRQUFRLEVBQUUsSUFBSTtZQUNkLE1BQU0sRUFBRSxLQUFLO1lBQ2IsUUFBUSxFQUFFLHFCQUFxQjtZQUMvQixhQUFhLEVBQUUsVUFBUyxJQUFJLElBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQSxDQUFBLENBQUM7U0FDbkQsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxTQUFTLEdBQUcsSUFBSSxlQUFlLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDaEQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUMsVUFBQyxJQUFTO1lBQzNELEtBQUksQ0FBQyxLQUFLLENBQTZCLElBQUksQ0FBQyxNQUFPLENBQUMsSUFBSSxDQUFDLENBQUE7UUFDN0QsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLFVBQVUsRUFBRSxVQUFVLElBQVM7WUFDcEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN6QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDRCw0QkFBSyxHQUFMLFVBQU0sSUFBVztRQUFqQixpQkFpQkM7UUFoQkcsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUUsU0FBUyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDN0QsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxNQUFNO1lBQzdCLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzVCLElBQUksS0FBSyxHQUFHLElBQUksMEJBQVcsRUFBRSxDQUFDO1lBQzlCLEdBQUcsQ0FBQSxDQUFhLFVBQU0sRUFBTixpQkFBTSxFQUFOLG9CQUFNLEVBQU4sSUFBTSxDQUFDO2dCQUFuQixJQUFJLElBQUksZUFBQTtnQkFDUixJQUFJLEtBQUssR0FBRyxJQUFJLGFBQUssRUFBRSxDQUFDO2dCQUN4QixLQUFLLENBQUMsU0FBUyxHQUFHLGNBQWMsQ0FBQztnQkFDakMsS0FBSyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO2dCQUM5QixLQUFLLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztnQkFDdEIsMkJBQTJCO2dCQUMzQixLQUFLLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUNqQztZQUNELEtBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ25DLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQXZDRDtRQUFDLGdCQUFTLENBQUMsU0FBUyxDQUFDOztpREFBQTtJQUNyQjtRQUFDLGdCQUFTLENBQUMsUUFBUSxDQUFDOztrREFBQTtJQUNwQjtRQUFDLGdCQUFTLENBQUMsV0FBVyxDQUFDOzs4Q0FBQTtJQXJCM0I7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLGVBQWU7WUFDekIsUUFBUSxFQUFFLG1lQVdUO1NBQ0osQ0FBQzs7b0JBQUE7SUF5REYsbUJBQUM7QUFBRCxDQUFDLEFBeERELElBd0RDO0FBeERZLG9CQUFZLGVBd0R4QixDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIEVsZW1lbnRSZWYsIFZpZXdDaGlsZH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7cmVnaXN0ZXJFbGVtZW50fSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZWxlbWVudC1yZWdpc3RyeVwiO1xyXG5pbXBvcnQgeyBTdGFja0xheW91dCB9IGZyb20gXCJ1aS9sYXlvdXRzL3N0YWNrLWxheW91dFwiO1xyXG5pbXBvcnQgeyBMYWJlbCB9IGZyb20gXCJ1aS9sYWJlbFwiO1xyXG52YXIgbWFwc01vZHVsZSA9IHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtZ29vZ2xlLW1hcHMtc2RrXCIpO1xyXG52YXIgR1BsYWNlcyA9IHJlcXVpcmUoXCIuL2dvb2dsZS1wbGFjZXNcIik7XHJcbmltcG9ydCBzZWFyY2hCYXJNb2R1bGUgPSByZXF1aXJlKFwidWkvc2VhcmNoLWJhclwiKTtcclxuLy8gSW1wb3J0YW50IC0gbXVzdCByZWdpc3RlciBNYXBWaWV3IHBsdWdpbiBpbiBvcmRlciB0byB1c2UgaW4gQW5ndWxhciB0ZW1wbGF0ZXNcclxucmVnaXN0ZXJFbGVtZW50KFwiTWFwVmlld1wiLCAoKSA9PiByZXF1aXJlKFwibmF0aXZlc2NyaXB0LWdvb2dsZS1tYXBzLXNka1wiKS5NYXBWaWV3KTtcclxuIGltcG9ydCAnLi9yeGpzLW9wZXJhdG9ycyc7XHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtYXAtY29tcG9uZW50JyxcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICA8U3RhY2tMYXlvdXQgaWQ9XCJtYXAtbW9kYWxcIj5cclxuICAgICAgICA8QWJzb2x1dGVMYXlvdXQ+XHJcbiAgICAgICAgICAgIDxHcmlkTGF5b3V0IHJvd3M9XCI1MCwqXCI+XHJcbiAgICAgICAgICAgICAgICA8U2VhcmNoQmFyIHJvdz1cIjBcIiBzdHlsZT1cImhlaWdodDo1MHB4O1wiICNzZWFyY2ggKHRhcCk9XCJrZXl1cCgpXCI+RGE8L1NlYXJjaEJhcj5cclxuICAgICAgICAgICAgICAgIDxNYXBWaWV3IHJvdz1cIjFcIiAobWFwUmVhZHkpPVwib25NYXBSZWFkeSgkZXZlbnQpXCIgc3R5bGU9XCJoZWlnaHQ6NTAwcHg7XCI+PC9NYXBWaWV3PlxyXG4gICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XHJcbiAgICAgICAgICAgIDxTdGFja0xheW91dCAjY29udGFpbmVyIHRvcD1cIjUwXCIgd2lkdGg9XCIxMDAlXCI+XHJcbiAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XHJcbiAgICAgICAgPC9BYnNvbHV0ZUxheW91dD5cclxuICAgIDwvU3RhY2tMYXlvdXQ+XHJcbiAgICBgXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNYXBDb21wb25lbnQge1xyXG4gICAgcHVibGljIHNlYXJjaDphbnk7XHJcbiAgICBwdWJsaWMgY29uYXRpbmVyOlN0YWNrTGF5b3V0O1xyXG4gICAgcHVibGljIHN0YWNrOiBTdGFja0xheW91dDtcclxuICAgIEBWaWV3Q2hpbGQoXCJNYXBWaWV3XCIpIG1hcFZpZXc6IEVsZW1lbnRSZWY7XHJcbiAgICBAVmlld0NoaWxkKFwic2VhcmNoXCIpIHNlYXJjaEVsOiBFbGVtZW50UmVmO1xyXG4gICAgQFZpZXdDaGlsZChcImNvbnRhaW5lclwiKSBjb250OiBFbGVtZW50UmVmO1xyXG4gICAgbmdPbkluaXQoKXtcclxuICAgICAgICBcclxuICAgICAgICB0aGlzLnNlYXJjaCA9IHRoaXMuc2VhcmNoRWwubmF0aXZlRWxlbWVudDtcclxuICAgICAgICB0aGlzLmNvbmF0aW5lciA9IHRoaXMuY29udC5uYXRpdmVFbGVtZW50O1xyXG4gICAgICAgIEdQbGFjZXMuaW5pdCh7XHJcbiAgICAgICAgICAgIGdvb2dsZVNlcnZlckFwaUtleTogJ0FJemFTeUJEVl9CZ1dXQXdjc2dBbDVZbllWVHVqRV9vRUxzUHRtTScsXHJcbiAgICAgICAgICAgIGxhbmd1YWdlOiAnZXMnLFxyXG4gICAgICAgICAgICByYWRpdXM6ICc1MDAnLFxyXG4gICAgICAgICAgICBsb2NhdGlvbjogJzQyLjY2Mzg0MCwyMy4zMTQzOTInLFxyXG4gICAgICAgICAgICBlcnJvckNhbGxiYWNrOiBmdW5jdGlvbih0ZXh0KXtjb25zb2xlLmxvZyh0ZXh0KX1cclxuICAgICAgICB9KTtcclxuICAgICAgICB2YXIgc2VhcmNoQmFyID0gbmV3IHNlYXJjaEJhck1vZHVsZS5TZWFyY2hCYXIoKTtcclxuICAgICAgICB0aGlzLnNlYXJjaC5vbihzZWFyY2hCYXJNb2R1bGUuU2VhcmNoQmFyLnN1Ym1pdEV2ZW50LChhcmdzOiBhbnkpPT57IFxyXG4gICAgICAgICAgICB0aGlzLmtleXVwKCg8c2VhcmNoQmFyTW9kdWxlLlNlYXJjaEJhcj5hcmdzLm9iamVjdCkudGV4dClcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnNlYXJjaC5vbihzZWFyY2hCYXJNb2R1bGUuU2VhcmNoQmFyLmNsZWFyRXZlbnQsIGZ1bmN0aW9uIChhcmdzOiBhbnkpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJDbGVhclwiKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGtleXVwKHRleHQ6c3RyaW5nKXtcclxuICAgICAgICBpZih0aGlzLmNvbmF0aW5lci5nZXRDaGlsZEF0KDApIT11bmRlZmluZWQpXHJcbiAgICAgICAgICAgIHRoaXMuY29uYXRpbmVyLnJlbW92ZUNoaWxkKHRoaXMuY29uYXRpbmVyLmdldENoaWxkQXQoMCkpO1xyXG4gICAgICAgIEdQbGFjZXMuc2VhcmNoKHRleHQpLnRoZW4oKHJlc3VsdCk9PntcclxuICAgICAgICAgICAgcmVzdWx0ID0gSlNPTi5wYXJzZShyZXN1bHQpO1xyXG4gICAgICAgICAgICB2YXIgc3RhY2sgPSBuZXcgU3RhY2tMYXlvdXQoKTtcclxuICAgICAgICAgICAgZm9yKGxldCBpdGVtIG9mIHJlc3VsdCl7XHJcbiAgICAgICAgICAgICAgICBsZXQgbGFiZWwgPSBuZXcgTGFiZWwoKTtcclxuICAgICAgICAgICAgICAgIGxhYmVsLmNsYXNzTmFtZSA9IFwiZ29vZ2xlLXBsYWNlXCI7XHJcbiAgICAgICAgICAgICAgICBsYWJlbC50ZXh0ID0gaXRlbS5kZXNjcmlwdGlvbjtcclxuICAgICAgICAgICAgICAgIGxhYmVsLnRleHRXcmFwID0gdHJ1ZTsgXHJcbiAgICAgICAgICAgICAgICAvLyBsYWJlbC5zdHlsZS53aWR0aCA9ICAgIDtcclxuICAgICAgICAgICAgICAgIHN0YWNrLmFkZENoaWxkKGxhYmVsKTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGl0ZW0uZGVzY3JpcHRpb24pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuY29uYXRpbmVyLmFkZENoaWxkKHN0YWNrKTtcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG4gICAgLy9NYXAgZXZlbnRcclxuICAgIG9uTWFwUmVhZHkgPSAoYXJncykgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiTWFwIFJlYWR5XCIpO1xyXG4gICAgICAgIHZhciBtYXBWaWV3ID0gYXJncy5vYmplY3Q7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJTZXR0aW5nIGEgbWFya2VyLi4uXCIpO1xyXG4gICAgICAgIHZhciBtYXJrZXIgPSBuZXcgbWFwc01vZHVsZS5NYXJrZXIoKTtcclxuICAgICAgICBtYXJrZXIucG9zaXRpb24gPSBtYXBzTW9kdWxlLlBvc2l0aW9uLnBvc2l0aW9uRnJvbUxhdExuZygtMzMuODYsIDE1MS4yMCk7XHJcbiAgICAgICAgbWFya2VyLnRpdGxlID0gXCJTeWRuZXlcIjtcclxuICAgICAgICBtYXJrZXIuc25pcHBldCA9IFwiQXVzdHJhbGlhXCI7XHJcbiAgICAgICAgbWFya2VyLnVzZXJEYXRhID0geyBpbmRleCA6IDF9O1xyXG4gICAgICAgIG1hcFZpZXcuYWRkTWFya2VyKG1hcmtlcik7XHJcbiAgICB9O1xyXG59Il19