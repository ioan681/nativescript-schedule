"use strict";
var core_1 = require("@angular/core");
var forms_1 = require("nativescript-angular/forms");
var platform_1 = require("nativescript-angular/platform");
var http_1 = require("nativescript-angular/http");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var auth_service_1 = require('./login/auth.service');
var auth_guard_1 = require('./login/auth.guard');
var app_component_1 = require("./app.component");
var map_component_1 = require("./map.component");
var login_component_1 = require("./login/login.component");
var signup_component_1 = require("./login/signup.component");
var calendar_module_1 = require('./Calendar/calendar.module');
var app_routing_module_1 = require('./app-routing.module');
require("nativescript-localstorage");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_1.NativeScriptModule,
                http_1.NativeScriptHttpModule,
                forms_1.NativeScriptFormsModule,
                app_routing_module_1.AppRoutingModule,
                calendar_module_1.CalendarModule,
            ],
            declarations: [
                login_component_1.LoginComponent,
                signup_component_1.SignUpComponent,
                app_component_1.AppComponent,
                map_component_1.MapComponent
            ],
            entryComponents: [
                map_component_1.MapComponent,
            ],
            bootstrap: [app_component_1.AppComponent],
            providers: [auth_service_1.AuthService, auth_guard_1.AuthGuard, modal_dialog_1.ModalDialogService],
            schemas: [core_1.NO_ERRORS_SCHEMA]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL2Rldi9hcHAubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxxQkFBMkMsZUFBZSxDQUFDLENBQUE7QUFDM0Qsc0JBQXdDLDRCQUE0QixDQUFDLENBQUE7QUFDckUseUJBQW1DLCtCQUErQixDQUFDLENBQUE7QUFDbkUscUJBQXVDLDJCQUEyQixDQUFDLENBQUE7QUFFbkUsNkJBQW1DLG1DQUFtQyxDQUFDLENBQUE7QUFFdkUsNkJBQStCLHNCQUFzQixDQUFDLENBQUE7QUFDdEQsMkJBQTBCLG9CQUFvQixDQUFDLENBQUE7QUFFL0MsOEJBQTZCLGlCQUFpQixDQUFDLENBQUE7QUFDL0MsOEJBQTZCLGlCQUFpQixDQUFDLENBQUE7QUFDL0MsZ0NBQStCLHlCQUF5QixDQUFDLENBQUE7QUFDekQsaUNBQWdDLDBCQUEwQixDQUFDLENBQUE7QUFDM0QsZ0NBQStCLDRCQUE0QixDQUFDLENBQUE7QUFDNUQsbUNBQXdDLHNCQUFzQixDQUFDLENBQUE7QUFDL0QsT0FBTyxDQUFFLDJCQUEyQixDQUFFLENBQUM7QUFzQnZDO0lBQUE7SUFBeUIsQ0FBQztJQXJCMUI7UUFBQyxlQUFRLENBQUM7WUFDTixPQUFPLEVBQUU7Z0JBQ0wsNkJBQWtCO2dCQUNsQiw2QkFBc0I7Z0JBQ3RCLCtCQUF1QjtnQkFDdkIscUNBQWdCO2dCQUNoQixnQ0FBYzthQUNiO1lBQ0wsWUFBWSxFQUFFO2dCQUNWLGdDQUFjO2dCQUNkLGtDQUFlO2dCQUNmLDRCQUFZO2dCQUNaLDRCQUFZO2FBQ1g7WUFDTCxlQUFlLEVBQUU7Z0JBQ2IsNEJBQVk7YUFDZjtZQUNELFNBQVMsRUFBRSxDQUFDLDRCQUFZLENBQUM7WUFDekIsU0FBUyxFQUFFLENBQUMsMEJBQVcsRUFBQyxzQkFBUyxFQUFFLGlDQUFrQixDQUFDO1lBQ3RELE9BQU8sRUFBRSxDQUFDLHVCQUFnQixDQUFDO1NBQzlCLENBQUM7O2lCQUFBO0lBQ3VCLGdCQUFDO0FBQUQsQ0FBQyxBQUExQixJQUEwQjtBQUFiLGlCQUFTLFlBQUksQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBOT19FUlJPUlNfU0NIRU1BIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZm9ybXNcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0TW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3BsYXRmb3JtXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdEh0dHBNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvaHR0cFwiO1xyXG5cclxuaW1wb3J0IHsgTW9kYWxEaWFsb2dTZXJ2aWNlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL21vZGFsLWRpYWxvZ1wiO1xyXG5cclxuaW1wb3J0IHsgQXV0aFNlcnZpY2V9IFx0XHRcdFx0ZnJvbSAnLi9sb2dpbi9hdXRoLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBdXRoR3VhcmQgfSBmcm9tICcuL2xvZ2luL2F1dGguZ3VhcmQnO1xyXG5cclxuaW1wb3J0IHsgQXBwQ29tcG9uZW50IH0gZnJvbSBcIi4vYXBwLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBNYXBDb21wb25lbnQgfSBmcm9tIFwiLi9tYXAuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IExvZ2luQ29tcG9uZW50IH0gZnJvbSBcIi4vbG9naW4vbG9naW4uY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IFNpZ25VcENvbXBvbmVudCB9IGZyb20gXCIuL2xvZ2luL3NpZ251cC5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgQ2FsZW5kYXJNb2R1bGUgfSBmcm9tICcuL0NhbGVuZGFyL2NhbGVuZGFyLm1vZHVsZSc7XHJcbmltcG9ydCB7IEFwcFJvdXRpbmdNb2R1bGUgfSAgICAgICAgZnJvbSAnLi9hcHAtcm91dGluZy5tb2R1bGUnO1xyXG5yZXF1aXJlKCBcIm5hdGl2ZXNjcmlwdC1sb2NhbHN0b3JhZ2VcIiApO1xyXG5ATmdNb2R1bGUoe1xyXG4gICAgaW1wb3J0czogW1xyXG4gICAgICAgIE5hdGl2ZVNjcmlwdE1vZHVsZSxcclxuICAgICAgICBOYXRpdmVTY3JpcHRIdHRwTW9kdWxlLFxyXG4gICAgICAgIE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlLFxyXG4gICAgICAgIEFwcFJvdXRpbmdNb2R1bGUsXHJcbiAgICAgICAgQ2FsZW5kYXJNb2R1bGUsXHJcbiAgICAgICAgXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIExvZ2luQ29tcG9uZW50LFxyXG4gICAgICAgIFNpZ25VcENvbXBvbmVudCxcclxuICAgICAgICBBcHBDb21wb25lbnQsXHJcbiAgICAgICAgTWFwQ29tcG9uZW50XHJcbiAgICAgICAgXSxcclxuICAgIGVudHJ5Q29tcG9uZW50czogW1xyXG4gICAgICAgIE1hcENvbXBvbmVudCxcclxuICAgIF0sXHJcbiAgICBib290c3RyYXA6IFtBcHBDb21wb25lbnRdLFxyXG4gICAgcHJvdmlkZXJzOiBbQXV0aFNlcnZpY2UsQXV0aEd1YXJkLCBNb2RhbERpYWxvZ1NlcnZpY2VdLFxyXG4gICAgc2NoZW1hczogW05PX0VSUk9SU19TQ0hFTUFdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBcHBNb2R1bGUgeyB9XHJcbiJdfQ==