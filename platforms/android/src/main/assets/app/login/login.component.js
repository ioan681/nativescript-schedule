"use strict";
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var auth_service_1 = require('./auth.service');
//
var LoginComponent = (function () {
    function LoginComponent(router, auth) {
        this.router = router;
        this.auth = auth;
        this.model = {};
        this.loading = false;
        this.error = '';
    }
    LoginComponent.prototype.getUser = function () {
        this.auth.getUser().subscribe(function (result) {
            console.log(result["_body"]);
            for (var da in result["_body"]) {
                console.log(da + " " + result["_body"][da]);
            }
        });
    };
    LoginComponent.prototype.ngOnInit = function () {
        //this.getUser();
        // reset login status
        this.auth.logout();
        // document.getElementById('icons').appendChild(document.getElementById('my-signin3'))
        // document.getElementById('my-signin3').style.display="inline";
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.loading = true;
        this.auth.login(this.model.email, this.model.pass)
            .subscribe(function (result) {
            if (result === true) {
                _this.router.navigate(['/month/1']);
            }
            else {
                _this.error = 'Username or password is incorrect';
                _this.loading = false;
            }
        }, function (error) {
            //this.error = 'Username or password is incorrect';
            _this.loading = false;
            console.log("da");
            console.log(_this.error);
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            templateUrl: 'view/login.component.html',
            styleUrls: ['css/login.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, auth_service_1.AuthService])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vZGV2L2xvZ2luL2xvZ2luLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEscUJBQWtDLGVBQWUsQ0FBQyxDQUFBO0FBQ2xELHVCQUF1QixpQkFBaUIsQ0FBQyxDQUFBO0FBQ3pDLDZCQUEyQixnQkFBZ0IsQ0FBQyxDQUFBO0FBQzVDLEVBQUU7QUFPRjtJQUtJLHdCQUFvQixNQUFjLEVBQVUsSUFBaUI7UUFBekMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUFVLFNBQUksR0FBSixJQUFJLENBQWE7UUFKN0QsVUFBSyxHQUFRLEVBQUUsQ0FBQztRQUNoQixZQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ2hCLFVBQUssR0FBRyxFQUFFLENBQUM7SUFFcUQsQ0FBQztJQUNqRSxnQ0FBTyxHQUFQO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxNQUFNO1lBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUE7WUFDNUIsR0FBRyxDQUFBLENBQUMsSUFBSSxFQUFFLElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUEsQ0FBQztnQkFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUMsR0FBRyxHQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFBO1lBQzNDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDRCxpQ0FBUSxHQUFSO1FBQ0ksaUJBQWlCO1FBQ2pCLHFCQUFxQjtRQUNoQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3hCLHNGQUFzRjtRQUN0RixnRUFBZ0U7SUFDcEUsQ0FBQztJQUVELDhCQUFLLEdBQUw7UUFBQSxpQkFpQkM7UUFoQkcsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7YUFDN0MsU0FBUyxDQUFDLFVBQUEsTUFBTTtZQUNiLEVBQUUsQ0FBQyxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNsQixLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDdkMsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLEtBQUksQ0FBQyxLQUFLLEdBQUcsbUNBQW1DLENBQUM7Z0JBQ2pELEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3pCLENBQUM7UUFDTCxDQUFDLEVBQ0QsVUFBQSxLQUFLO1lBQ0csbURBQW1EO1lBQ25ELEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3JCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7WUFDakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDM0IsQ0FBQyxDQUFDLENBQUM7SUFDZixDQUFDO0lBNUNMO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFdBQVcsRUFBRSwyQkFBMkI7WUFDeEMsU0FBUyxFQUFFLENBQUMseUJBQXlCLENBQUM7U0FDekMsQ0FBQzs7c0JBQUE7SUEwQ0YscUJBQUM7QUFBRCxDQUFDLEFBeENELElBd0NDO0FBeENZLHNCQUFjLGlCQXdDMUIsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IEF1dGhTZXJ2aWNlfSBmcm9tICcuL2F1dGguc2VydmljZSc7XHJcbi8vXHJcbiBcclxuQENvbXBvbmVudCh7XHJcbiAgICB0ZW1wbGF0ZVVybDogJ3ZpZXcvbG9naW4uY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJ2Nzcy9sb2dpbi5jb21wb25lbnQuY3NzJ11cclxufSlcclxuIFxyXG5leHBvcnQgY2xhc3MgTG9naW5Db21wb25lbnR7XHJcbiAgICBtb2RlbDogYW55ID0ge307XHJcbiAgICBsb2FkaW5nID0gZmFsc2U7XHJcbiAgICBlcnJvciA9ICcnO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsIHByaXZhdGUgYXV0aDogQXV0aFNlcnZpY2UpIHt9XHJcbiAgICBnZXRVc2VyKCkge1xyXG4gICAgICAgIHRoaXMuYXV0aC5nZXRVc2VyKCkuc3Vic2NyaWJlKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3VsdFtcIl9ib2R5XCJdKVxyXG4gICAgICAgICAgICBmb3IobGV0IGRhIGluIHJlc3VsdFtcIl9ib2R5XCJdKXtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhK1wiIFwiK3Jlc3VsdFtcIl9ib2R5XCJdW2RhXSlcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgLy90aGlzLmdldFVzZXIoKTtcclxuICAgICAgICAvLyByZXNldCBsb2dpbiBzdGF0dXNcclxuICAgICAgICAgICAgIHRoaXMuYXV0aC5sb2dvdXQoKTtcclxuICAgICAgICAvLyBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnaWNvbnMnKS5hcHBlbmRDaGlsZChkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbXktc2lnbmluMycpKVxyXG4gICAgICAgIC8vIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdteS1zaWduaW4zJykuc3R5bGUuZGlzcGxheT1cImlubGluZVwiO1xyXG4gICAgfVxyXG5cclxuICAgIGxvZ2luKCkge1xyXG4gICAgICAgIHRoaXMubG9hZGluZyA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5hdXRoLmxvZ2luKHRoaXMubW9kZWwuZW1haWwsIHRoaXMubW9kZWwucGFzcylcclxuICAgICAgICAgICAgLnN1YnNjcmliZShyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3VsdCA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL21vbnRoLzEnXSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZXJyb3IgPSAnVXNlcm5hbWUgb3IgcGFzc3dvcmQgaXMgaW5jb3JyZWN0JztcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vdGhpcy5lcnJvciA9ICdVc2VybmFtZSBvciBwYXNzd29yZCBpcyBpbmNvcnJlY3QnO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiZGFcIilcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmVycm9yKVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcbn0iXX0=