"use strict";
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var YearComponent = (function () {
    function YearComponent(router) {
        this.router = router;
    }
    YearComponent.prototype.go = function (id) {
        var link = ['/user', id];
        this.router.navigate(link);
    };
    YearComponent = __decorate([
        core_1.Component({
            selector: 'year-calendar',
            templateUrl: 'view/calendar/year.component.html',
            styleUrls: ['css/year.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router])
    ], YearComponent);
    return YearComponent;
}());
exports.YearComponent = YearComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoieWVhci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9kZXYvQ2FsZW5kYXIveWVhci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHFCQUEwQixlQUFlLENBQUMsQ0FBQTtBQUMxQyx1QkFBdUIsaUJBQWlCLENBQUMsQ0FBQTtBQU16QztJQUNFLHVCQUFvQixNQUFjO1FBQWQsV0FBTSxHQUFOLE1BQU0sQ0FBUTtJQUFHLENBQUM7SUFDdEMsMEJBQUUsR0FBRixVQUFHLEVBQVM7UUFDVixJQUFJLElBQUksR0FBRyxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztRQUN6QixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM3QixDQUFDO0lBVkg7UUFBQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGVBQWU7WUFDekIsV0FBVyxFQUFFLG1DQUFtQztZQUNoRCxTQUFTLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQztTQUN0QyxDQUFDOztxQkFBQTtJQU9GLG9CQUFDO0FBQUQsQ0FBQyxBQU5ELElBTUM7QUFOWSxxQkFBYSxnQkFNekIsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAneWVhci1jYWxlbmRhcicsXHJcbiAgdGVtcGxhdGVVcmw6ICd2aWV3L2NhbGVuZGFyL3llYXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWydjc3MveWVhci5jb21wb25lbnQuY3NzJ10gXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBZZWFyQ29tcG9uZW50IHsgXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXI6IFJvdXRlcikge31cclxuICBnbyhpZDpudW1iZXIpe1xyXG4gICAgbGV0IGxpbmsgPSBbJy91c2VyJywgaWRdO1xyXG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUobGluayk7XHJcbiAgfVxyXG59XHJcbiJdfQ==