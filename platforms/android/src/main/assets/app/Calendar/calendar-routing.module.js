"use strict";
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var auth_guard_1 = require('../login/auth.guard');
var year_component_1 = require('./year.component');
var month_component_1 = require('./month.component');
var week_component_1 = require('./week.component');
//import { NavigationComponent } from '../navigation.component';
var CalendarRoutes = [
    { path: '',
        //component: NavigationComponent
        canActivate: [auth_guard_1.AuthGuard],
        children: [
            { path: 'year', component: year_component_1.YearComponent },
            { path: '', component: week_component_1.WeekComponent },
            { path: 'month/:id', component: month_component_1.MonthComponent },
            { path: 'week/:id', component: week_component_1.WeekComponent },
        ]
    },
];
var CalendarRoutingModule = (function () {
    function CalendarRoutingModule() {
    }
    CalendarRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.NativeScriptRouterModule.forChild(CalendarRoutes)
            ],
            exports: [
                router_1.NativeScriptRouterModule
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], CalendarRoutingModule);
    return CalendarRoutingModule;
}());
exports.CalendarRoutingModule = CalendarRoutingModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FsZW5kYXItcm91dGluZy5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9kZXYvQ2FsZW5kYXIvY2FsZW5kYXItcm91dGluZy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHFCQUEyQyxlQUFlLENBQUMsQ0FBQTtBQUMzRCx1QkFBeUMsNkJBQTZCLENBQUMsQ0FBQTtBQUN2RSwyQkFBMEIscUJBQXFCLENBQUMsQ0FBQTtBQUNoRCwrQkFBZ0Msa0JBQWtCLENBQUMsQ0FBQTtBQUNuRCxnQ0FBaUMsbUJBQW1CLENBQUMsQ0FBQTtBQUNyRCwrQkFBZ0Msa0JBQWtCLENBQUMsQ0FBQTtBQUNuRCxnRUFBZ0U7QUFDaEUsSUFBTSxjQUFjLEdBQUc7SUFFckIsRUFBRSxJQUFJLEVBQUUsRUFBRTtRQUNSLGdDQUFnQztRQUNoQyxXQUFXLEVBQUUsQ0FBQyxzQkFBUyxDQUFDO1FBQ3hCLFFBQVEsRUFBRTtZQUNSLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsOEJBQWEsRUFBRTtZQUMxQyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLDhCQUFhLEVBQUU7WUFDdEMsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxnQ0FBYyxFQUFFO1lBQ2hELEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsOEJBQWEsRUFBRTtTQUUvQztLQUNGO0NBQ0YsQ0FBQztBQVVGO0lBQUE7SUFBcUMsQ0FBQztJQVJ0QztRQUFDLGVBQVEsQ0FBQztZQUNSLE9BQU8sRUFBRTtnQkFDUCxpQ0FBd0IsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDO2FBQ2xEO1lBQ0QsT0FBTyxFQUFFO2dCQUNQLGlDQUF3QjthQUN6QjtTQUNGLENBQUM7OzZCQUFBO0lBQ21DLDRCQUFDO0FBQUQsQ0FBQyxBQUF0QyxJQUFzQztBQUF6Qiw2QkFBcUIsd0JBQUksQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBOT19FUlJPUlNfU0NIRU1BIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBBdXRoR3VhcmQgfSBmcm9tICcuLi9sb2dpbi9hdXRoLmd1YXJkJztcclxuaW1wb3J0IHsgWWVhckNvbXBvbmVudCB9ICAgZnJvbSAnLi95ZWFyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1vbnRoQ29tcG9uZW50IH0gICBmcm9tICcuL21vbnRoLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFdlZWtDb21wb25lbnQgfSAgIGZyb20gJy4vd2Vlay5jb21wb25lbnQnO1xyXG4vL2ltcG9ydCB7IE5hdmlnYXRpb25Db21wb25lbnQgfSBmcm9tICcuLi9uYXZpZ2F0aW9uLmNvbXBvbmVudCc7XHJcbmNvbnN0IENhbGVuZGFyUm91dGVzID0gW1xyXG4gIFxyXG4gIHsgcGF0aDogJycsXHJcbiAgICAvL2NvbXBvbmVudDogTmF2aWdhdGlvbkNvbXBvbmVudFxyXG4gICAgY2FuQWN0aXZhdGU6IFtBdXRoR3VhcmRdLFxyXG4gICAgY2hpbGRyZW46IFtcclxuICAgICAgeyBwYXRoOiAneWVhcicsIGNvbXBvbmVudDogWWVhckNvbXBvbmVudCB9LFxyXG4gICAgICB7IHBhdGg6ICcnLCBjb21wb25lbnQ6IFdlZWtDb21wb25lbnQgfSxcclxuICAgICAgeyBwYXRoOiAnbW9udGgvOmlkJywgY29tcG9uZW50OiBNb250aENvbXBvbmVudCB9LFxyXG4gICAgICB7IHBhdGg6ICd3ZWVrLzppZCcsIGNvbXBvbmVudDogV2Vla0NvbXBvbmVudCB9LFxyXG4gICAgICAvLyB7IHBhdGg6ICcnLCBjb21wb25lbnQ6IFllYXJDb21wb25lbnQgfSxcclxuICAgIF0gXHJcbiAgfSxcclxuXTtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgaW1wb3J0czogW1xyXG4gICAgTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlLmZvckNoaWxkKENhbGVuZGFyUm91dGVzKVxyXG4gIF0sXHJcbiAgZXhwb3J0czogW1xyXG4gICAgTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlXHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ2FsZW5kYXJSb3V0aW5nTW9kdWxlIHsgfSJdfQ==