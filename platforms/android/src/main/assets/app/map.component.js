"use strict";
var core_1 = require('@angular/core');
var element_registry_1 = require("nativescript-angular/element-registry");
var stack_layout_1 = require("ui/layouts/stack-layout");
var label_1 = require("ui/label");
var mapsModule = require("nativescript-google-maps-sdk");
var GPlaces = require("./google-places");
var searchBarModule = require("ui/search-bar");
var gestures_1 = require("ui/gestures");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
// Important - must register MapView plugin in order to use in Angular templates
element_registry_1.registerElement("MapView", function () { return require("nativescript-google-maps-sdk").MapView; });
require('./rxjs-operators');
var MapComponent = (function () {
    function MapComponent(params) {
        var _this = this;
        this.params = params;
        this.marker = {
            id: undefined,
            longitude: undefined,
            latitude: undefined
        };
        //Map event 
        this.onMapReady = function (args) {
            // console.log("Map Ready");
            _this.mapView = args.object;
            // console.log("Setting a marker...");
            // var marker = new mapsModule.Marker();
            // marker.position = mapsModule.Position.positionFromLatLng(42.663840,24.314392);
            // marker.title = "Sydney";
            // marker.snippet = "Australia";
            // marker.userData = { index : 1};
            // mapView.addMarker(marker); 
        };
    }
    MapComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.search = this.searchEl.nativeElement;
        this.container = this.cont.nativeElement;
        //this.mapView = this.map.nativeElement;
        GPlaces.init({
            googleServerApiKey: 'AIzaSyBDV_BgWWAwcsgAl5YnYVTujE_oELsPtmM',
            language: 'es',
            radius: '500',
            location: '42.663840,23.314392',
            errorCallback: function (text) { console.log(text); }
        });
        var searchBar = new searchBarModule.SearchBar();
        this.search.on(searchBarModule.SearchBar.submitEvent, function (args) {
            _this.keyup(args.object.text);
        });
        this.search.on(searchBarModule.SearchBar.clearEvent, function (args) {
            if (_this.container.getChildAt(0) != undefined)
                _this.container.removeChild(_this.container.getChildAt(0));
        });
    };
    MapComponent.prototype.keyup = function (text) {
        var _this = this;
        if (this.container.getChildAt(0) != undefined)
            this.container.removeChild(this.container.getChildAt(0));
        GPlaces.search(text).then(function (result) {
            result = JSON.parse(result);
            var stack = new stack_layout_1.StackLayout();
            var _loop_1 = function(item) {
                var label = new label_1.Label();
                label.className = "google-place";
                label.text = item.description;
                label.textWrap = true;
                stack.addChild(label);
                label.on(gestures_1.GestureTypes.tap, function (args) {
                    if (_this.container.getChildAt(0) != undefined)
                        _this.container.removeChild(_this.container.getChildAt(0));
                    _this.getLocation(item.placeId);
                });
            };
            for (var _i = 0, result_1 = result; _i < result_1.length; _i++) {
                var item = result_1[_i];
                _loop_1(item);
            }
            _this.container.addChild(stack);
        });
    };
    MapComponent.prototype.getLocation = function (id) {
        var _this = this;
        GPlaces.details(id).then(function (place) {
            // for(let item in place){
            //     console.log(item+"=>"+place[item])
            // }
            var marker = new mapsModule.Marker();
            marker.position = mapsModule.Position.positionFromLatLng(place["latitude"], place["longitude"]);
            marker.title = place["name"];
            marker.userData = { index: 1 };
            _this.mapView.addMarker(marker);
            _this.marker.id = id;
            _this.marker.latitude = place["latitude"];
            _this.marker.longitude = place["longitude"];
        });
    };
    MapComponent.prototype.submit = function () {
        this.params.closeCallback(this.marker);
    };
    __decorate([
        core_1.ViewChild("MapView"), 
        __metadata('design:type', core_1.ElementRef)
    ], MapComponent.prototype, "map", void 0);
    __decorate([
        core_1.ViewChild("search"), 
        __metadata('design:type', core_1.ElementRef)
    ], MapComponent.prototype, "searchEl", void 0);
    __decorate([
        core_1.ViewChild("container"), 
        __metadata('design:type', core_1.ElementRef)
    ], MapComponent.prototype, "cont", void 0);
    MapComponent = __decorate([
        core_1.Component({
            selector: 'map-component',
            template: "\n    <StackLayout id=\"map-modal\">\n        <AbsoluteLayout>\n            <GridLayout rows=\"50,*,40\" style=\"width:100%;height:100%;\">\n                <SearchBar row=\"0\" style=\"height:50px;\" #search (tap)=\"keyup()\">Da</SearchBar>\n                <MapView row=\"1\" (mapReady)=\"onMapReady($event)\" zoom=\"12\" style=\"height:80%;width:100%;\"\n                latitude=\"42.663840\" longitude=\"23.314392\"></MapView>\n                <button row=\"2\" text=\"submit\" (tap)=\"submit()\"></button>\n            </GridLayout>\n            <StackLayout #container top=\"50\" width=\"100%\">\n            \n            </StackLayout>\n        </AbsoluteLayout>\n    </StackLayout>\n    "
        }), 
        __metadata('design:paramtypes', [modal_dialog_1.ModalDialogParams])
    ], MapComponent);
    return MapComponent;
}());
exports.MapComponent = MapComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL2Rldi9tYXAuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxxQkFBK0MsZUFBZSxDQUFDLENBQUE7QUFDL0QsaUNBQThCLHVDQUF1QyxDQUFDLENBQUE7QUFDdEUsNkJBQTRCLHlCQUF5QixDQUFDLENBQUE7QUFDdEQsc0JBQXNCLFVBQVUsQ0FBQyxDQUFBO0FBQ2pDLElBQUksVUFBVSxHQUFHLE9BQU8sQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO0FBQ3pELElBQUksT0FBTyxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0FBQ3pDLElBQU8sZUFBZSxXQUFXLGVBQWUsQ0FBQyxDQUFDO0FBQ2xELHlCQUErQyxhQUFhLENBQUMsQ0FBQTtBQUM3RCw2QkFBa0MsbUNBQW1DLENBQUMsQ0FBQTtBQUN0RSxnRkFBZ0Y7QUFDaEYsa0NBQWUsQ0FBQyxTQUFTLEVBQUUsY0FBTSxPQUFBLE9BQU8sQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDLE9BQU8sRUFBL0MsQ0FBK0MsQ0FBQyxDQUFDO0FBQ2pGLFFBQU8sa0JBQWtCLENBQUMsQ0FBQTtBQW1CM0I7SUFhSSxzQkFBb0IsTUFBeUI7UUFiakQsaUJBdUZDO1FBMUV1QixXQUFNLEdBQU4sTUFBTSxDQUFtQjtRQVJ0QyxXQUFNLEdBQUc7WUFDWixFQUFFLEVBQUMsU0FBUztZQUNaLFNBQVMsRUFBQyxTQUFTO1lBQ25CLFFBQVEsRUFBQyxTQUFTO1NBQ3JCLENBQUE7UUErREQsWUFBWTtRQUNaLGVBQVUsR0FBRyxVQUFDLElBQUk7WUFDZCw0QkFBNEI7WUFDNUIsS0FBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQzNCLHNDQUFzQztZQUN0Qyx3Q0FBd0M7WUFDeEMsaUZBQWlGO1lBQ2pGLDJCQUEyQjtZQUMzQixnQ0FBZ0M7WUFDaEMsa0NBQWtDO1lBQ2xDLDhCQUE4QjtRQUNsQyxDQUFDLENBQUE7SUFwRUQsQ0FBQztJQUNELCtCQUFRLEdBQVI7UUFBQSxpQkFtQkM7UUFsQkcsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQztRQUMxQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDO1FBQ3pDLHdDQUF3QztRQUN4QyxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQ1Qsa0JBQWtCLEVBQUUseUNBQXlDO1lBQzdELFFBQVEsRUFBRSxJQUFJO1lBQ2QsTUFBTSxFQUFFLEtBQUs7WUFDYixRQUFRLEVBQUUscUJBQXFCO1lBQy9CLGFBQWEsRUFBRSxVQUFTLElBQUksSUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBLENBQUEsQ0FBQztTQUNuRCxDQUFDLENBQUM7UUFDSCxJQUFJLFNBQVMsR0FBRyxJQUFJLGVBQWUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNoRCxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBQyxVQUFDLElBQVM7WUFDM0QsS0FBSSxDQUFDLEtBQUssQ0FBNkIsSUFBSSxDQUFDLE1BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUM3RCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFDLFVBQUMsSUFBUztZQUMxRCxFQUFFLENBQUEsQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBRSxTQUFTLENBQUM7Z0JBQzNDLEtBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDN0QsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0QsNEJBQUssR0FBTCxVQUFNLElBQVc7UUFBakIsaUJBb0JDO1FBbkJHLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFFLFNBQVMsQ0FBQztZQUN2QyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdELE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsTUFBTTtZQUM3QixNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUM1QixJQUFJLEtBQUssR0FBRyxJQUFJLDBCQUFXLEVBQUUsQ0FBQztZQUM5QjtnQkFDSSxJQUFJLEtBQUssR0FBRyxJQUFJLGFBQUssRUFBRSxDQUFDO2dCQUN4QixLQUFLLENBQUMsU0FBUyxHQUFHLGNBQWMsQ0FBQztnQkFDakMsS0FBSyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO2dCQUM5QixLQUFLLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztnQkFDdEIsS0FBSyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDdEIsS0FBSyxDQUFDLEVBQUUsQ0FBQyx1QkFBWSxDQUFDLEdBQUcsRUFBQyxVQUFDLElBQXNCO29CQUM3QyxFQUFFLENBQUEsQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBRSxTQUFTLENBQUM7d0JBQ3ZDLEtBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzdELEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNuQyxDQUFDLENBQUMsQ0FBQzs7WUFWUCxHQUFHLENBQUEsQ0FBYSxVQUFNLEVBQU4saUJBQU0sRUFBTixvQkFBTSxFQUFOLElBQU0sQ0FBQztnQkFBbkIsSUFBSSxJQUFJLGVBQUE7O2FBV1g7WUFDRCxLQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQyxDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFDRCxrQ0FBVyxHQUFYLFVBQVksRUFBRTtRQUFkLGlCQWNDO1FBYkcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxLQUFLO1lBQzNCLDBCQUEwQjtZQUMxQix5Q0FBeUM7WUFDekMsSUFBSTtZQUNKLElBQUksTUFBTSxHQUFHLElBQUksVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ3JDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEVBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDL0YsTUFBTSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDN0IsTUFBTSxDQUFDLFFBQVEsR0FBRyxFQUFFLEtBQUssRUFBRyxDQUFDLEVBQUMsQ0FBQztZQUMvQixLQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUMvQixLQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUM7WUFDcEIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3pDLEtBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUMvQyxDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFhRCw2QkFBTSxHQUFOO1FBQ0ksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUE1RUQ7UUFBQyxnQkFBUyxDQUFDLFNBQVMsQ0FBQzs7NkNBQUE7SUFDckI7UUFBQyxnQkFBUyxDQUFDLFFBQVEsQ0FBQzs7a0RBQUE7SUFDcEI7UUFBQyxnQkFBUyxDQUFDLFdBQVcsQ0FBQzs7OENBQUE7SUE5QjNCO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxlQUFlO1lBQ3pCLFFBQVEsRUFBRSwyckJBY1Q7U0FDSixDQUFDOztvQkFBQTtJQXdGRixtQkFBQztBQUFELENBQUMsQUF2RkQsSUF1RkM7QUF2Rlksb0JBQVksZUF1RnhCLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgRWxlbWVudFJlZiwgVmlld0NoaWxkfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtyZWdpc3RlckVsZW1lbnR9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9lbGVtZW50LXJlZ2lzdHJ5XCI7XHJcbmltcG9ydCB7IFN0YWNrTGF5b3V0IH0gZnJvbSBcInVpL2xheW91dHMvc3RhY2stbGF5b3V0XCI7XHJcbmltcG9ydCB7IExhYmVsIH0gZnJvbSBcInVpL2xhYmVsXCI7XHJcbnZhciBtYXBzTW9kdWxlID0gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1nb29nbGUtbWFwcy1zZGtcIik7XHJcbnZhciBHUGxhY2VzID0gcmVxdWlyZShcIi4vZ29vZ2xlLXBsYWNlc1wiKTtcclxuaW1wb3J0IHNlYXJjaEJhck1vZHVsZSA9IHJlcXVpcmUoXCJ1aS9zZWFyY2gtYmFyXCIpO1xyXG5pbXBvcnQgeyBHZXN0dXJlVHlwZXMsIEdlc3R1cmVFdmVudERhdGEgfSBmcm9tIFwidWkvZ2VzdHVyZXNcIjtcclxuaW1wb3J0IHsgTW9kYWxEaWFsb2dQYXJhbXMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvbW9kYWwtZGlhbG9nXCI7XHJcbi8vIEltcG9ydGFudCAtIG11c3QgcmVnaXN0ZXIgTWFwVmlldyBwbHVnaW4gaW4gb3JkZXIgdG8gdXNlIGluIEFuZ3VsYXIgdGVtcGxhdGVzXHJcbnJlZ2lzdGVyRWxlbWVudChcIk1hcFZpZXdcIiwgKCkgPT4gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1nb29nbGUtbWFwcy1zZGtcIikuTWFwVmlldyk7XHJcbiBpbXBvcnQgJy4vcnhqcy1vcGVyYXRvcnMnO1xyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbWFwLWNvbXBvbmVudCcsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgPFN0YWNrTGF5b3V0IGlkPVwibWFwLW1vZGFsXCI+XHJcbiAgICAgICAgPEFic29sdXRlTGF5b3V0PlxyXG4gICAgICAgICAgICA8R3JpZExheW91dCByb3dzPVwiNTAsKiw0MFwiIHN0eWxlPVwid2lkdGg6MTAwJTtoZWlnaHQ6MTAwJTtcIj5cclxuICAgICAgICAgICAgICAgIDxTZWFyY2hCYXIgcm93PVwiMFwiIHN0eWxlPVwiaGVpZ2h0OjUwcHg7XCIgI3NlYXJjaCAodGFwKT1cImtleXVwKClcIj5EYTwvU2VhcmNoQmFyPlxyXG4gICAgICAgICAgICAgICAgPE1hcFZpZXcgcm93PVwiMVwiIChtYXBSZWFkeSk9XCJvbk1hcFJlYWR5KCRldmVudClcIiB6b29tPVwiMTJcIiBzdHlsZT1cImhlaWdodDo4MCU7d2lkdGg6MTAwJTtcIlxyXG4gICAgICAgICAgICAgICAgbGF0aXR1ZGU9XCI0Mi42NjM4NDBcIiBsb25naXR1ZGU9XCIyMy4zMTQzOTJcIj48L01hcFZpZXc+XHJcbiAgICAgICAgICAgICAgICA8YnV0dG9uIHJvdz1cIjJcIiB0ZXh0PVwic3VibWl0XCIgKHRhcCk9XCJzdWJtaXQoKVwiPjwvYnV0dG9uPlxyXG4gICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XHJcbiAgICAgICAgICAgIDxTdGFja0xheW91dCAjY29udGFpbmVyIHRvcD1cIjUwXCIgd2lkdGg9XCIxMDAlXCI+XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICA8L1N0YWNrTGF5b3V0PlxyXG4gICAgICAgIDwvQWJzb2x1dGVMYXlvdXQ+XHJcbiAgICA8L1N0YWNrTGF5b3V0PlxyXG4gICAgYFxyXG59KVxyXG5leHBvcnQgY2xhc3MgTWFwQ29tcG9uZW50IHtcclxuICAgIHB1YmxpYyBzZWFyY2g6YW55O1xyXG4gICAgcHVibGljIGNvbnRhaW5lcjphbnk7XHJcbiAgICBwdWJsaWMgc3RhY2s6U3RhY2tMYXlvdXQ7XHJcbiAgICBwdWJsaWMgbWFwVmlldzphbnk7XHJcbiAgICBwdWJsaWMgbWFya2VyID0ge1xyXG4gICAgICAgIGlkOnVuZGVmaW5lZCxcclxuICAgICAgICBsb25naXR1ZGU6dW5kZWZpbmVkLFxyXG4gICAgICAgIGxhdGl0dWRlOnVuZGVmaW5lZFxyXG4gICAgfVxyXG4gICAgQFZpZXdDaGlsZChcIk1hcFZpZXdcIikgbWFwOiBFbGVtZW50UmVmO1xyXG4gICAgQFZpZXdDaGlsZChcInNlYXJjaFwiKSBzZWFyY2hFbDogRWxlbWVudFJlZjtcclxuICAgIEBWaWV3Q2hpbGQoXCJjb250YWluZXJcIikgY29udDogRWxlbWVudFJlZjtcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcGFyYW1zOiBNb2RhbERpYWxvZ1BhcmFtcyl7XHJcblxyXG4gICAgfVxyXG4gICAgbmdPbkluaXQoKXtcclxuICAgICAgICB0aGlzLnNlYXJjaCA9IHRoaXMuc2VhcmNoRWwubmF0aXZlRWxlbWVudDtcclxuICAgICAgICB0aGlzLmNvbnRhaW5lciA9IHRoaXMuY29udC5uYXRpdmVFbGVtZW50O1xyXG4gICAgICAgIC8vdGhpcy5tYXBWaWV3ID0gdGhpcy5tYXAubmF0aXZlRWxlbWVudDtcclxuICAgICAgICBHUGxhY2VzLmluaXQoe1xyXG4gICAgICAgICAgICBnb29nbGVTZXJ2ZXJBcGlLZXk6ICdBSXphU3lCRFZfQmdXV0F3Y3NnQWw1WW5ZVlR1akVfb0VMc1B0bU0nLFxyXG4gICAgICAgICAgICBsYW5ndWFnZTogJ2VzJyxcclxuICAgICAgICAgICAgcmFkaXVzOiAnNTAwJyxcclxuICAgICAgICAgICAgbG9jYXRpb246ICc0Mi42NjM4NDAsMjMuMzE0MzkyJyxcclxuICAgICAgICAgICAgZXJyb3JDYWxsYmFjazogZnVuY3Rpb24odGV4dCl7Y29uc29sZS5sb2codGV4dCl9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdmFyIHNlYXJjaEJhciA9IG5ldyBzZWFyY2hCYXJNb2R1bGUuU2VhcmNoQmFyKCk7XHJcbiAgICAgICAgdGhpcy5zZWFyY2gub24oc2VhcmNoQmFyTW9kdWxlLlNlYXJjaEJhci5zdWJtaXRFdmVudCwoYXJnczogYW55KT0+eyBcclxuICAgICAgICAgICAgdGhpcy5rZXl1cCgoPHNlYXJjaEJhck1vZHVsZS5TZWFyY2hCYXI+YXJncy5vYmplY3QpLnRleHQpXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5zZWFyY2gub24oc2VhcmNoQmFyTW9kdWxlLlNlYXJjaEJhci5jbGVhckV2ZW50LChhcmdzOiBhbnkpPT57XHJcbiAgICAgICAgICAgIGlmKHRoaXMuY29udGFpbmVyLmdldENoaWxkQXQoMCkhPXVuZGVmaW5lZClcclxuICAgICAgICAgICAgdGhpcy5jb250YWluZXIucmVtb3ZlQ2hpbGQodGhpcy5jb250YWluZXIuZ2V0Q2hpbGRBdCgwKSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBrZXl1cCh0ZXh0OnN0cmluZyl7XHJcbiAgICAgICAgaWYodGhpcy5jb250YWluZXIuZ2V0Q2hpbGRBdCgwKSE9dW5kZWZpbmVkKVxyXG4gICAgICAgICAgICB0aGlzLmNvbnRhaW5lci5yZW1vdmVDaGlsZCh0aGlzLmNvbnRhaW5lci5nZXRDaGlsZEF0KDApKTtcclxuICAgICAgICBHUGxhY2VzLnNlYXJjaCh0ZXh0KS50aGVuKChyZXN1bHQpPT57XHJcbiAgICAgICAgICAgIHJlc3VsdCA9IEpTT04ucGFyc2UocmVzdWx0KTtcclxuICAgICAgICAgICAgdmFyIHN0YWNrID0gbmV3IFN0YWNrTGF5b3V0KCk7XHJcbiAgICAgICAgICAgIGZvcihsZXQgaXRlbSBvZiByZXN1bHQpe1xyXG4gICAgICAgICAgICAgICAgbGV0IGxhYmVsID0gbmV3IExhYmVsKCk7XHJcbiAgICAgICAgICAgICAgICBsYWJlbC5jbGFzc05hbWUgPSBcImdvb2dsZS1wbGFjZVwiO1xyXG4gICAgICAgICAgICAgICAgbGFiZWwudGV4dCA9IGl0ZW0uZGVzY3JpcHRpb247XHJcbiAgICAgICAgICAgICAgICBsYWJlbC50ZXh0V3JhcCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBzdGFjay5hZGRDaGlsZChsYWJlbCk7XHJcbiAgICAgICAgICAgICAgICBsYWJlbC5vbihHZXN0dXJlVHlwZXMudGFwLChhcmdzOiBHZXN0dXJlRXZlbnREYXRhKT0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZih0aGlzLmNvbnRhaW5lci5nZXRDaGlsZEF0KDApIT11bmRlZmluZWQpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY29udGFpbmVyLnJlbW92ZUNoaWxkKHRoaXMuY29udGFpbmVyLmdldENoaWxkQXQoMCkpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0TG9jYXRpb24oaXRlbS5wbGFjZUlkKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuY29udGFpbmVyLmFkZENoaWxkKHN0YWNrKTtcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG4gICAgZ2V0TG9jYXRpb24oaWQpe1xyXG4gICAgICAgIEdQbGFjZXMuZGV0YWlscyhpZCkudGhlbigocGxhY2UpPT57XHJcbiAgICAgICAgICAgIC8vIGZvcihsZXQgaXRlbSBpbiBwbGFjZSl7XHJcbiAgICAgICAgICAgIC8vICAgICBjb25zb2xlLmxvZyhpdGVtK1wiPT5cIitwbGFjZVtpdGVtXSlcclxuICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgICB2YXIgbWFya2VyID0gbmV3IG1hcHNNb2R1bGUuTWFya2VyKCk7XHJcbiAgICAgICAgICAgIG1hcmtlci5wb3NpdGlvbiA9IG1hcHNNb2R1bGUuUG9zaXRpb24ucG9zaXRpb25Gcm9tTGF0TG5nKHBsYWNlW1wibGF0aXR1ZGVcIl0scGxhY2VbXCJsb25naXR1ZGVcIl0pO1xyXG4gICAgICAgICAgICBtYXJrZXIudGl0bGUgPSBwbGFjZVtcIm5hbWVcIl07XHJcbiAgICAgICAgICAgIG1hcmtlci51c2VyRGF0YSA9IHsgaW5kZXggOiAxfTtcclxuICAgICAgICAgICAgdGhpcy5tYXBWaWV3LmFkZE1hcmtlcihtYXJrZXIpOyBcclxuICAgICAgICAgICAgdGhpcy5tYXJrZXIuaWQgPSBpZDtcclxuICAgICAgICAgICAgdGhpcy5tYXJrZXIubGF0aXR1ZGUgPSBwbGFjZVtcImxhdGl0dWRlXCJdO1xyXG4gICAgICAgICAgICB0aGlzLm1hcmtlci5sb25naXR1ZGUgPSBwbGFjZVtcImxvbmdpdHVkZVwiXTtcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG4gICAgLy9NYXAgZXZlbnQgXHJcbiAgICBvbk1hcFJlYWR5ID0gKGFyZ3MpID0+IHtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZyhcIk1hcCBSZWFkeVwiKTtcclxuICAgICAgICB0aGlzLm1hcFZpZXcgPSBhcmdzLm9iamVjdDtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZyhcIlNldHRpbmcgYSBtYXJrZXIuLi5cIik7XHJcbiAgICAgICAgLy8gdmFyIG1hcmtlciA9IG5ldyBtYXBzTW9kdWxlLk1hcmtlcigpO1xyXG4gICAgICAgIC8vIG1hcmtlci5wb3NpdGlvbiA9IG1hcHNNb2R1bGUuUG9zaXRpb24ucG9zaXRpb25Gcm9tTGF0TG5nKDQyLjY2Mzg0MCwyNC4zMTQzOTIpO1xyXG4gICAgICAgIC8vIG1hcmtlci50aXRsZSA9IFwiU3lkbmV5XCI7XHJcbiAgICAgICAgLy8gbWFya2VyLnNuaXBwZXQgPSBcIkF1c3RyYWxpYVwiO1xyXG4gICAgICAgIC8vIG1hcmtlci51c2VyRGF0YSA9IHsgaW5kZXggOiAxfTtcclxuICAgICAgICAvLyBtYXBWaWV3LmFkZE1hcmtlcihtYXJrZXIpOyBcclxuICAgIH1cclxuICAgIHN1Ym1pdCgpe1xyXG4gICAgICAgIHRoaXMucGFyYW1zLmNsb3NlQ2FsbGJhY2sodGhpcy5tYXJrZXIpO1xyXG4gICAgfVxyXG59Il19