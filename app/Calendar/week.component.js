"use strict";
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var gestures_1 = require("ui/gestures");
var grid_layout_1 = require("ui/layouts/grid-layout");
var calendar_service_1 = require('./calendar.service');
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var modal_component_1 = require("./modal.component");
var page_1 = require("ui/page");
var label_1 = require("ui/label");
var enums_1 = require("ui/enums");
var WeekComponent = (function () {
    function WeekComponent(route, appoint, _modalService, vcRef, page) {
        this.route = route;
        this.appoint = appoint;
        this._modalService = _modalService;
        this.vcRef = vcRef;
        this.page = page;
        this.target = new label_1.Label;
        this.save = new label_1.Label;
        this.month2 = "January";
        this.state = 0;
    }
    WeekComponent.prototype.ngOnInit = function () {
        //console.log(document.getElementById("side-container"))
        this.cont = this.container.nativeElement;
        this.text = this.title.nativeElement;
        this.hour = new grid_layout_1.GridLayout();
        for (var i = 0; i < 7; i += 1) {
            var secondColumn = new grid_layout_1.ItemSpec(1, grid_layout_1.GridUnitType.star);
            this.hour.addColumn(secondColumn);
            var td2 = this.hours();
            grid_layout_1.GridLayout.setColumn(td2, i);
            this.hour.addChild(td2);
        }
        this.Create(0);
    };
    WeekComponent.prototype.Days = function () {
        var mon;
        var date = new Date();
        this.month = date.getMonth() + 1;
        this.year = date.getFullYear();
        var day = date.getDay();
        var today = date.getDate();
        if (today - day < 0) {
            if (this.month == 1) {
                this.month = 12;
                this.year -= 1;
            }
            else {
                this.month -= 1;
            }
            var obj = { mon: 0 };
            this.GetMonth(obj);
            this.lastday = obj.mon + today - day;
        }
        else
            this.lastday = today - day;
        return this.Week();
    };
    WeekComponent.prototype.Week = function () {
        var obj = { mon: 0 };
        this.GetMonth(obj);
        var j = this.lastday;
        var days = new Array;
        if (this.lastday - 7 < 0) {
            if (this.month == 1) {
                this.month = 12;
                this.year -= 1;
            }
            else
                this.month -= 1;
        }
        for (var i = 0; i < 7; i += 1) {
            if (j == obj.mon)
                j = 0;
            j += 1;
            days.push(j);
        }
        this.lastday = j;
        if (days[0] < days[6] && days[0] - 7 <= 0) {
            if (this.month == 12) {
                this.month = 1;
                this.year += 1;
            }
            else
                this.month += 1;
        }
        this.Month();
        if (days[0] > days[6] && days[6] - 7 <= 0) {
            if (this.month == 12) {
                this.month = 1;
                this.year += 1;
            }
            else
                this.month += 1;
        }
        return days;
    };
    WeekComponent.prototype.Week1 = function () {
        var obj = { mon: 0 };
        if (this.lastday - 14 >= 0) {
            this.lastday -= 14;
            this.GetMonth(obj);
        }
        else {
            if (this.month == 1) {
                this.month = 12;
                this.year -= 1;
                this.GetMonth(obj);
            }
            else {
                this.month -= 1;
                this.GetMonth(obj);
            }
            this.lastday = (this.lastday - 14 + obj.mon);
        }
        var days = new Array;
        var j = this.lastday;
        for (var i = 0; i < 7; i += 1) {
            if (j == obj.mon)
                j = 0;
            j += 1;
            days.push(j);
        }
        this.lastday = j;
        if (this.lastday - 7 < 0) {
            if (this.month == 12) {
                this.month = 1;
                this.year += 1;
            }
            else
                this.month += 1;
        }
        this.Month();
        return days;
    };
    WeekComponent.prototype.GetMonth = function (obj) {
        if (this.month <= 7) {
            if (this.month % 2 == 1)
                obj.mon = 31;
            else
                obj.mon = 30;
            if (this.month == 2) {
                if (((this.year % 4 == 0) && (this.year % 100 != 0))
                    || (this.year % 400 == 0))
                    obj.mon = 29;
                else
                    obj.mon = 28;
            }
        }
        else {
            if (this.month % 2 == 1)
                obj.mon = 30;
            else
                obj.mon = 31;
        }
    };
    WeekComponent.prototype.Create = function (n) {
        var _this = this;
        this.cont2 = new grid_layout_1.GridLayout();
        this.cont2.style.width = "100%";
        this.cont2.style.borderRightColor = "#e7e7e7";
        this.cont2.style.borderWidth = "1px";
        this.cont2.style.backgroundColor = "white";
        var week;
        switch (n) {
            case -1:
                week = this.Week1();
                break;
            case 0:
                week = this.Days();
                break;
            case 1:
                week = this.Week();
                break;
        }
        var grid = this.hour;
        grid.id = "contt";
        grid.style.marginTop = 40;
        var i = 0;
        var days = new grid_layout_1.GridLayout();
        week.forEach(function (element) {
            var secondColumn = new grid_layout_1.ItemSpec(1, grid_layout_1.GridUnitType.star);
            days.addColumn(secondColumn);
            var td = new label_1.Label();
            td.text = element;
            td.className = "day-week";
            grid_layout_1.GridLayout.setColumn(td, i);
            days.addChild(td);
            i += 1;
        });
        days.id = "days";
        this.cont2.addChild(days);
        this.cont2.addChild(grid);
        if (this.state == 1)
            this.cont2.translateX = -1000;
        if (this.state == 2)
            this.cont2.translateX = 1000;
        this.cont.addChild(this.cont2);
        this.showAttach(week[0]);
        this.cont2.animate({
            translate: { x: 0, y: 0 },
            duration: 800,
            curve: enums_1.AnimationCurve.easeOut
        });
        this.hour = new grid_layout_1.GridLayout();
        for (var i_1 = 0; i_1 < 7; i_1 += 1) {
            var secondColumn = new grid_layout_1.ItemSpec(1, grid_layout_1.GridUnitType.star);
            this.hour.addColumn(secondColumn);
            var td2 = this.hours();
            grid_layout_1.GridLayout.setColumn(td2, i_1);
            this.hour.addChild(td2);
        }
        this.cont.on(gestures_1.GestureTypes.swipe, function (args) {
            console.log("Swipe Direction: " + args.direction);
            if (args.direction == 2) {
                _this.Next();
            }
            if (args.direction == 1) {
                _this.Prev();
            }
        });
    };
    WeekComponent.prototype.showAttach = function (day) {
        var _this = this;
        var date = this.year + "-" + this.month + "-" + day;
        var arr = new Array;
        this.appoint.getAppointment(date).subscribe(function (result) {
            if (result.length > 0) {
                var grandParent = _this.cont2.getChildById('contt');
                // for(let da in grandParent){
                //    console.log(da)
                // }
                arr = result;
                var d = void 0, parent_1, children = void 0, start = void 0, end = void 0, i = void 0, label = void 0, time = void 0;
                for (var key in arr) {
                    d = arr[key]["date"].split("-");
                    d = parseInt(d[2]) - day;
                    parent_1 = grandParent.getChildAt(d);
                    start = arr[key]["start"].split(":");
                    start = parseInt(start[0]);
                    parent_1.removeChild(parent_1.getChildById(start));
                    label = new label_1.Label();
                    label.text = arr[key]["name"] + "\r\n" + arr[key]["start"].substring(0, 5);
                    label.textWrap = true;
                    label.backgroundColor = arr[key]["color"];
                    label.className = "hour-btn attachment-btn";
                    grid_layout_1.GridLayout.setRow(label, start);
                    parent_1.addChild(label);
                }
            }
            else
                console.log("not");
        }, function (error) {
            console.log(error);
        });
    };
    WeekComponent.prototype.hours = function () {
        var _this = this;
        var td = new grid_layout_1.GridLayout();
        var label;
        for (var i = 0; i < 24; i++) {
            var secondRow = new grid_layout_1.ItemSpec(1, grid_layout_1.GridUnitType.auto);
            td.addRow(secondRow);
            label = new label_1.Label();
            label.className = "hour-btn";
            if (i < 10)
                label.text = "0" + i + ":30";
            else
                label.text = i + ":30";
            label.id = i;
            label.on(gestures_1.GestureTypes.tap, function (args) {
                _this.clicked(args.object);
            });
            label.on(gestures_1.GestureTypes.swipe, function (args) {
                console.log("Swipe Direction: " + args.direction);
                if (args.direction == 2) {
                    _this.Next();
                }
                if (args.direction == 1) {
                    _this.Prev();
                }
            });
            //label.style.paddingTop = 20; 
            grid_layout_1.GridLayout.setRow(label, i);
            td.addChild(label);
        }
        return td;
    };
    WeekComponent.prototype.Prev = function () {
        this.state = 1;
        this.Create(-1);
    };
    WeekComponent.prototype.Next = function () {
        this.state = 2;
        this.Create(1);
    };
    WeekComponent.prototype.Month = function () {
        var _this = this;
        switch (this.month) {
            case 1:
                this.month2 = "January";
                break;
            case 2:
                this.month2 = "February";
                break;
            case 3:
                this.month2 = "March";
                break;
            case 4:
                this.month2 = "April";
                break;
            case 5:
                this.month2 = "May";
                break;
            case 6:
                this.month2 = "June";
                break;
            case 7:
                this.month2 = "July";
                break;
            case 8:
                this.month2 = "August";
                break;
            case 9:
                this.month2 = "September";
                break;
            case 10:
                this.month2 = "October";
                break;
            case 11:
                this.month2 = "November";
                break;
            case 12:
                this.month2 = "December";
                break;
        }
        this.year2 = this.year;
        setTimeout(function () {
            _this.text.text = _this.month2;
        }, 300);
    };
    WeekComponent.prototype.clicked = function (event) {
        var _this = this;
        if (event == this.target) {
            var obj = {
                start: this.save.text,
                vcRef: this.vcRef
            };
            var options = {
                viewContainerRef: this.vcRef,
                context: obj,
                fullscreen: true
            };
            this._modalService.showModal(modal_component_1.ModalViewComponent, options)
                .then(function (appointment) {
                if (appointment != undefined) {
                    _this.Appointment(appointment);
                }
            });
        }
        else {
            this.target.text = this.save.text;
            this.target.className = this.save.className;
            this.save.text = event.text;
            this.save.className = event.className;
            this.target = event;
            event.className = "hour-btn new-attach";
            event.text = "+";
        }
    };
    WeekComponent.prototype.Appointment = function (obj) {
        var _this = this;
        var grandParent = this.cont2.getChildById('contt');
        var parent = this.target.parent;
        var days = this.page.getViewById("days");
        var label = days.getChildAt(grandParent.getChildIndex(parent));
        var color = obj.color["_hex"];
        var date = this.year + "-" + this.month + "-" + label.text;
        var name = obj.name;
        if (obj.name != "") {
            for (var da in obj) {
                console.log(da + "=>" + obj[da]);
            }
            var from = obj.from;
            var to_1 = obj.to;
            this.appoint.postAppointment(obj.name, obj.from, obj.to, date, color, obj.latitude, obj.longitude, obj.id).subscribe(function (result) {
                if (result === true) {
                    var i = 1;
                    var val = parseInt(to_1.value);
                    var remove = void 0;
                    parent.removeChild(_this.target);
                    // while(i){
                    // 	remove = this.target.nextSibling;
                    // 	if(val > parseInt(remove.value)){
                    // 	i+=1; 
                    // 	this.target.parentElement.removeChild(remove);
                    // 	}
                    // 	else break;
                    // };
                    label = new label_1.Label();
                    label.text = obj["name"] + "\r\n" + obj["from"].substring(0, 5);
                    label.textWrap = true;
                    label.backgroundColor = obj["color"];
                    label.className = "hour-btn attachment-btn";
                    var val2 = obj["from"].split(":");
                    var start = (parseInt(val2[0]));
                    grid_layout_1.GridLayout.setRow(label, start);
                    parent.addChild(label);
                }
                else {
                    console.error("Something went wrong");
                }
            }, function (error) { console.error(error); });
        }
    };
    __decorate([
        core_1.ViewChild("title"), 
        __metadata('design:type', core_1.ElementRef)
    ], WeekComponent.prototype, "title", void 0);
    __decorate([
        core_1.ViewChild("container"), 
        __metadata('design:type', core_1.ElementRef)
    ], WeekComponent.prototype, "container", void 0);
    WeekComponent = __decorate([
        core_1.Component({
            selector: 'year-calendar',
            template: "\n\t<ActionBar title=\"\">\n\t\t<Label text=\"tralala\" #title></Label>\n\t</ActionBar>\n\t<AbsoluteLayout>\n\t\t<ScrollView orientation=\"vertical\" width=\"100%\" height=\"99%\">\n\t\t\t<AbsoluteLayout style=\"width:100%;height:1200;\" #container></AbsoluteLayout>\n\t\t</ScrollView>\n\t</AbsoluteLayout>\n    ",
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, calendar_service_1.CalendarService, modal_dialog_1.ModalDialogService, core_1.ViewContainerRef, page_1.Page])
    ], WeekComponent);
    return WeekComponent;
}());
exports.WeekComponent = WeekComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2Vlay5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9kZXYvQ2FsZW5kYXIvd2Vlay5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHFCQUMyRCxlQUFlLENBQUMsQ0FBQTtBQUMzRSx1QkFBdUMsaUJBQWlCLENBQUMsQ0FBQTtBQUN6RCx5QkFBc0UsYUFBYSxDQUFDLENBQUE7QUFDcEYsNEJBQWtELHdCQUF3QixDQUFDLENBQUE7QUFFM0UsaUNBQWdDLG9CQUFvQixDQUFDLENBQUE7QUFDckQsNkJBQXVELG1DQUFtQyxDQUFDLENBQUE7QUFDM0YsZ0NBQW1DLG1CQUFtQixDQUFDLENBQUE7QUFDdkQscUJBQWlDLFNBQVMsQ0FBQyxDQUFBO0FBQzNDLHNCQUFzQixVQUFVLENBQUMsQ0FBQTtBQUNqQyxzQkFBK0IsVUFBVSxDQUFDLENBQUE7QUFpQjFDO0lBZ0JDLHVCQUFvQixLQUFxQixFQUFTLE9BQXdCLEVBQ2xFLGFBQWlDLEVBQVMsS0FBdUIsRUFBVSxJQUFVO1FBRHpFLFVBQUssR0FBTCxLQUFLLENBQWdCO1FBQVMsWUFBTyxHQUFQLE9BQU8sQ0FBaUI7UUFDbEUsa0JBQWEsR0FBYixhQUFhLENBQW9CO1FBQVMsVUFBSyxHQUFMLEtBQUssQ0FBa0I7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFNO1FBaEJyRixXQUFNLEdBQUcsSUFBSSxhQUFLLENBQUM7UUFDbkIsU0FBSSxHQUFHLElBQUksYUFBSyxDQUFDO1FBT2xCLFdBQU0sR0FBRyxTQUFTLENBQUM7UUFDbkIsVUFBSyxHQUFVLENBQUMsQ0FBQztJQU95RSxDQUFDO0lBQ2xHLGdDQUFRLEdBQVI7UUFDQyx3REFBd0Q7UUFDeEQsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQztRQUN2QyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSx3QkFBVSxFQUFFLENBQUM7UUFDL0IsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFDLENBQUMsR0FBQyxDQUFDLEVBQUMsQ0FBQyxJQUFFLENBQUMsRUFBQyxDQUFDO1lBQ2xCLElBQUksWUFBWSxHQUFHLElBQUksc0JBQVEsQ0FBQyxDQUFDLEVBQUUsMEJBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0RCxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUNyQyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDdkIsd0JBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZCLENBQUM7UUFFSCxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ2hCLENBQUM7SUFDRCw0QkFBSSxHQUFKO1FBQ0MsSUFBSSxHQUFVLENBQUM7UUFDZixJQUFJLElBQUksR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxHQUFDLENBQUMsQ0FBQztRQUMvQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUMvQixJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDeEIsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzNCLEVBQUUsQ0FBQSxDQUFDLEtBQUssR0FBQyxHQUFHLEdBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNoQixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFBLENBQUM7Z0JBQ25CLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO2dCQUNoQixJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQztZQUNoQixDQUFDO1lBQ0QsSUFBSSxDQUFBLENBQUM7Z0JBQ0osSUFBSSxDQUFDLEtBQUssSUFBRyxDQUFDLENBQUM7WUFDaEIsQ0FBQztZQUNELElBQUksR0FBRyxHQUFHLEVBQUMsR0FBRyxFQUFFLENBQUMsRUFBQyxDQUFBO1lBQ2xCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDbkIsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsR0FBRyxHQUFHLEtBQUssR0FBRyxHQUFHLENBQUM7UUFDdEMsQ0FBQztRQUNELElBQUk7WUFBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssR0FBQyxHQUFHLENBQUM7UUFDOUIsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBQ0QsNEJBQUksR0FBSjtRQUNDLElBQUksR0FBRyxHQUFHLEVBQUMsR0FBRyxFQUFFLENBQUMsRUFBQyxDQUFBO1FBQ2xCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbkIsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUNyQixJQUFJLElBQUksR0FBRyxJQUFJLEtBQUssQ0FBQztRQUNyQixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQ3RCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUEsQ0FBQztnQkFBQSxJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztnQkFBQyxJQUFJLENBQUMsSUFBSSxJQUFHLENBQUMsQ0FBQztZQUFBLENBQUM7WUFDcEQsSUFBSTtnQkFBQyxJQUFJLENBQUMsS0FBSyxJQUFHLENBQUMsQ0FBQztRQUNyQixDQUFDO1FBQ0QsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FBQyxJQUFFLENBQUMsRUFBRSxDQUFDO1lBQzVCLEVBQUUsQ0FBQSxDQUFDLENBQUMsSUFBSSxHQUFHLENBQUMsR0FBRyxDQUFDO2dCQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDdkIsQ0FBQyxJQUFHLENBQUMsQ0FBQztZQUNOLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDYixDQUFDO1FBQ0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFDakIsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUMsQ0FBQyxJQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDckMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUMsQ0FBQSxDQUFDO2dCQUFBLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO2dCQUFDLElBQUksQ0FBQyxJQUFJLElBQUcsQ0FBQyxDQUFDO1lBQUEsQ0FBQztZQUNwRCxJQUFJO2dCQUFDLElBQUksQ0FBQyxLQUFLLElBQUcsQ0FBQyxDQUFDO1FBQ3JCLENBQUM7UUFDRCxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDYixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBQyxDQUFDLElBQUUsQ0FBQyxDQUFDLENBQUEsQ0FBQztZQUNyQyxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQyxDQUFBLENBQUM7Z0JBQUEsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7Z0JBQUMsSUFBSSxDQUFDLElBQUksSUFBRyxDQUFDLENBQUM7WUFBQSxDQUFDO1lBQ3BELElBQUk7Z0JBQUMsSUFBSSxDQUFDLEtBQUssSUFBRyxDQUFDLENBQUM7UUFDckIsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDYixDQUFDO0lBQ0QsNkJBQUssR0FBTDtRQUNDLElBQUksR0FBRyxHQUFHLEVBQUMsR0FBRyxFQUFFLENBQUMsRUFBQyxDQUFBO1FBQ2xCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDMUIsSUFBSSxDQUFDLE9BQU8sSUFBRyxFQUFFLENBQUM7WUFDbEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNwQixDQUFDO1FBQ0QsSUFBSSxDQUFDLENBQUM7WUFDTCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFBLENBQUM7Z0JBQ25CLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO2dCQUNoQixJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQztnQkFDZixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BCLENBQUM7WUFDRCxJQUFJLENBQUEsQ0FBQztnQkFDSixJQUFJLENBQUMsS0FBSyxJQUFHLENBQUMsQ0FBQztnQkFDZixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BCLENBQUM7WUFDRCxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzlDLENBQUM7UUFDRCxJQUFJLElBQUksR0FBRyxJQUFJLEtBQUssQ0FBQztRQUNyQixJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ3JCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQUMsSUFBRSxDQUFDLEVBQUUsQ0FBQztZQUM1QixFQUFFLENBQUEsQ0FBQyxDQUFDLElBQUksR0FBRyxDQUFDLEdBQUcsQ0FBQztnQkFBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3ZCLENBQUMsSUFBRyxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ2IsQ0FBQztRQUNELElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDeEIsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUMsQ0FBQSxDQUFDO2dCQUNwQixJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztnQkFDZixJQUFJLENBQUMsSUFBSSxJQUFHLENBQUMsQ0FBQztZQUNmLENBQUM7WUFDRCxJQUFJO2dCQUFDLElBQUksQ0FBQyxLQUFLLElBQUcsQ0FBQyxDQUFDO1FBQ3JCLENBQUM7UUFDRCxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDYixNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2IsQ0FBQztJQUNELGdDQUFRLEdBQVIsVUFBUyxHQUFRO1FBQ2hCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUEsQ0FBQztZQUNuQixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUM7WUFDckMsSUFBSTtnQkFBQyxHQUFHLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQztZQUNsQixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFBLENBQUM7Z0JBQ25CLEVBQUUsQ0FBQSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDO3VCQUMvQyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUFBLEdBQUcsQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDO2dCQUN4QyxJQUFJO29CQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDO1lBQ25CLENBQUM7UUFDRixDQUFDO1FBQ0QsSUFBSSxDQUFBLENBQUM7WUFDSixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUM7WUFDckMsSUFBSTtnQkFBQyxHQUFHLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQztRQUNuQixDQUFDO0lBQ0YsQ0FBQztJQUNELDhCQUFNLEdBQU4sVUFBTyxDQUFNO1FBQWIsaUJBeURDO1FBeERBLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSx3QkFBVSxFQUFFLENBQUM7UUFDN0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQztRQUNqQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxTQUFTLENBQUM7UUFDOUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUNuQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsT0FBTyxDQUFBO1FBQzVDLElBQUksSUFBUSxDQUFDO1FBQ2IsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNYLEtBQUssQ0FBQyxDQUFDO2dCQUFDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQUEsS0FBSyxDQUFDO1lBQ2xDLEtBQUssQ0FBQztnQkFBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUFBLEtBQUssQ0FBQztZQUNoQyxLQUFLLENBQUM7Z0JBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFBQSxLQUFLLENBQUM7UUFDakMsQ0FBQztRQUNELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDbkIsSUFBSSxDQUFDLEVBQUUsR0FBRyxPQUFPLENBQUE7UUFDakIsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO1FBQzVCLElBQUksQ0FBQyxHQUFDLENBQUMsQ0FBQztRQUNOLElBQUksSUFBSSxHQUFHLElBQUksd0JBQVUsRUFBRSxDQUFDO1FBQzlCLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQyxPQUFXO1lBQ3JCLElBQUksWUFBWSxHQUFHLElBQUksc0JBQVEsQ0FBQyxDQUFDLEVBQUUsMEJBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0RCxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzdCLElBQUksRUFBRSxHQUFHLElBQUksYUFBSyxFQUFFLENBQUM7WUFDeEIsRUFBRSxDQUFDLElBQUksR0FBRyxPQUFPLENBQUM7WUFDZixFQUFFLENBQUMsU0FBUyxHQUFHLFVBQVUsQ0FBQztZQUMxQix3QkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDNUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNyQixDQUFDLElBQUUsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSixJQUFJLENBQUMsRUFBRSxHQUFDLE1BQU0sQ0FBQztRQUNiLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDO1lBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsQ0FBQyxJQUFJLENBQUM7UUFDbEQsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUM7WUFBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFDakQsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDekIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7WUFDakIsU0FBUyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFDO1lBQ3hCLFFBQVEsRUFBRSxHQUFHO1lBQ2IsS0FBSyxFQUFFLHNCQUFjLENBQUMsT0FBTztTQUM5QixDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksd0JBQVUsRUFBRSxDQUFDO1FBQy9CLEdBQUcsQ0FBQSxDQUFDLElBQUksR0FBQyxHQUFDLENBQUMsRUFBQyxHQUFDLEdBQUMsQ0FBQyxFQUFDLEdBQUMsSUFBRSxDQUFDLEVBQUMsQ0FBQztZQUNsQixJQUFJLFlBQVksR0FBRyxJQUFJLHNCQUFRLENBQUMsQ0FBQyxFQUFFLDBCQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdEQsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDckMsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ3ZCLHdCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRSxHQUFDLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN2QixDQUFDO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsdUJBQVksQ0FBQyxLQUFLLEVBQUMsVUFBQyxJQUEyQjtZQUM3RCxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNqRCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7Z0JBQ3JCLEtBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNiLENBQUM7WUFDRCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7Z0JBQ3JCLEtBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNiLENBQUM7UUFDRixDQUFDLENBQUMsQ0FBQztJQUVMLENBQUM7SUFDQSxrQ0FBVSxHQUFWLFVBQVcsR0FBVztRQUF0QixpQkFpQ0E7UUFoQ0EsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBQyxHQUFHLEdBQUMsSUFBSSxDQUFDLEtBQUssR0FBQyxHQUFHLEdBQUMsR0FBRyxDQUFDO1FBQzVDLElBQUksR0FBRyxHQUFHLElBQUksS0FBSyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FDMUMsVUFBQSxNQUFNO1lBQ0wsRUFBRSxDQUFBLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsQ0FBQSxDQUFDO2dCQUNkLElBQUksV0FBVyxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNuRCw4QkFBOEI7Z0JBQzlCLHFCQUFxQjtnQkFDckIsSUFBSTtnQkFDVCxHQUFHLEdBQUcsTUFBTSxDQUFDO2dCQUNiLElBQUksQ0FBQyxTQUFPLEVBQUMsUUFBVSxFQUFDLFFBQVEsU0FBSSxFQUFDLEtBQUssU0FBSSxFQUFDLEdBQUcsU0FBSSxFQUFDLENBQUMsU0FBSSxFQUFDLEtBQUssU0FBSSxFQUFDLElBQUksU0FBTyxDQUFDO2dCQUM5RSxHQUFHLENBQUEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsQ0FBQSxDQUFDO29CQUNsQixDQUFDLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDdEMsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBQyxHQUFHLENBQUM7b0JBQ2pCLFFBQU0sR0FBRyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFBO29CQUNsQyxLQUFLLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDM0MsS0FBSyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDckIsUUFBTSxDQUFDLFdBQVcsQ0FBQyxRQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7b0JBQy9DLEtBQUssR0FBRyxJQUFJLGFBQUssRUFBRSxDQUFDO29CQUNwQixLQUFLLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBQyxNQUFNLEdBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ3ZFLEtBQUssQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO29CQUN0QixLQUFLLENBQUMsZUFBZSxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDMUMsS0FBSyxDQUFDLFNBQVMsR0FBRyx5QkFBeUIsQ0FBQztvQkFDNUMsd0JBQVUsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUNuQyxRQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN0QixDQUFDO1lBQ1AsQ0FBQztZQUNHLElBQUk7Z0JBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUM1QixDQUFDLEVBQ0UsVUFBQSxLQUFLO1lBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUNwQixDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7SUFDQSw2QkFBSyxHQUFMO1FBQUEsaUJBNEJDO1FBM0JELElBQUksRUFBRSxHQUFHLElBQUksd0JBQVUsRUFBRSxDQUFDO1FBQzFCLElBQUksS0FBUyxDQUFDO1FBQ2QsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFDLENBQUMsR0FBQyxFQUFFLEVBQUMsQ0FBQyxFQUFFLEVBQUMsQ0FBQztZQUNyQixJQUFJLFNBQVMsR0FBRyxJQUFJLHNCQUFRLENBQUMsQ0FBQyxFQUFFLDBCQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbkQsRUFBRSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNyQixLQUFLLEdBQUcsSUFBSSxhQUFLLEVBQUUsQ0FBQztZQUNwQixLQUFLLENBQUMsU0FBUyxHQUFHLFVBQVUsQ0FBQztZQUM3QixFQUFFLENBQUEsQ0FBQyxDQUFDLEdBQUMsRUFBRSxDQUFDO2dCQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsR0FBRyxHQUFDLENBQUMsR0FBQyxLQUFLLENBQUM7WUFDbEMsSUFBSTtnQkFBQyxLQUFLLENBQUMsSUFBSSxHQUFHLENBQUMsR0FBQyxLQUFLLENBQUM7WUFDMUIsS0FBSyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDVixLQUFLLENBQUMsRUFBRSxDQUFDLHVCQUFZLENBQUMsR0FBRyxFQUFHLFVBQUMsSUFBc0I7Z0JBQ2pELEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzVCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsS0FBSyxDQUFDLEVBQUUsQ0FBQyx1QkFBWSxDQUFDLEtBQUssRUFBQyxVQUFDLElBQTJCO2dCQUMzRCxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDakQsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBRSxDQUFDLENBQUMsQ0FBQSxDQUFDO29CQUNyQixLQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ2IsQ0FBQztnQkFDRCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7b0JBQ3JCLEtBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDYixDQUFDO1lBQ0YsQ0FBQyxDQUFDLENBQUM7WUFDSCwrQkFBK0I7WUFDL0Isd0JBQVUsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzVCLEVBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDcEIsQ0FBQztRQUNELE1BQU0sQ0FBQyxFQUFFLENBQUM7SUFDVixDQUFDO0lBQ0YsNEJBQUksR0FBSjtRQUNHLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNqQixDQUFDO0lBQ0QsNEJBQUksR0FBSjtRQUNHLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDaEIsQ0FBQztJQUNELDZCQUFLLEdBQUw7UUFBQSxpQkFtQkM7UUFsQkEsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDcEIsS0FBSyxDQUFDO2dCQUFFLElBQUksQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFDO2dCQUFBLEtBQUssQ0FBQztZQUN0QyxLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUM7Z0JBQUEsS0FBSyxDQUFDO1lBQ3ZDLEtBQUssQ0FBQztnQkFBRSxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQztnQkFBQSxLQUFLLENBQUM7WUFDcEMsS0FBSyxDQUFDO2dCQUFFLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDO2dCQUFBLEtBQUssQ0FBQztZQUNwQyxLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7Z0JBQUEsS0FBSyxDQUFDO1lBQ2xDLEtBQUssQ0FBQztnQkFBRSxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztnQkFBQSxLQUFLLENBQUM7WUFDbkMsS0FBSyxDQUFDO2dCQUFFLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO2dCQUFBLEtBQUssQ0FBQztZQUNuQyxLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUM7Z0JBQUEsS0FBSyxDQUFDO1lBQ3JDLEtBQUssQ0FBQztnQkFBRSxJQUFJLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQztnQkFBQSxLQUFLLENBQUM7WUFDeEMsS0FBSyxFQUFFO2dCQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFDO2dCQUFBLEtBQUssQ0FBQztZQUN0QyxLQUFLLEVBQUU7Z0JBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUM7Z0JBQUEsS0FBSyxDQUFDO1lBQ3ZDLEtBQUssRUFBRTtnQkFBQyxJQUFJLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQztnQkFBQSxLQUFLLENBQUM7UUFDeEMsQ0FBQztRQUNELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztRQUNyQixVQUFVLENBQUM7WUFDVixLQUFJLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFBO1FBQzdCLENBQUMsRUFBQyxHQUFHLENBQUMsQ0FBQTtJQUNULENBQUM7SUFDQSwrQkFBTyxHQUFQLFVBQVEsS0FBVTtRQUFsQixpQkE0QkM7UUEzQkMsRUFBRSxDQUFBLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQSxDQUFDO1lBQzFCLElBQUksR0FBRyxHQUFHO2dCQUNULEtBQUssRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUk7Z0JBQ3JCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSzthQUNqQixDQUFBO1lBQ0QsSUFBSSxPQUFPLEdBQXVCO2dCQUM5QixnQkFBZ0IsRUFBRSxJQUFJLENBQUMsS0FBSztnQkFDNUIsT0FBTyxFQUFFLEdBQUc7Z0JBQ1osVUFBVSxFQUFFLElBQUk7YUFDakIsQ0FBQztZQUNKLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLG9DQUFrQixFQUFFLE9BQU8sQ0FBQztpQkFDeEQsSUFBSSxDQUFDLFVBQUMsV0FBZ0I7Z0JBQ3JCLEVBQUUsQ0FBQSxDQUFDLFdBQVcsSUFBSSxTQUFTLENBQUMsQ0FBQSxDQUFDO29CQUM1QixLQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUMvQixDQUFDO1lBQ0QsQ0FBQyxDQUFDLENBQUM7UUFFTixDQUFDO1FBQ0QsSUFBSSxDQUFBLENBQUM7WUFDSixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUNsQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQTtZQUMzQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO1lBQzVCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUM7WUFDdEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDcEIsS0FBSyxDQUFDLFNBQVMsR0FBRyxxQkFBcUIsQ0FBQztZQUN4QyxLQUFLLENBQUMsSUFBSSxHQUFDLEdBQUcsQ0FBQTtRQUNmLENBQUM7SUFDRCxDQUFDO0lBQ0QsbUNBQVcsR0FBWCxVQUFZLEdBQU87UUFBbkIsaUJBNkNDO1FBNUNELElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFBO1FBQ2xELElBQUksTUFBTSxHQUFlLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO1FBQzVDLElBQUksSUFBSSxHQUFlLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFBO1FBQ3BELElBQUksS0FBSyxHQUFVLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQ3RFLElBQUksS0FBSyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDOUIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBQyxHQUFHLEdBQUMsSUFBSSxDQUFDLEtBQUssR0FBQyxHQUFHLEdBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztRQUNuRCxJQUFJLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDO1FBQ3BCLEVBQUUsQ0FBQSxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUcsRUFBRSxDQUFDLENBQUEsQ0FBQztZQUNqQixHQUFHLENBQUEsQ0FBQyxJQUFJLEVBQUUsSUFBSSxHQUFHLENBQUMsQ0FBQSxDQUFDO2dCQUNsQixPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBQyxJQUFJLEdBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUE7WUFDN0IsQ0FBQztZQUNELElBQUksSUFBSSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUM7WUFDcEIsSUFBSSxJQUFFLEdBQUcsR0FBRyxDQUFDLEVBQUUsQ0FBQztZQUNoQixJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUMsR0FBRyxDQUFDLEVBQUUsRUFBQyxJQUFJLEVBQUMsS0FBSyxFQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUMsR0FBRyxDQUFDLFNBQVMsRUFBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUM1RyxVQUFBLE1BQU07Z0JBQ0wsRUFBRSxDQUFDLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ3RCLElBQUksQ0FBQyxHQUFDLENBQUMsQ0FBQztvQkFDUixJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUMsSUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUM3QixJQUFJLE1BQU0sU0FBSSxDQUFDO29CQUNmLE1BQU0sQ0FBQyxXQUFXLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUNoQyxZQUFZO29CQUNaLHFDQUFxQztvQkFDckMscUNBQXFDO29CQUNyQyxVQUFVO29CQUNWLGtEQUFrRDtvQkFDbEQsS0FBSztvQkFDTCxlQUFlO29CQUNmLEtBQUs7b0JBQ0wsS0FBSyxHQUFHLElBQUksYUFBSyxFQUFFLENBQUM7b0JBQ3BCLEtBQUssQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFDLE1BQU0sR0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDNUQsS0FBSyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7b0JBQ3RCLEtBQUssQ0FBQyxlQUFlLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUNyQyxLQUFLLENBQUMsU0FBUyxHQUFHLHlCQUF5QixDQUFDO29CQUU1QyxJQUFJLElBQUksR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUNsQyxJQUFJLEtBQUssR0FBRyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNoQyx3QkFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7b0JBQ2hDLE1BQU0sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3hCLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ1AsT0FBTyxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2dCQUN2QyxDQUFDO1lBQ0YsQ0FBQyxFQUNELFVBQUEsS0FBSyxJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQSxDQUFDLENBQUMsQ0FBQztRQUNsQyxDQUFDO0lBQ0QsQ0FBQztJQXJWRjtRQUFDLGdCQUFTLENBQUMsT0FBTyxDQUFDOztnREFBQTtJQUNsQjtRQUFDLGdCQUFTLENBQUMsV0FBVyxDQUFDOztvREFBQTtJQTlCekI7UUFBQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGVBQWU7WUFDMUIsUUFBUSxFQUFFLDBUQVNOO1NBR0osQ0FBQzs7cUJBQUE7SUFxV0Ysb0JBQUM7QUFBRCxDQUFDLEFBcFdELElBb1dDO0FBcFdZLHFCQUFhLGdCQW9XekIsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LElucHV0LHRyYW5zaXRpb24sXHJcbiAgICBFbGVtZW50UmVmLCBPbkluaXQsIFZpZXdDaGlsZCwgVmlld0NvbnRhaW5lclJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSwgUGFyYW1zIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgR2VzdHVyZVR5cGVzLCBTd2lwZUdlc3R1cmVFdmVudERhdGEsIEdlc3R1cmVFdmVudERhdGEgfSBmcm9tIFwidWkvZ2VzdHVyZXNcIjtcclxuaW1wb3J0IHsgR3JpZExheW91dCwgR3JpZFVuaXRUeXBlLCBJdGVtU3BlY30gZnJvbSBcInVpL2xheW91dHMvZ3JpZC1sYXlvdXRcIjtcclxuaW1wb3J0IHsgU3RhY2tMYXlvdXQgfSBmcm9tIFwidWkvbGF5b3V0cy9zdGFjay1sYXlvdXRcIjtcclxuaW1wb3J0IHsgQ2FsZW5kYXJTZXJ2aWNlIH0gZnJvbSAnLi9jYWxlbmRhci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTW9kYWxEaWFsb2dTZXJ2aWNlLCBNb2RhbERpYWxvZ09wdGlvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvbW9kYWwtZGlhbG9nXCI7XHJcbmltcG9ydCB7IE1vZGFsVmlld0NvbXBvbmVudCB9IGZyb20gXCIuL21vZGFsLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBQYWdlIH0gICAgICAgICAgICAgZnJvbSBcInVpL3BhZ2VcIjtcclxuaW1wb3J0IHsgTGFiZWwgfSBmcm9tIFwidWkvbGFiZWxcIjtcclxuaW1wb3J0IHsgQW5pbWF0aW9uQ3VydmUgfSBmcm9tIFwidWkvZW51bXNcIjtcclxuaW1wb3J0IHsgQW5pbWF0aW9uIH0gZnJvbSBcInVpL2FuaW1hdGlvblwiO1xyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ3llYXItY2FsZW5kYXInLFxyXG5cdHRlbXBsYXRlOiBgXHJcblx0PEFjdGlvbkJhciB0aXRsZT1cIlwiPlxyXG5cdFx0PExhYmVsIHRleHQ9XCJ0cmFsYWxhXCIgI3RpdGxlPjwvTGFiZWw+XHJcblx0PC9BY3Rpb25CYXI+XHJcblx0PEFic29sdXRlTGF5b3V0PlxyXG5cdFx0PFNjcm9sbFZpZXcgb3JpZW50YXRpb249XCJ2ZXJ0aWNhbFwiIHdpZHRoPVwiMTAwJVwiIGhlaWdodD1cIjk5JVwiPlxyXG5cdFx0XHQ8QWJzb2x1dGVMYXlvdXQgc3R5bGU9XCJ3aWR0aDoxMDAlO2hlaWdodDoxMjAwO1wiICNjb250YWluZXI+PC9BYnNvbHV0ZUxheW91dD5cclxuXHRcdDwvU2Nyb2xsVmlldz5cclxuXHQ8L0Fic29sdXRlTGF5b3V0PlxyXG4gICAgYCxcclxuICAvL3RlbXBsYXRlVXJsOiAndmlldy9jYWxlbmRhci93ZWVrLmNvbXBvbmVudC5odG1sJyxcclxuICAvL3N0eWxlVXJsczogWydjc3Mvd2Vlay5jb21wb25lbnQuY3NzJywnY3NzL21vbnRoLmNvbXBvbmVudC5jc3MnXSxcclxufSlcclxuZXhwb3J0IGNsYXNzIFdlZWtDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIHB1YmxpYyB0YXJnZXQgPSBuZXcgTGFiZWw7XHJcbiAgcHVibGljIHNhdmUgPSBuZXcgTGFiZWw7XHJcblx0cHVibGljIHdlZWs6bnVtYmVyW107XHJcbiAgcHVibGljIHRleHQ6YW55O1xyXG5cdHB1YmxpYyB5ZWFyOm51bWJlcjtcclxuXHRwdWJsaWMgeWVhcjI6bnVtYmVyO1xyXG5cdHB1YmxpYyBsYXN0ZGF5Om51bWJlcjtcclxuXHRwdWJsaWMgbW9udGg6bnVtYmVyO1xyXG5cdHB1YmxpYyBtb250aDIgPSBcIkphbnVhcnlcIjsgIFxyXG5cdHB1YmxpYyBzdGF0ZTpudW1iZXIgPSAwO1xyXG4gIHB1YmxpYyBob3VyOiBHcmlkTGF5b3V0O1xyXG5cdHB1YmxpYyBjb250OiBhbnk7XHJcblx0cHVibGljIGNvbnQyOiBhbnk7XHJcblx0QFZpZXdDaGlsZChcInRpdGxlXCIpIHRpdGxlOiBFbGVtZW50UmVmO1xyXG4gIEBWaWV3Q2hpbGQoXCJjb250YWluZXJcIikgY29udGFpbmVyOiBFbGVtZW50UmVmO1xyXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGU6IEFjdGl2YXRlZFJvdXRlLHByaXZhdGUgYXBwb2ludDogQ2FsZW5kYXJTZXJ2aWNlLCBcclxuXHRwcml2YXRlIF9tb2RhbFNlcnZpY2U6IE1vZGFsRGlhbG9nU2VydmljZSxwcml2YXRlIHZjUmVmOiBWaWV3Q29udGFpbmVyUmVmLCBwcml2YXRlIHBhZ2U6IFBhZ2UgKSB7fVxyXG5cdG5nT25Jbml0KCl7XHJcblx0XHQvL2NvbnNvbGUubG9nKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwic2lkZS1jb250YWluZXJcIikpXHJcblx0XHR0aGlzLmNvbnQgPSB0aGlzLmNvbnRhaW5lci5uYXRpdmVFbGVtZW50O1xyXG4gICAgdGhpcy50ZXh0ID0gdGhpcy50aXRsZS5uYXRpdmVFbGVtZW50O1xyXG4gICAgdGhpcy5ob3VyID0gbmV3IEdyaWRMYXlvdXQoKTtcclxuXHRcdGZvcihsZXQgaT0wO2k8NztpKz0xKXtcclxuICAgICAgbGV0IHNlY29uZENvbHVtbiA9IG5ldyBJdGVtU3BlYygxLCBHcmlkVW5pdFR5cGUuc3Rhcik7XHJcbiAgICAgIHRoaXMuaG91ci5hZGRDb2x1bW4oc2Vjb25kQ29sdW1uKTtcclxuXHRcdFx0bGV0IHRkMiA9IHRoaXMuaG91cnMoKTtcclxuXHRcdFx0R3JpZExheW91dC5zZXRDb2x1bW4odGQyLCBpKTtcclxuXHRcdFx0dGhpcy5ob3VyLmFkZENoaWxkKHRkMik7XHJcbiAgICB9IFxyXG5cdFx0XHJcblx0XHR0aGlzLkNyZWF0ZSgwKTtcclxuXHR9XHJcblx0RGF5cygpe1xyXG5cdFx0bGV0IG1vbjpudW1iZXI7XHJcblx0XHRsZXQgZGF0ZSA9IG5ldyBEYXRlKCk7XHJcblx0XHR0aGlzLm1vbnRoID0gZGF0ZS5nZXRNb250aCgpKzE7XHJcblx0XHR0aGlzLnllYXIgPSBkYXRlLmdldEZ1bGxZZWFyKCk7XHJcblx0XHRsZXQgZGF5ID0gZGF0ZS5nZXREYXkoKTtcclxuXHRcdGxldCB0b2RheSA9IGRhdGUuZ2V0RGF0ZSgpO1x0XHJcblx0XHRpZih0b2RheS1kYXk8MCkge1xyXG5cdFx0XHRpZih0aGlzLm1vbnRoID09IDEpe1xyXG5cdFx0XHRcdHRoaXMubW9udGggPSAxMjtcclxuXHRcdFx0XHR0aGlzLnllYXIgLT0gMTtcclxuXHRcdFx0fVxyXG5cdFx0XHRlbHNle1xyXG5cdFx0XHRcdHRoaXMubW9udGggLT0xO1xyXG5cdFx0XHR9XHJcblx0XHRcdGxldCBvYmogPSB7bW9uOiAwfVxyXG5cdFx0XHR0aGlzLkdldE1vbnRoKG9iaik7XHJcblx0XHRcdHRoaXMubGFzdGRheSA9IG9iai5tb24gKyB0b2RheSAtIGRheTtcclxuXHRcdH1cclxuXHRcdGVsc2UgdGhpcy5sYXN0ZGF5ID0gdG9kYXktZGF5O1xyXG5cdFx0cmV0dXJuIHRoaXMuV2VlaygpO1xyXG5cdH1cclxuXHRXZWVrKCl7XHJcblx0XHR2YXIgb2JqID0ge21vbjogMH1cclxuXHRcdHRoaXMuR2V0TW9udGgob2JqKTtcclxuXHRcdGxldCBqID0gdGhpcy5sYXN0ZGF5O1xyXG5cdFx0bGV0IGRheXMgPSBuZXcgQXJyYXk7XHJcblx0XHRpZih0aGlzLmxhc3RkYXktNyA8IDApe1xyXG5cdFx0XHRpZih0aGlzLm1vbnRoID09IDEpe3RoaXMubW9udGggPSAxMjsgdGhpcy55ZWFyIC09MTt9XHJcblx0XHRcdGVsc2UgdGhpcy5tb250aCAtPTE7XHJcblx0XHR9XHJcblx0XHRmb3IgKHZhciBpID0gMDsgaSA8IDc7aSs9MSkge1xyXG5cdFx0XHRpZihqID09IG9iai5tb24pIGogPSAwO1xyXG5cdFx0XHRqICs9MTtcclxuXHRcdFx0ZGF5cy5wdXNoKGopXHJcblx0XHR9XHJcblx0XHR0aGlzLmxhc3RkYXkgPSBqO1xyXG5cdFx0aWYoZGF5c1swXSA8IGRheXNbNl0gJiYgZGF5c1swXS03PD0wKXtcclxuXHRcdFx0aWYodGhpcy5tb250aCA9PSAxMil7dGhpcy5tb250aCA9IDE7IHRoaXMueWVhciArPTE7fVxyXG5cdFx0XHRlbHNlIHRoaXMubW9udGggKz0xO1xyXG5cdFx0fVxyXG5cdFx0dGhpcy5Nb250aCgpO1xyXG5cdFx0aWYoZGF5c1swXSA+IGRheXNbNl0gJiYgZGF5c1s2XS03PD0wKXtcclxuXHRcdFx0aWYodGhpcy5tb250aCA9PSAxMil7dGhpcy5tb250aCA9IDE7IHRoaXMueWVhciArPTE7fVxyXG5cdFx0XHRlbHNlIHRoaXMubW9udGggKz0xO1xyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIGRheXM7XHJcblx0fVxyXG5cdFdlZWsxKCl7XHJcblx0XHR2YXIgb2JqID0ge21vbjogMH1cclxuXHRcdGlmKHRoaXMubGFzdGRheSAtIDE0ID49IDApe1xyXG5cdFx0XHR0aGlzLmxhc3RkYXkgLT0xNDtcclxuXHRcdFx0dGhpcy5HZXRNb250aChvYmopO1xyXG5cdFx0fVxyXG5cdFx0ZWxzZSB7XHJcblx0XHRcdGlmKHRoaXMubW9udGggPT0gMSl7XHJcblx0XHRcdFx0dGhpcy5tb250aCA9IDEyO1xyXG5cdFx0XHRcdHRoaXMueWVhciAtPSAxO1xyXG5cdFx0XHRcdHRoaXMuR2V0TW9udGgob2JqKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRlbHNle1xyXG5cdFx0XHRcdHRoaXMubW9udGggLT0xO1xyXG5cdFx0XHRcdHRoaXMuR2V0TW9udGgob2JqKTtcclxuXHRcdFx0fVxyXG5cdFx0XHR0aGlzLmxhc3RkYXkgPSAodGhpcy5sYXN0ZGF5IC0gMTQgKyBvYmoubW9uKTtcclxuXHRcdH1cclxuXHRcdGxldCBkYXlzID0gbmV3IEFycmF5O1xyXG5cdFx0bGV0IGogPSB0aGlzLmxhc3RkYXk7XHJcblx0XHRmb3IgKHZhciBpID0gMDsgaSA8IDc7aSs9MSkge1xyXG5cdFx0XHRpZihqID09IG9iai5tb24pIGogPSAwO1xyXG5cdFx0XHRqICs9MTtcclxuXHRcdFx0ZGF5cy5wdXNoKGopXHJcblx0XHR9XHJcblx0XHR0aGlzLmxhc3RkYXkgPSBqO1xyXG5cdFx0aWYodGhpcy5sYXN0ZGF5IC0gNyA8IDApe1xyXG5cdFx0XHRpZih0aGlzLm1vbnRoID09IDEyKXtcclxuXHRcdFx0XHR0aGlzLm1vbnRoID0gMTtcclxuXHRcdFx0XHR0aGlzLnllYXIgKz0xO1xyXG5cdFx0XHR9XHJcblx0XHRcdGVsc2UgdGhpcy5tb250aCArPTE7XHJcblx0XHR9XHJcblx0XHR0aGlzLk1vbnRoKCk7XHJcblx0XHRyZXR1cm4gZGF5cztcclxuXHR9XHJcblx0R2V0TW9udGgob2JqOiBhbnkpe1xyXG5cdFx0aWYodGhpcy5tb250aCA8PSA3KXtcclxuXHRcdFx0aWYodGhpcy5tb250aCAlIDIgPT0gMSkgb2JqLm1vbiA9IDMxO1xyXG5cdFx0XHRlbHNlIG9iai5tb24gPSAzMDtcclxuXHRcdFx0aWYodGhpcy5tb250aCA9PSAyKXtcclxuXHRcdFx0XHRpZigoKHRoaXMueWVhciAlIDQgPT0gMCkgJiYgKHRoaXMueWVhciAlIDEwMCAhPSAwKSkgXHJcblx0XHRcdFx0XHR8fCAodGhpcy55ZWFyICUgNDAwID09IDApKW9iai5tb24gPSAyOTtcclxuXHRcdFx0XHRlbHNlIG9iai5tb24gPSAyODtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0ZWxzZXtcclxuXHRcdFx0aWYodGhpcy5tb250aCAlIDIgPT0gMSkgb2JqLm1vbiA9IDMwO1xyXG5cdFx0XHRlbHNlIG9iai5tb24gPSAzMTtcclxuXHRcdH1cclxuXHR9XHJcblx0Q3JlYXRlKG46IGFueSl7XHJcblx0XHR0aGlzLmNvbnQyID0gbmV3IEdyaWRMYXlvdXQoKTtcclxuIFx0XHR0aGlzLmNvbnQyLnN0eWxlLndpZHRoID0gXCIxMDAlXCI7XHJcblx0XHR0aGlzLmNvbnQyLnN0eWxlLmJvcmRlclJpZ2h0Q29sb3IgPSBcIiNlN2U3ZTdcIjtcclxuXHRcdHRoaXMuY29udDIuc3R5bGUuYm9yZGVyV2lkdGggPSBcIjFweFwiO1xyXG4gICAgdGhpcy5jb250Mi5zdHlsZS5iYWNrZ3JvdW5kQ29sb3IgPSBcIndoaXRlXCJcclxuXHRcdHZhciB3ZWVrOmFueTtcclxuXHRcdHN3aXRjaCAobikge1xyXG5cdFx0XHRjYXNlIC0xOndlZWsgPSB0aGlzLldlZWsxKCk7YnJlYWs7XHJcblx0XHRcdGNhc2UgMDp3ZWVrID0gdGhpcy5EYXlzKCk7YnJlYWs7XHJcblx0XHRcdGNhc2UgMTp3ZWVrID0gdGhpcy5XZWVrKCk7YnJlYWs7XHJcblx0XHR9XHJcblx0XHRsZXQgZ3JpZCA9IHRoaXMuaG91cjtcclxuICAgIGdyaWQuaWQgPSBcImNvbnR0XCJcclxuICAgIGdyaWQuc3R5bGUubWFyZ2luVG9wID0gNDA7XHJcblx0XHRsZXQgaT0wO1xyXG4gICAgbGV0IGRheXMgPSBuZXcgR3JpZExheW91dCgpO1xyXG5cdFx0d2Vlay5mb3JFYWNoKChlbGVtZW50OmFueSkgPT4ge1xyXG4gICAgICBsZXQgc2Vjb25kQ29sdW1uID0gbmV3IEl0ZW1TcGVjKDEsIEdyaWRVbml0VHlwZS5zdGFyKTtcclxuICAgICAgZGF5cy5hZGRDb2x1bW4oc2Vjb25kQ29sdW1uKTtcclxuICAgICAgbGV0IHRkID0gbmV3IExhYmVsKCk7XHJcblx0XHRcdHRkLnRleHQgPSBlbGVtZW50O1xyXG4gICAgICB0ZC5jbGFzc05hbWUgPSBcImRheS13ZWVrXCI7XHJcbiAgICAgIEdyaWRMYXlvdXQuc2V0Q29sdW1uKHRkLCBpKTtcclxuICAgICAgZGF5cy5hZGRDaGlsZCh0ZCk7XHJcblx0XHRcdGkrPTE7XHJcblx0ICB9KTtcclxuXHRcdGRheXMuaWQ9XCJkYXlzXCI7XHJcbiAgICB0aGlzLmNvbnQyLmFkZENoaWxkKGRheXMpO1xyXG5cdCAgdGhpcy5jb250Mi5hZGRDaGlsZChncmlkKTtcclxuICAgIGlmKHRoaXMuc3RhdGUgPT0gMSkgdGhpcy5jb250Mi50cmFuc2xhdGVYID0gLTEwMDA7XHJcbiAgICBpZih0aGlzLnN0YXRlID09IDIpIHRoaXMuY29udDIudHJhbnNsYXRlWCA9IDEwMDA7XHJcbiAgICB0aGlzLmNvbnQuYWRkQ2hpbGQodGhpcy5jb250Mik7XHJcbiAgICB0aGlzLnNob3dBdHRhY2god2Vla1swXSk7XHJcbiAgICB0aGlzLmNvbnQyLmFuaW1hdGUoe1xyXG4gICAgICB0cmFuc2xhdGU6IHsgeDogMCwgeTogMH0sICAgIFxyXG4gICAgICBkdXJhdGlvbjogODAwLFxyXG4gICAgICBjdXJ2ZTogQW5pbWF0aW9uQ3VydmUuZWFzZU91dFxyXG4gICAgfSk7XHJcbiAgICB0aGlzLmhvdXIgPSBuZXcgR3JpZExheW91dCgpO1xyXG5cdFx0Zm9yKGxldCBpPTA7aTw3O2krPTEpe1xyXG4gICAgICBsZXQgc2Vjb25kQ29sdW1uID0gbmV3IEl0ZW1TcGVjKDEsIEdyaWRVbml0VHlwZS5zdGFyKTtcclxuICAgICAgdGhpcy5ob3VyLmFkZENvbHVtbihzZWNvbmRDb2x1bW4pO1xyXG5cdFx0XHRsZXQgdGQyID0gdGhpcy5ob3VycygpO1xyXG5cdFx0XHRHcmlkTGF5b3V0LnNldENvbHVtbih0ZDIsIGkpO1xyXG5cdFx0XHR0aGlzLmhvdXIuYWRkQ2hpbGQodGQyKTtcclxuICAgIH1cclxuICAgIHRoaXMuY29udC5vbihHZXN0dXJlVHlwZXMuc3dpcGUsKGFyZ3M6IFN3aXBlR2VzdHVyZUV2ZW50RGF0YSk9PntcclxuXHRcdFx0Y29uc29sZS5sb2coXCJTd2lwZSBEaXJlY3Rpb246IFwiICsgYXJncy5kaXJlY3Rpb24pO1xyXG5cdFx0XHRcdGlmKGFyZ3MuZGlyZWN0aW9uPT0yKXtcclxuXHRcdFx0XHRcdHRoaXMuTmV4dCgpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRpZihhcmdzLmRpcmVjdGlvbj09MSl7XHJcblx0XHRcdFx0XHR0aGlzLlByZXYoKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG4gICBcclxuXHR9XHJcbiAgc2hvd0F0dGFjaChkYXk6IG51bWJlcil7XHJcblx0XHRsZXQgZGF0ZSA9IHRoaXMueWVhcitcIi1cIit0aGlzLm1vbnRoK1wiLVwiK2RheTtcclxuXHRcdGxldCBhcnIgPSBuZXcgQXJyYXk7XHJcblx0XHR0aGlzLmFwcG9pbnQuZ2V0QXBwb2ludG1lbnQoZGF0ZSkuc3Vic2NyaWJlKFxyXG5cdFx0XHRyZXN1bHQgPT4ge1xyXG5cdFx0XHRcdGlmKHJlc3VsdC5sZW5ndGg+MCl7XHJcbiAgICAgICAgICBsZXQgZ3JhbmRQYXJlbnQgPSB0aGlzLmNvbnQyLmdldENoaWxkQnlJZCgnY29udHQnKTtcclxuICAgICAgICAgIC8vIGZvcihsZXQgZGEgaW4gZ3JhbmRQYXJlbnQpe1xyXG4gICAgICAgICAgLy8gICAgY29uc29sZS5sb2coZGEpXHJcbiAgICAgICAgICAvLyB9XHJcblx0XHRcdFx0XHRhcnIgPSByZXN1bHQ7XHJcblx0XHRcdFx0XHRsZXQgZDpudW1iZXIscGFyZW50OmFueSxjaGlsZHJlbjphbnksc3RhcnQ6YW55LGVuZDphbnksaTphbnksbGFiZWw6YW55LHRpbWU6c3RyaW5nO1xyXG4gICAgICAgICAgZm9yKGxldCBrZXkgaW4gYXJyKXtcclxuICAgICAgICAgICAgZCA9IGFycltrZXldW1wiZGF0ZVwiXS5zcGxpdChcIi1cIik7XHJcblx0XHRcdFx0XHRcdGQgPSBwYXJzZUludChkWzJdKS1kYXk7XHJcbiAgICAgICAgICAgIHBhcmVudCA9IGdyYW5kUGFyZW50LmdldENoaWxkQXQoZClcclxuICAgICAgICAgICAgc3RhcnQgPSBhcnJba2V5XVtcInN0YXJ0XCJdLnNwbGl0KFwiOlwiKTtcclxuXHRcdFx0XHRcdFx0c3RhcnQgPSBwYXJzZUludChzdGFydFswXSk7XHJcbiAgICAgICAgICAgIHBhcmVudC5yZW1vdmVDaGlsZChwYXJlbnQuZ2V0Q2hpbGRCeUlkKHN0YXJ0KSk7XHJcbiAgICAgICAgICAgIGxhYmVsID0gbmV3IExhYmVsKCk7XHJcbiAgICAgICAgICAgIGxhYmVsLnRleHQgPSBhcnJba2V5XVtcIm5hbWVcIl0rXCJcXHJcXG5cIithcnJba2V5XVtcInN0YXJ0XCJdLnN1YnN0cmluZygwLCA1KTtcclxuICAgICAgICAgICAgbGFiZWwudGV4dFdyYXAgPSB0cnVlO1xyXG4gICAgICAgICAgICBsYWJlbC5iYWNrZ3JvdW5kQ29sb3JcdD0gYXJyW2tleV1bXCJjb2xvclwiXTtcclxuICAgICAgICAgICAgbGFiZWwuY2xhc3NOYW1lID0gXCJob3VyLWJ0biBhdHRhY2htZW50LWJ0blwiO1xyXG4gICAgICAgICAgICBHcmlkTGF5b3V0LnNldFJvdyhsYWJlbCwgc3RhcnQpO1xyXG5cdFx0XHQgICAgICBwYXJlbnQuYWRkQ2hpbGQobGFiZWwpO1xyXG4gICAgICAgICAgfVxyXG5cdFx0XHRcdH1cclxuICAgICAgICBlbHNlIGNvbnNvbGUubG9nKFwibm90XCIpXHJcblx0XHRcdH0sXHJcbiAgICAgIGVycm9yID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhlcnJvcilcclxuICAgICAgfSk7IFxyXG5cdH1cclxuICBob3Vycygpe1xyXG5cdFx0bGV0IHRkID0gbmV3IEdyaWRMYXlvdXQoKTtcclxuXHRcdGxldCBsYWJlbDphbnk7XHJcblx0XHRmb3IobGV0IGk9MDtpPDI0O2krKyl7XHJcblx0XHRcdHZhciBzZWNvbmRSb3cgPSBuZXcgSXRlbVNwZWMoMSwgR3JpZFVuaXRUeXBlLmF1dG8pO1xyXG5cdFx0XHR0ZC5hZGRSb3coc2Vjb25kUm93KTtcclxuXHRcdFx0bGFiZWwgPSBuZXcgTGFiZWwoKTtcclxuXHRcdFx0bGFiZWwuY2xhc3NOYW1lID0gXCJob3VyLWJ0blwiO1xyXG5cdFx0XHRpZihpPDEwKSBsYWJlbC50ZXh0ID0gXCIwXCIraStcIjozMFwiO1xyXG5cdFx0XHRlbHNlIGxhYmVsLnRleHQgPSBpK1wiOjMwXCI7XHJcblx0XHRcdGxhYmVsLmlkID0gaTtcclxuICAgICAgbGFiZWwub24oR2VzdHVyZVR5cGVzLnRhcCwgIChhcmdzOiBHZXN0dXJlRXZlbnREYXRhKT0+IHtcclxuICAgICAgICB0aGlzLmNsaWNrZWQoYXJncy5vYmplY3QpO1xyXG4gICAgICB9KTtcclxuICAgICAgbGFiZWwub24oR2VzdHVyZVR5cGVzLnN3aXBlLChhcmdzOiBTd2lwZUdlc3R1cmVFdmVudERhdGEpPT57XHJcblx0XHRcdGNvbnNvbGUubG9nKFwiU3dpcGUgRGlyZWN0aW9uOiBcIiArIGFyZ3MuZGlyZWN0aW9uKTtcclxuXHRcdFx0XHRpZihhcmdzLmRpcmVjdGlvbj09Mil7XHJcblx0XHRcdFx0XHR0aGlzLk5leHQoKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0aWYoYXJncy5kaXJlY3Rpb249PTEpe1xyXG5cdFx0XHRcdFx0dGhpcy5QcmV2KCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KTtcclxuXHRcdFx0Ly9sYWJlbC5zdHlsZS5wYWRkaW5nVG9wID0gMjA7IFxyXG5cdFx0XHRHcmlkTGF5b3V0LnNldFJvdyhsYWJlbCwgaSk7XHJcblx0XHRcdHRkLmFkZENoaWxkKGxhYmVsKTtcclxuXHRcdH1cclxuXHRcdHJldHVybiB0ZDtcclxuICB9XHJcblx0UHJldigpe1xyXG4gICAgdGhpcy5zdGF0ZSA9IDE7XHJcblx0XHR0aGlzLkNyZWF0ZSgtMSk7XHJcblx0fVxyXG5cdE5leHQoKXtcclxuICAgIHRoaXMuc3RhdGUgPSAyO1xyXG5cdFx0dGhpcy5DcmVhdGUoMSk7XHJcblx0fVxyXG5cdE1vbnRoKCl7XHJcblx0XHRzd2l0Y2ggKHRoaXMubW9udGgpIHtcclxuXHRcdFx0Y2FzZSAxOiB0aGlzLm1vbnRoMiA9IFwiSmFudWFyeVwiO2JyZWFrO1xyXG5cdFx0XHRjYXNlIDI6IHRoaXMubW9udGgyID0gXCJGZWJydWFyeVwiO2JyZWFrO1xyXG5cdFx0XHRjYXNlIDM6IHRoaXMubW9udGgyID0gXCJNYXJjaFwiO2JyZWFrO1xyXG5cdFx0XHRjYXNlIDQ6IHRoaXMubW9udGgyID0gXCJBcHJpbFwiO2JyZWFrO1xyXG5cdFx0XHRjYXNlIDU6IHRoaXMubW9udGgyID0gXCJNYXlcIjticmVhaztcclxuXHRcdFx0Y2FzZSA2OiB0aGlzLm1vbnRoMiA9IFwiSnVuZVwiO2JyZWFrO1xyXG5cdFx0XHRjYXNlIDc6IHRoaXMubW9udGgyID0gXCJKdWx5XCI7YnJlYWs7XHJcblx0XHRcdGNhc2UgODogdGhpcy5tb250aDIgPSBcIkF1Z3VzdFwiO2JyZWFrO1xyXG5cdFx0XHRjYXNlIDk6IHRoaXMubW9udGgyID0gXCJTZXB0ZW1iZXJcIjticmVhaztcclxuXHRcdFx0Y2FzZSAxMDp0aGlzLm1vbnRoMiA9IFwiT2N0b2JlclwiO2JyZWFrO1xyXG5cdFx0XHRjYXNlIDExOnRoaXMubW9udGgyID0gXCJOb3ZlbWJlclwiO2JyZWFrO1xyXG5cdFx0XHRjYXNlIDEyOnRoaXMubW9udGgyID0gXCJEZWNlbWJlclwiO2JyZWFrO1xyXG5cdFx0fVxyXG5cdFx0dGhpcy55ZWFyMiA9IHRoaXMueWVhcjtcclxuICAgIHNldFRpbWVvdXQoKCk9PntcclxuXHRcdFx0XHRcdHRoaXMudGV4dC50ZXh0ID0gdGhpcy5tb250aDJcclxuXHRcdFx0XHR9LDMwMClcclxuXHR9XHJcbiAgY2xpY2tlZChldmVudDogYW55KXtcclxuICAgIGlmKGV2ZW50ID09IHRoaXMudGFyZ2V0KXtcclxuXHRcdFx0bGV0IG9iaiA9IHtcclxuXHRcdFx0XHRzdGFydDogdGhpcy5zYXZlLnRleHQsXHJcblx0XHRcdFx0dmNSZWY6IHRoaXMudmNSZWZcclxuXHRcdFx0fVxyXG5cdFx0XHRsZXQgb3B0aW9uczogTW9kYWxEaWFsb2dPcHRpb25zID0ge1xyXG5cdFx0XHRcdFx0XHRcdHZpZXdDb250YWluZXJSZWY6IHRoaXMudmNSZWYsXHJcblx0XHRcdFx0XHRcdFx0Y29udGV4dDogb2JqLFxyXG5cdFx0XHRcdFx0XHRcdGZ1bGxzY3JlZW46IHRydWVcclxuXHRcdFx0XHRcdH07XHJcblx0XHRcdHRoaXMuX21vZGFsU2VydmljZS5zaG93TW9kYWwoTW9kYWxWaWV3Q29tcG9uZW50LCBvcHRpb25zKVxyXG5cdFx0XHQudGhlbigoYXBwb2ludG1lbnQ6IGFueSkgPT4ge1xyXG5cdFx0XHRcdFx0aWYoYXBwb2ludG1lbnQgIT0gdW5kZWZpbmVkKXtcclxuXHRcdFx0XHRcdFx0dGhpcy5BcHBvaW50bWVudChhcHBvaW50bWVudCk7XHJcblx0XHRcdFx0XHR9IFxyXG4gICAgXHR9KTtcclxuXHRcdFxyXG5cdFx0fVxyXG5cdFx0ZWxzZXtcclxuXHRcdFx0dGhpcy50YXJnZXQudGV4dCA9IHRoaXMuc2F2ZS50ZXh0O1xyXG5cdFx0XHR0aGlzLnRhcmdldC5jbGFzc05hbWUgPSB0aGlzLnNhdmUuY2xhc3NOYW1lXHJcblx0XHRcdHRoaXMuc2F2ZS50ZXh0ID0gZXZlbnQudGV4dDtcclxuXHRcdFx0dGhpcy5zYXZlLmNsYXNzTmFtZSA9IGV2ZW50LmNsYXNzTmFtZTtcclxuXHRcdFx0dGhpcy50YXJnZXQgPSBldmVudDtcclxuXHRcdFx0ZXZlbnQuY2xhc3NOYW1lID0gXCJob3VyLWJ0biBuZXctYXR0YWNoXCI7XHJcblx0XHRcdGV2ZW50LnRleHQ9XCIrXCJcclxuXHRcdH1cclxuICB9XHJcbiAgQXBwb2ludG1lbnQob2JqOmFueSl7XHJcblx0XHRsZXQgZ3JhbmRQYXJlbnQgPSB0aGlzLmNvbnQyLmdldENoaWxkQnlJZCgnY29udHQnKVxyXG5cdFx0bGV0IHBhcmVudCA9IDxHcmlkTGF5b3V0PnRoaXMudGFyZ2V0LnBhcmVudDtcclxuXHRcdGxldCBkYXlzID0gPEdyaWRMYXlvdXQ+dGhpcy5wYWdlLmdldFZpZXdCeUlkKFwiZGF5c1wiKVxyXG5cdFx0bGV0IGxhYmVsID0gPExhYmVsPmRheXMuZ2V0Q2hpbGRBdChncmFuZFBhcmVudC5nZXRDaGlsZEluZGV4KHBhcmVudCkpO1xyXG5cdFx0bGV0IGNvbG9yID0gb2JqLmNvbG9yW1wiX2hleFwiXTtcclxuXHRcdGxldCBkYXRlID0gdGhpcy55ZWFyK1wiLVwiK3RoaXMubW9udGgrXCItXCIrbGFiZWwudGV4dDtcclxuXHRcdGxldCBuYW1lID0gb2JqLm5hbWU7XHJcblx0XHRpZihvYmoubmFtZSAhPVwiXCIpe1xyXG5cdFx0XHRmb3IobGV0IGRhIGluIG9iail7IFxyXG5cdFx0XHRcdGNvbnNvbGUubG9nKGRhK1wiPT5cIitvYmpbZGFdKVxyXG5cdFx0XHR9XHJcblx0XHRcdGxldCBmcm9tID0gb2JqLmZyb207XHJcblx0XHRcdGxldCB0byA9IG9iai50bztcclxuXHRcdFx0dGhpcy5hcHBvaW50LnBvc3RBcHBvaW50bWVudChvYmoubmFtZSxvYmouZnJvbSxvYmoudG8sZGF0ZSxjb2xvcixvYmoubGF0aXR1ZGUsb2JqLmxvbmdpdHVkZSxvYmouaWQpLnN1YnNjcmliZShcclxuXHRcdFx0XHRyZXN1bHQgPT4ge1xyXG5cdFx0XHRcdFx0aWYgKHJlc3VsdCA9PT0gdHJ1ZSkge1xyXG5cdFx0XHRcdFx0bGV0IGk9MTtcclxuXHRcdFx0XHRcdGxldCB2YWwgPSBwYXJzZUludCh0by52YWx1ZSk7XHJcblx0XHRcdFx0XHRsZXQgcmVtb3ZlOmFueTtcclxuXHRcdFx0XHRcdHBhcmVudC5yZW1vdmVDaGlsZCh0aGlzLnRhcmdldCk7XHJcblx0XHRcdFx0XHQvLyB3aGlsZShpKXtcclxuXHRcdFx0XHRcdC8vIFx0cmVtb3ZlID0gdGhpcy50YXJnZXQubmV4dFNpYmxpbmc7XHJcblx0XHRcdFx0XHQvLyBcdGlmKHZhbCA+IHBhcnNlSW50KHJlbW92ZS52YWx1ZSkpe1xyXG5cdFx0XHRcdFx0Ly8gXHRpKz0xOyBcclxuXHRcdFx0XHRcdC8vIFx0dGhpcy50YXJnZXQucGFyZW50RWxlbWVudC5yZW1vdmVDaGlsZChyZW1vdmUpO1xyXG5cdFx0XHRcdFx0Ly8gXHR9XHJcblx0XHRcdFx0XHQvLyBcdGVsc2UgYnJlYWs7XHJcblx0XHRcdFx0XHQvLyB9O1xyXG5cdFx0XHRcdFx0bGFiZWwgPSBuZXcgTGFiZWwoKTtcclxuXHRcdFx0XHRcdGxhYmVsLnRleHQgPSBvYmpbXCJuYW1lXCJdK1wiXFxyXFxuXCIrb2JqW1wiZnJvbVwiXS5zdWJzdHJpbmcoMCwgNSk7XHJcblx0XHRcdFx0XHRsYWJlbC50ZXh0V3JhcCA9IHRydWU7XHJcblx0XHRcdFx0XHRsYWJlbC5iYWNrZ3JvdW5kQ29sb3JcdD0gb2JqW1wiY29sb3JcIl07XHJcblx0XHRcdFx0XHRsYWJlbC5jbGFzc05hbWUgPSBcImhvdXItYnRuIGF0dGFjaG1lbnQtYnRuXCI7XHJcblx0XHRcdFx0XHQgXHJcblx0XHRcdFx0XHRsZXQgdmFsMiA9IG9ialtcImZyb21cIl0uc3BsaXQoXCI6XCIpO1xyXG5cdFx0XHRcdFx0bGV0IHN0YXJ0ID0gKHBhcnNlSW50KHZhbDJbMF0pKTtcclxuXHRcdFx0XHRcdEdyaWRMYXlvdXQuc2V0Um93KGxhYmVsLCBzdGFydCk7XHJcblx0XHRcdFx0XHRwYXJlbnQuYWRkQ2hpbGQobGFiZWwpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRjb25zb2xlLmVycm9yKFwiU29tZXRoaW5nIHdlbnQgd3JvbmdcIik7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9LFxyXG5cdFx0XHRlcnJvciA9Pntjb25zb2xlLmVycm9yKGVycm9yKTt9KTtcclxuXHRcdH1cclxuICB9XHJcbn1cclxuIl19