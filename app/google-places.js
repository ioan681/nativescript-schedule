var application = require("application");
var imageSource = require("image-source");
var _googleServerApiKey, _defaultLanguage, _radius, _errorCallbackl, _defaultLocation, _placesApiUrl = 'https://maps.googleapis.com/maps/api/place/autocomplete/json', _placesDetailsApiUrl = 'https://maps.googleapis.com/maps/api/place/details/json', _placesImagesApiUrl = 'https://maps.googleapis.com/maps/api/place/photo', _queryAutoCompleteApiUrl = 'https://maps.googleapis.com/maps/api/place/queryautocomplete/json';
function handleErrors(response) {
    if (!response.ok) {
        if (this._errorCallback)
            this._errorCallback(response.statusText);
    }
    return response;
}
function capitalize(text) {
    return text.replace(/(?:^|\s)\S/g, function (a) { return a.toUpperCase(); });
}
;
exports.init = function (params) {
    _googleServerApiKey = params.googleServerApiKey;
    this._language = params.language || 'es';
    this._radius = params.radius || '100000';
    this._location = params.location || '42.663840,23.314392';
    this._errorCallback = params.errorCallback;
};
exports.search = function (text) {
    var searchBy = capitalize(text).replace(new RegExp(" ", 'g'), "");
    var url = _placesApiUrl + "?input=" + searchBy + "&location=" + this._location + "&language=" + this._language + "&radius=" + this._radius + "&key=" + _googleServerApiKey;
    return fetch(url)
        .then(handleErrors)
        .then(function (response) {
        return response.json();
    }).then(function (data) {
        var items = [];
        for (var i = 0; i < data.predictions.length; i++) {
            items.push({
                description: data.predictions[i].description,
                placeId: data.predictions[i].place_id,
            });
        }
        return JSON.stringify(items);
    });
};
exports.queryAutoComplete = function (text, types) {
    var searchBy = capitalize(text).replace(new RegExp(" ", 'g'), "");
    var url = _queryAutoCompleteApiUrl + "?input=" + searchBy + "&location=" + this._location + "&types=" + types + "&language=" + this._language + "&radius=" + _radius + "&key=" + _googleServerApiKey;
    return fetch(url)
        .then(handleErrors)
        .then(function (response) {
        return response.json();
    }).then(function (data) {
        var items = [];
        for (var i = 0; i < data.predictions.length; i++) {
            items.push({
                description: data.predictions[i].description,
                placeId: data.predictions[i].place_id,
                'data': data.predictions[i]
            });
        }
        return items;
    });
};
exports.details = function (placeid) {
    var url = _placesDetailsApiUrl + "?placeid=" + placeid + "&language=" + this._language + "&key=" + _googleServerApiKey;
    console.log(url);
    return fetch(url)
        .then(handleErrors)
        .then(function (response) {
        return response.json();
    }).then(function (data) {
        var place = new Object();
        var address_components = data.result.address_components;
        // for(var key in address_components){
        //   var address_component = address_components[key]
        //   if (address_component.types[0] == "route"){
        //       place.route = address_component.long_name;
        //   }
        //   if (address_component.types[0] == "locality"){
        //       place.locality = address_component.long_name;
        //   }
        //   if (address_component.types[0] == "country"){
        //       place.country = address_component.long_name;
        //   }
        //   if (address_component.types[0] == "postal_code_prefix"){
        //       place.zipCode = address_component.long_name;
        //   }
        //   if (address_component.types[0] == "street_number"){
        //       place.number = address_component.long_name;
        //   }
        //   if(address_component.types[0] == "sublocality_level_1"){
        //     place.sublocality = address_component.long_name;
        //   }
        // }
        place["latitude"] = data.result.geometry.location.lat;
        place["longitude"] = data.result.geometry.location.lng;
        place["name"] = data.result.name;
        place["phone"] = data.result.international_phone_number;
        place["formattedAddress"] = data.result.formatted_address;
        if (data.result.photos && data.result.photos.length > 0) {
            place["photoReference"] = data.result.photos[0].photo_reference;
        }
        return place;
    });
};
exports.loadPlacePhoto = function (photoreference, onSuccessCallback, onFailCallback) {
    var url = _placesImagesApiUrl + "?maxwidth=100&photoreference=" + photoreference + "&key=" + _googleServerApiKey;
    return imageSource.fromUrl(url);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ29vZ2xlLXBsYWNlcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL2Rldi9nb29nbGUtcGxhY2VzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLElBQUksV0FBVyxHQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQztBQUN6QyxJQUFJLFdBQVcsR0FBRyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7QUFFMUMsSUFBSSxtQkFBbUIsRUFDbkIsZ0JBQWdCLEVBQ2hCLE9BQU8sRUFDUCxlQUFlLEVBQ2YsZ0JBQWdCLEVBQ2hCLGFBQWEsR0FBRyw4REFBOEQsRUFDOUUsb0JBQW9CLEdBQUcseURBQXlELEVBQ2hGLG1CQUFtQixHQUFHLGtEQUFrRCxFQUN4RSx3QkFBd0IsR0FBRyxtRUFBbUUsQ0FBQztBQUVuRyxzQkFBc0IsUUFBUTtJQUM1QixFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBRWpCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7WUFDckIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUE7SUFDNUMsQ0FBQztJQUVELE1BQU0sQ0FBQyxRQUFRLENBQUM7QUFDbEIsQ0FBQztBQUVELG9CQUFvQixJQUFJO0lBQ3RCLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxVQUFTLENBQUMsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDOUUsQ0FBQztBQUFBLENBQUM7QUFFRixPQUFPLENBQUMsSUFBSSxHQUFHLFVBQVMsTUFBTTtJQUM1QixtQkFBbUIsR0FBRyxNQUFNLENBQUMsa0JBQWtCLENBQUM7SUFDaEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQztJQUN6QyxJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxNQUFNLElBQUksUUFBUSxDQUFDO0lBQ3pDLElBQUksQ0FBQyxTQUFTLEdBQUcsTUFBTSxDQUFDLFFBQVEsSUFBSSxxQkFBcUIsQ0FBQztJQUMxRCxJQUFJLENBQUMsY0FBYyxHQUFHLE1BQU0sQ0FBQyxhQUFhLENBQUM7QUFDN0MsQ0FBQyxDQUFBO0FBRUQsT0FBTyxDQUFDLE1BQU0sR0FBRyxVQUFTLElBQUk7SUFDMUIsSUFBSSxRQUFRLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDbEUsSUFBSSxHQUFHLEdBQUcsYUFBYSxHQUFHLFNBQVMsR0FBRyxRQUFRLEdBQUcsWUFBWSxHQUFFLElBQUksQ0FBQyxTQUFTLEdBQUcsWUFBWSxHQUFFLElBQUksQ0FBQyxTQUFTLEdBQUUsVUFBVSxHQUFFLElBQUksQ0FBQyxPQUFPLEdBQUUsT0FBTyxHQUFHLG1CQUFtQixDQUFDO0lBQ3RLLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO1NBQ2hCLElBQUksQ0FBQyxZQUFZLENBQUM7U0FDbEIsSUFBSSxDQUFDLFVBQVMsUUFBUTtRQUNyQixNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3pCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFTLElBQUk7UUFDbkIsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFBO1FBQ2QsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBQyxDQUFDO1lBQy9DLEtBQUssQ0FBQyxJQUFJLENBQUM7Z0JBQ1QsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVztnQkFDNUMsT0FBTyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUTthQUV0QyxDQUFDLENBQUE7UUFDSixDQUFDO1FBRUQsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDL0IsQ0FBQyxDQUFDLENBQUE7QUFDTixDQUFDLENBQUE7QUFFRCxPQUFPLENBQUMsaUJBQWlCLEdBQUcsVUFBUyxJQUFJLEVBQUUsS0FBSztJQUM1QyxJQUFJLFFBQVEsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksTUFBTSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNsRSxJQUFJLEdBQUcsR0FBRyx3QkFBd0IsR0FBRyxTQUFTLEdBQUcsUUFBUSxHQUFHLFlBQVksR0FBRSxJQUFJLENBQUMsU0FBUyxHQUFFLFNBQVMsR0FBRyxLQUFLLEdBQUcsWUFBWSxHQUFFLElBQUksQ0FBQyxTQUFTLEdBQUUsVUFBVSxHQUFFLE9BQU8sR0FBRSxPQUFPLEdBQUcsbUJBQW1CLENBQUE7SUFDOUwsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7U0FDaEIsSUFBSSxDQUFDLFlBQVksQ0FBQztTQUNsQixJQUFJLENBQUMsVUFBUyxRQUFRO1FBQ3JCLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDekIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVMsSUFBSTtRQUNuQixJQUFJLEtBQUssR0FBRyxFQUFFLENBQUE7UUFDZCxHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFDLENBQUM7WUFDL0MsS0FBSyxDQUFDLElBQUksQ0FBQztnQkFDVCxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXO2dCQUM1QyxPQUFPLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRO2dCQUNyQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7YUFDNUIsQ0FBQyxDQUFBO1FBQ0osQ0FBQztRQUNELE1BQU0sQ0FBQyxLQUFLLENBQUE7SUFDZCxDQUFDLENBQUMsQ0FBQTtBQUNOLENBQUMsQ0FBQTtBQUVELE9BQU8sQ0FBQyxPQUFPLEdBQUcsVUFBUyxPQUFPO0lBQzlCLElBQUksR0FBRyxHQUFHLG9CQUFvQixHQUFHLFdBQVcsR0FBRyxPQUFPLEdBQUcsWUFBWSxHQUFFLElBQUksQ0FBQyxTQUFTLEdBQUUsT0FBTyxHQUFHLG1CQUFtQixDQUFBO0lBQ3BILE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUE7SUFDaEIsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7U0FDaEIsSUFBSSxDQUFDLFlBQVksQ0FBQztTQUNsQixJQUFJLENBQUMsVUFBUyxRQUFRO1FBQ3JCLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDekIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVMsSUFBSTtRQUVuQixJQUFJLEtBQUssR0FBRyxJQUFJLE1BQU0sRUFBRSxDQUFDO1FBQ3pCLElBQUksa0JBQWtCLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQTtRQUN2RCxzQ0FBc0M7UUFFdEMsb0RBQW9EO1FBR3BELGdEQUFnRDtRQUNoRCxtREFBbUQ7UUFDbkQsTUFBTTtRQUVOLG1EQUFtRDtRQUNuRCxzREFBc0Q7UUFDdEQsTUFBTTtRQUVOLGtEQUFrRDtRQUNsRCxxREFBcUQ7UUFDckQsTUFBTTtRQUVOLDZEQUE2RDtRQUM3RCxxREFBcUQ7UUFDckQsTUFBTTtRQUVOLHdEQUF3RDtRQUN4RCxvREFBb0Q7UUFDcEQsTUFBTTtRQUVOLDZEQUE2RDtRQUM3RCx1REFBdUQ7UUFDdkQsTUFBTTtRQUNOLElBQUk7UUFFSixLQUFLLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQztRQUN0RCxLQUFLLENBQUMsV0FBVyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQztRQUN2RCxLQUFLLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDakMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsMEJBQTBCLENBQUM7UUFDeEQsS0FBSyxDQUFDLGtCQUFrQixDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQztRQUUxRCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUEsQ0FBQztZQUN0RCxLQUFLLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUE7UUFDakUsQ0FBQztRQUVELE1BQU0sQ0FBQyxLQUFLLENBQUE7SUFFZCxDQUFDLENBQUMsQ0FBQTtBQUNOLENBQUMsQ0FBQTtBQUVELE9BQU8sQ0FBQyxjQUFjLEdBQUcsVUFBUyxjQUFjLEVBQUUsaUJBQWlCLEVBQUUsY0FBYztJQUNqRixJQUFJLEdBQUcsR0FBRyxtQkFBbUIsR0FBRywrQkFBK0IsR0FBRyxjQUFjLEdBQUcsT0FBTyxHQUFHLG1CQUFtQixDQUFDO0lBQ2pILE1BQU0sQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFBO0FBQ2pDLENBQUMsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbInZhciBhcHBsaWNhdGlvbiA9IHJlcXVpcmUoXCJhcHBsaWNhdGlvblwiKTtcbnZhciBpbWFnZVNvdXJjZSA9IHJlcXVpcmUoXCJpbWFnZS1zb3VyY2VcIik7XG5cbnZhciBfZ29vZ2xlU2VydmVyQXBpS2V5LFxuICAgIF9kZWZhdWx0TGFuZ3VhZ2UsXG4gICAgX3JhZGl1cyxcbiAgICBfZXJyb3JDYWxsYmFja2wsXG4gICAgX2RlZmF1bHRMb2NhdGlvbixcbiAgICBfcGxhY2VzQXBpVXJsID0gJ2h0dHBzOi8vbWFwcy5nb29nbGVhcGlzLmNvbS9tYXBzL2FwaS9wbGFjZS9hdXRvY29tcGxldGUvanNvbicsXG4gICAgX3BsYWNlc0RldGFpbHNBcGlVcmwgPSAnaHR0cHM6Ly9tYXBzLmdvb2dsZWFwaXMuY29tL21hcHMvYXBpL3BsYWNlL2RldGFpbHMvanNvbicsXG4gICAgX3BsYWNlc0ltYWdlc0FwaVVybCA9ICdodHRwczovL21hcHMuZ29vZ2xlYXBpcy5jb20vbWFwcy9hcGkvcGxhY2UvcGhvdG8nLFxuICAgIF9xdWVyeUF1dG9Db21wbGV0ZUFwaVVybCA9ICdodHRwczovL21hcHMuZ29vZ2xlYXBpcy5jb20vbWFwcy9hcGkvcGxhY2UvcXVlcnlhdXRvY29tcGxldGUvanNvbic7XG5cbmZ1bmN0aW9uIGhhbmRsZUVycm9ycyhyZXNwb25zZSkge1xuICBpZiAoIXJlc3BvbnNlLm9rKSB7XG5cbiAgICBpZih0aGlzLl9lcnJvckNhbGxiYWNrKVxuICAgICAgdGhpcy5fZXJyb3JDYWxsYmFjayhyZXNwb25zZS5zdGF0dXNUZXh0KVxuICB9XG5cbiAgcmV0dXJuIHJlc3BvbnNlO1xufVxuXG5mdW5jdGlvbiBjYXBpdGFsaXplKHRleHQpIHtcbiAgcmV0dXJuIHRleHQucmVwbGFjZSgvKD86XnxcXHMpXFxTL2csIGZ1bmN0aW9uKGEpIHsgcmV0dXJuIGEudG9VcHBlckNhc2UoKTsgfSk7XG59O1xuXG5leHBvcnRzLmluaXQgPSBmdW5jdGlvbihwYXJhbXMpe1xuICBfZ29vZ2xlU2VydmVyQXBpS2V5ID0gcGFyYW1zLmdvb2dsZVNlcnZlckFwaUtleTtcbiAgdGhpcy5fbGFuZ3VhZ2UgPSBwYXJhbXMubGFuZ3VhZ2UgfHwgJ2VzJztcbiAgdGhpcy5fcmFkaXVzID0gcGFyYW1zLnJhZGl1cyB8fCAnMTAwMDAwJztcbiAgdGhpcy5fbG9jYXRpb24gPSBwYXJhbXMubG9jYXRpb24gfHwgJzQyLjY2Mzg0MCwyMy4zMTQzOTInO1xuICB0aGlzLl9lcnJvckNhbGxiYWNrID0gcGFyYW1zLmVycm9yQ2FsbGJhY2s7XG59XG5cbmV4cG9ydHMuc2VhcmNoID0gZnVuY3Rpb24odGV4dCl7XG4gICAgdmFyIHNlYXJjaEJ5ID0gY2FwaXRhbGl6ZSh0ZXh0KS5yZXBsYWNlKG5ldyBSZWdFeHAoXCIgXCIsICdnJyksIFwiXCIpO1xuICAgIHZhciB1cmwgPSBfcGxhY2VzQXBpVXJsICsgXCI/aW5wdXQ9XCIgKyBzZWFyY2hCeSArIFwiJmxvY2F0aW9uPVwiKyB0aGlzLl9sb2NhdGlvbiArIFwiJmxhbmd1YWdlPVwiKyB0aGlzLl9sYW5ndWFnZSArXCImcmFkaXVzPVwiKyB0aGlzLl9yYWRpdXMgK1wiJmtleT1cIiArIF9nb29nbGVTZXJ2ZXJBcGlLZXk7XG4gICAgcmV0dXJuIGZldGNoKHVybClcbiAgICAudGhlbihoYW5kbGVFcnJvcnMpXG4gICAgLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgIHJldHVybiByZXNwb25zZS5qc29uKCk7XG4gICAgfSkudGhlbihmdW5jdGlvbihkYXRhKSB7XG4gICAgICB2YXIgaXRlbXMgPSBbXVxuICAgICAgZm9yKHZhciBpID0gMDsgaSA8IGRhdGEucHJlZGljdGlvbnMubGVuZ3RoOyBpKyspe1xuICAgICAgICBpdGVtcy5wdXNoKHtcbiAgICAgICAgICBkZXNjcmlwdGlvbjogZGF0YS5wcmVkaWN0aW9uc1tpXS5kZXNjcmlwdGlvbixcbiAgICAgICAgICBwbGFjZUlkOiBkYXRhLnByZWRpY3Rpb25zW2ldLnBsYWNlX2lkLFxuICAgICAgICAgIC8vJ2RhdGEnOiBkYXRhLnByZWRpY3Rpb25zW2ldXG4gICAgICAgIH0pXG4gICAgICB9XG4gICAgIFxuICAgICAgcmV0dXJuIEpTT04uc3RyaW5naWZ5KGl0ZW1zKTtcbiAgICB9KVxufVxuXG5leHBvcnRzLnF1ZXJ5QXV0b0NvbXBsZXRlID0gZnVuY3Rpb24odGV4dCwgdHlwZXMpe1xuICAgIHZhciBzZWFyY2hCeSA9IGNhcGl0YWxpemUodGV4dCkucmVwbGFjZShuZXcgUmVnRXhwKFwiIFwiLCAnZycpLCBcIlwiKTtcbiAgICB2YXIgdXJsID0gX3F1ZXJ5QXV0b0NvbXBsZXRlQXBpVXJsICsgXCI/aW5wdXQ9XCIgKyBzZWFyY2hCeSArIFwiJmxvY2F0aW9uPVwiKyB0aGlzLl9sb2NhdGlvbiArXCImdHlwZXM9XCIgKyB0eXBlcyArIFwiJmxhbmd1YWdlPVwiKyB0aGlzLl9sYW5ndWFnZSArXCImcmFkaXVzPVwiKyBfcmFkaXVzICtcIiZrZXk9XCIgKyBfZ29vZ2xlU2VydmVyQXBpS2V5XG4gICAgcmV0dXJuIGZldGNoKHVybClcbiAgICAudGhlbihoYW5kbGVFcnJvcnMpXG4gICAgLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgIHJldHVybiByZXNwb25zZS5qc29uKCk7XG4gICAgfSkudGhlbihmdW5jdGlvbihkYXRhKSB7XG4gICAgICB2YXIgaXRlbXMgPSBbXVxuICAgICAgZm9yKHZhciBpID0gMDsgaSA8IGRhdGEucHJlZGljdGlvbnMubGVuZ3RoOyBpKyspe1xuICAgICAgICBpdGVtcy5wdXNoKHtcbiAgICAgICAgICBkZXNjcmlwdGlvbjogZGF0YS5wcmVkaWN0aW9uc1tpXS5kZXNjcmlwdGlvbixcbiAgICAgICAgICBwbGFjZUlkOiBkYXRhLnByZWRpY3Rpb25zW2ldLnBsYWNlX2lkLFxuICAgICAgICAgICdkYXRhJzogZGF0YS5wcmVkaWN0aW9uc1tpXVxuICAgICAgICB9KVxuICAgICAgfVxuICAgICAgcmV0dXJuIGl0ZW1zXG4gICAgfSlcbn1cblxuZXhwb3J0cy5kZXRhaWxzID0gZnVuY3Rpb24ocGxhY2VpZCl7XG4gICAgdmFyIHVybCA9IF9wbGFjZXNEZXRhaWxzQXBpVXJsICsgXCI/cGxhY2VpZD1cIiArIHBsYWNlaWQgKyBcIiZsYW5ndWFnZT1cIisgdGhpcy5fbGFuZ3VhZ2UgK1wiJmtleT1cIiArIF9nb29nbGVTZXJ2ZXJBcGlLZXlcbiAgICBjb25zb2xlLmxvZyh1cmwpXG4gICAgcmV0dXJuIGZldGNoKHVybClcbiAgICAudGhlbihoYW5kbGVFcnJvcnMpXG4gICAgLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgIHJldHVybiByZXNwb25zZS5qc29uKCk7XG4gICAgfSkudGhlbihmdW5jdGlvbihkYXRhKSB7XG5cbiAgICAgIHZhciBwbGFjZSA9IG5ldyBPYmplY3QoKTtcbiAgICAgIHZhciBhZGRyZXNzX2NvbXBvbmVudHMgPSBkYXRhLnJlc3VsdC5hZGRyZXNzX2NvbXBvbmVudHNcbiAgICAgIC8vIGZvcih2YXIga2V5IGluIGFkZHJlc3NfY29tcG9uZW50cyl7XG5cbiAgICAgIC8vICAgdmFyIGFkZHJlc3NfY29tcG9uZW50ID0gYWRkcmVzc19jb21wb25lbnRzW2tleV1cblxuXG4gICAgICAvLyAgIGlmIChhZGRyZXNzX2NvbXBvbmVudC50eXBlc1swXSA9PSBcInJvdXRlXCIpe1xuICAgICAgLy8gICAgICAgcGxhY2Uucm91dGUgPSBhZGRyZXNzX2NvbXBvbmVudC5sb25nX25hbWU7XG4gICAgICAvLyAgIH1cblxuICAgICAgLy8gICBpZiAoYWRkcmVzc19jb21wb25lbnQudHlwZXNbMF0gPT0gXCJsb2NhbGl0eVwiKXtcbiAgICAgIC8vICAgICAgIHBsYWNlLmxvY2FsaXR5ID0gYWRkcmVzc19jb21wb25lbnQubG9uZ19uYW1lO1xuICAgICAgLy8gICB9XG5cbiAgICAgIC8vICAgaWYgKGFkZHJlc3NfY29tcG9uZW50LnR5cGVzWzBdID09IFwiY291bnRyeVwiKXtcbiAgICAgIC8vICAgICAgIHBsYWNlLmNvdW50cnkgPSBhZGRyZXNzX2NvbXBvbmVudC5sb25nX25hbWU7XG4gICAgICAvLyAgIH1cblxuICAgICAgLy8gICBpZiAoYWRkcmVzc19jb21wb25lbnQudHlwZXNbMF0gPT0gXCJwb3N0YWxfY29kZV9wcmVmaXhcIil7XG4gICAgICAvLyAgICAgICBwbGFjZS56aXBDb2RlID0gYWRkcmVzc19jb21wb25lbnQubG9uZ19uYW1lO1xuICAgICAgLy8gICB9XG5cbiAgICAgIC8vICAgaWYgKGFkZHJlc3NfY29tcG9uZW50LnR5cGVzWzBdID09IFwic3RyZWV0X251bWJlclwiKXtcbiAgICAgIC8vICAgICAgIHBsYWNlLm51bWJlciA9IGFkZHJlc3NfY29tcG9uZW50LmxvbmdfbmFtZTtcbiAgICAgIC8vICAgfVxuXG4gICAgICAvLyAgIGlmKGFkZHJlc3NfY29tcG9uZW50LnR5cGVzWzBdID09IFwic3VibG9jYWxpdHlfbGV2ZWxfMVwiKXtcbiAgICAgIC8vICAgICBwbGFjZS5zdWJsb2NhbGl0eSA9IGFkZHJlc3NfY29tcG9uZW50LmxvbmdfbmFtZTtcbiAgICAgIC8vICAgfVxuICAgICAgLy8gfVxuXG4gICAgICBwbGFjZVtcImxhdGl0dWRlXCJdID0gZGF0YS5yZXN1bHQuZ2VvbWV0cnkubG9jYXRpb24ubGF0O1xuICAgICAgcGxhY2VbXCJsb25naXR1ZGVcIl0gPSBkYXRhLnJlc3VsdC5nZW9tZXRyeS5sb2NhdGlvbi5sbmc7XG4gICAgICBwbGFjZVtcIm5hbWVcIl0gPSBkYXRhLnJlc3VsdC5uYW1lO1xuICAgICAgcGxhY2VbXCJwaG9uZVwiXSA9IGRhdGEucmVzdWx0LmludGVybmF0aW9uYWxfcGhvbmVfbnVtYmVyO1xuICAgICAgcGxhY2VbXCJmb3JtYXR0ZWRBZGRyZXNzXCJdID0gZGF0YS5yZXN1bHQuZm9ybWF0dGVkX2FkZHJlc3M7XG5cbiAgICAgIGlmKGRhdGEucmVzdWx0LnBob3RvcyAmJiBkYXRhLnJlc3VsdC5waG90b3MubGVuZ3RoID4gMCl7XG4gICAgICAgIHBsYWNlW1wicGhvdG9SZWZlcmVuY2VcIl0gPSBkYXRhLnJlc3VsdC5waG90b3NbMF0ucGhvdG9fcmVmZXJlbmNlXG4gICAgICB9XG5cbiAgICAgIHJldHVybiBwbGFjZVxuXG4gICAgfSlcbn1cblxuZXhwb3J0cy5sb2FkUGxhY2VQaG90byA9IGZ1bmN0aW9uKHBob3RvcmVmZXJlbmNlLCBvblN1Y2Nlc3NDYWxsYmFjaywgb25GYWlsQ2FsbGJhY2spe1xuICB2YXIgdXJsID0gX3BsYWNlc0ltYWdlc0FwaVVybCArIFwiP21heHdpZHRoPTEwMCZwaG90b3JlZmVyZW5jZT1cIiArIHBob3RvcmVmZXJlbmNlICsgXCIma2V5PVwiICsgX2dvb2dsZVNlcnZlckFwaUtleTtcbiAgcmV0dXJuIGltYWdlU291cmNlLmZyb21VcmwodXJsKVxufVxuIl19