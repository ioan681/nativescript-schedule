"use strict";
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var absolute_layout_1 = require("ui/layouts/absolute-layout");
var gestures_1 = require("ui/gestures");
var enums_1 = require("ui/enums");
var auth_service_1 = require('./login/auth.service');
require('./rxjs-operators');
var AppComponent = (function () {
    function AppComponent(auth, page) {
        this.auth = auth;
        this.page = page;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        var user;
        this.side = this.sideMenu.nativeElement;
        var absoluteLayout = new absolute_layout_1.AbsoluteLayout();
        this.side.translateX = -250;
        this.side.on(gestures_1.GestureTypes.swipe, function (args) {
            if (args.direction == 2) {
                _this.side.animate({
                    translate: { x: -250, y: 0 },
                    duration: 450,
                    curve: enums_1.AnimationCurve.easeInOut
                });
            }
            if (args.direction == 1) {
                _this.side.animate({
                    translate: { x: 0, y: 0 },
                    duration: 450,
                    curve: enums_1.AnimationCurve.easeInOut
                });
            }
        });
        // this.auth.getUser().subscribe(result => {
        //   user = result.json();
        //   var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        //   let token = currentUser && currentUser.token;
        //   localStorage.removeItem('currentUser');
        //   localStorage.setItem('currentUser', JSON.stringify(
        //     {name: user.name,email: user.email,telephone: user.telephone,
        //       profession: user.profession,about: user.about,img_name: user.img_name,
        //         token: token }));
        //   this.name = user.name;
        //   // document.getElementById("side-name").innerHTML=user.name;
        //   // document.getElementById("side-mail").innerHTML= "Mail: "+user.email;
        //   this.image = user.img_name;
        // });
    };
    AppComponent.prototype.signOut = function () {
        this.side.animate({
            translate: { x: -250, y: 0 },
            duration: 450,
            curve: enums_1.AnimationCurve.easeInOut
        });
    };
    __decorate([
        core_1.ViewChild("side"), 
        __metadata('design:type', core_1.ElementRef)
    ], AppComponent.prototype, "sideMenu", void 0);
    AppComponent = __decorate([
        core_1.Component({
            selector: "my-app",
            templateUrl: "view/app.component.html",
            styleUrls: ['css/nav.component.css']
        }), 
        __metadata('design:paramtypes', [auth_service_1.AuthService, page_1.Page])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL2Rldi9hcHAuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxxQkFBMkUsZUFBZSxDQUFDLENBQUE7QUFDM0YscUJBQWlDLFNBQVMsQ0FBQyxDQUFBO0FBQzNDLGdDQUErQiw0QkFBNEIsQ0FBQyxDQUFBO0FBRTVELHlCQUFvRCxhQUFhLENBQUMsQ0FBQTtBQUVsRSxzQkFBK0IsVUFBVSxDQUFDLENBQUE7QUFHMUMsNkJBQTJCLHNCQUFzQixDQUFDLENBQUE7QUFDbEQsUUFBTyxrQkFBa0IsQ0FBQyxDQUFBO0FBTTFCO0lBS0ksc0JBQW9CLElBQWlCLEVBQVUsSUFBVTtRQUFyQyxTQUFJLEdBQUosSUFBSSxDQUFhO1FBQVUsU0FBSSxHQUFKLElBQUksQ0FBTTtJQUFHLENBQUM7SUFDN0QsK0JBQVEsR0FBUjtRQUFBLGlCQW1DQztRQWxDRyxJQUFJLElBQVMsQ0FBQztRQUNkLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUM7UUFDeEMsSUFBSSxjQUFjLEdBQUcsSUFBSSxnQ0FBYyxFQUFFLENBQUM7UUFDMUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxHQUFHLENBQUM7UUFDNUIsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsdUJBQVksQ0FBQyxLQUFLLEVBQUMsVUFBQyxJQUEyQjtZQUMxRCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7Z0JBQ3BCLEtBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO29CQUNoQixTQUFTLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBQztvQkFDM0IsUUFBUSxFQUFFLEdBQUc7b0JBQ2IsS0FBSyxFQUFFLHNCQUFjLENBQUMsU0FBUztpQkFDaEMsQ0FBQyxDQUFDO1lBQ0wsQ0FBQztZQUNELEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUUsQ0FBQyxDQUFDLENBQUEsQ0FBQztnQkFDcEIsS0FBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7b0JBQ2hCLFNBQVMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBQztvQkFDeEIsUUFBUSxFQUFFLEdBQUc7b0JBQ2IsS0FBSyxFQUFFLHNCQUFjLENBQUMsU0FBUztpQkFDaEMsQ0FBQyxDQUFDO1lBQ0wsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsNENBQTRDO1FBQzVDLDBCQUEwQjtRQUMxQix1RUFBdUU7UUFDdkUsa0RBQWtEO1FBQ2xELDRDQUE0QztRQUM1Qyx3REFBd0Q7UUFDeEQsb0VBQW9FO1FBQ3BFLCtFQUErRTtRQUMvRSw0QkFBNEI7UUFDNUIsMkJBQTJCO1FBQzNCLGlFQUFpRTtRQUNqRSw0RUFBNEU7UUFDNUUsZ0NBQWdDO1FBQ2xDLE1BQU07SUFDUixDQUFDO0lBQ0QsOEJBQU8sR0FBUDtRQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ1YsU0FBUyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUM7WUFDM0IsUUFBUSxFQUFFLEdBQUc7WUFDYixLQUFLLEVBQUUsc0JBQWMsQ0FBQyxTQUFTO1NBQ2hDLENBQUMsQ0FBQztJQUNYLENBQUM7SUEvQ0Q7UUFBQyxnQkFBUyxDQUFDLE1BQU0sQ0FBQzs7a0RBQUE7SUFOdEI7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLFFBQVE7WUFDbEIsV0FBVyxFQUFFLHlCQUF5QjtZQUN0QyxTQUFTLEVBQUUsQ0FBQyx1QkFBdUIsQ0FBQztTQUN2QyxDQUFDOztvQkFBQTtJQWtERixtQkFBQztBQUFELENBQUMsQUFqREQsSUFpREM7QUFqRFksb0JBQVksZUFpRHhCLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsdHJhbnNpdGlvbiwgT25Jbml0LCBWaWV3Q2hpbGQsIEVsZW1lbnRSZWYgfSAgICAgICAgZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUGFnZSB9ICAgICAgICAgICAgIGZyb20gXCJ1aS9wYWdlXCI7XHJcbmltcG9ydCB7IEFic29sdXRlTGF5b3V0IH0gZnJvbSBcInVpL2xheW91dHMvYWJzb2x1dGUtbGF5b3V0XCI7XHJcbmltcG9ydCB7IFN0YWNrTGF5b3V0IH0gZnJvbSBcInVpL2xheW91dHMvc3RhY2stbGF5b3V0XCI7XHJcbmltcG9ydCB7IEdlc3R1cmVUeXBlcywgU3dpcGVHZXN0dXJlRXZlbnREYXRhIH0gZnJvbSBcInVpL2dlc3R1cmVzXCI7XHJcbmltcG9ydCB7IFRyYW5zaXRpb24gfSBmcm9tIFwidWkvdHJhbnNpdGlvblwiO1xyXG5pbXBvcnQgeyBBbmltYXRpb25DdXJ2ZSB9IGZyb20gXCJ1aS9lbnVtc1wiO1xyXG5pbXBvcnQgeyBBbmltYXRpb24gfSBmcm9tIFwidWkvYW5pbWF0aW9uXCI7XHJcbmltcG9ydCBJbWFnZU1vZHVsZSA9IHJlcXVpcmUoXCJ1aS9pbWFnZVwiKTtcclxuaW1wb3J0IHsgQXV0aFNlcnZpY2V9IGZyb20gJy4vbG9naW4vYXV0aC5zZXJ2aWNlJztcclxuaW1wb3J0ICcuL3J4anMtb3BlcmF0b3JzJztcclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogXCJteS1hcHBcIixcclxuICAgIHRlbXBsYXRlVXJsOiBcInZpZXcvYXBwLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgICBzdHlsZVVybHM6IFsnY3NzL25hdi5jb21wb25lbnQuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEFwcENvbXBvbmVudCB7XHJcbiAgICBAVmlld0NoaWxkKFwic2lkZVwiKSBzaWRlTWVudTogRWxlbWVudFJlZjtcclxuICAgIHB1YmxpYyBzaWRlOmFueTtcclxuICAgIHB1YmxpYyBpbWFnZTpzdHJpbmc7XHJcbiAgICBwdWJsaWMgbmFtZTogc3RyaW5nO1xyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBhdXRoOiBBdXRoU2VydmljZSwgcHJpdmF0ZSBwYWdlOiBQYWdlKSB7fVxyXG4gICAgbmdPbkluaXQoKXsgXHJcbiAgICAgICAgbGV0IHVzZXI6IGFueTtcclxuICAgICAgICB0aGlzLnNpZGUgPSB0aGlzLnNpZGVNZW51Lm5hdGl2ZUVsZW1lbnQ7XHJcbiAgICAgICAgdmFyIGFic29sdXRlTGF5b3V0ID0gbmV3IEFic29sdXRlTGF5b3V0KCk7XHJcbiAgICAgICAgdGhpcy5zaWRlLnRyYW5zbGF0ZVggPSAtMjUwO1xyXG4gICAgICAgIHRoaXMuc2lkZS5vbihHZXN0dXJlVHlwZXMuc3dpcGUsKGFyZ3M6IFN3aXBlR2VzdHVyZUV2ZW50RGF0YSk9PntcclxuICAgICAgICAgIGlmKGFyZ3MuZGlyZWN0aW9uPT0yKXtcclxuICAgICAgICAgICAgdGhpcy5zaWRlLmFuaW1hdGUoe1xyXG4gICAgICAgICAgICAgIHRyYW5zbGF0ZTogeyB4OiAtMjUwLCB5OiAwfSwgICAgXHJcbiAgICAgICAgICAgICAgZHVyYXRpb246IDQ1MCxcclxuICAgICAgICAgICAgICBjdXJ2ZTogQW5pbWF0aW9uQ3VydmUuZWFzZUluT3V0XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaWYoYXJncy5kaXJlY3Rpb249PTEpe1xyXG4gICAgICAgICAgICB0aGlzLnNpZGUuYW5pbWF0ZSh7XHJcbiAgICAgICAgICAgICAgdHJhbnNsYXRlOiB7IHg6IDAsIHk6IDB9LCAgICBcclxuICAgICAgICAgICAgICBkdXJhdGlvbjogNDUwLFxyXG4gICAgICAgICAgICAgIGN1cnZlOiBBbmltYXRpb25DdXJ2ZS5lYXNlSW5PdXRcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgLy8gdGhpcy5hdXRoLmdldFVzZXIoKS5zdWJzY3JpYmUocmVzdWx0ID0+IHtcclxuICAgICAgICAvLyAgIHVzZXIgPSByZXN1bHQuanNvbigpO1xyXG4gICAgICAgIC8vICAgdmFyIGN1cnJlbnRVc2VyID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnY3VycmVudFVzZXInKSk7XHJcbiAgICAgICAgLy8gICBsZXQgdG9rZW4gPSBjdXJyZW50VXNlciAmJiBjdXJyZW50VXNlci50b2tlbjtcclxuICAgICAgICAvLyAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCdjdXJyZW50VXNlcicpO1xyXG4gICAgICAgIC8vICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2N1cnJlbnRVc2VyJywgSlNPTi5zdHJpbmdpZnkoXHJcbiAgICAgICAgLy8gICAgIHtuYW1lOiB1c2VyLm5hbWUsZW1haWw6IHVzZXIuZW1haWwsdGVsZXBob25lOiB1c2VyLnRlbGVwaG9uZSxcclxuICAgICAgICAvLyAgICAgICBwcm9mZXNzaW9uOiB1c2VyLnByb2Zlc3Npb24sYWJvdXQ6IHVzZXIuYWJvdXQsaW1nX25hbWU6IHVzZXIuaW1nX25hbWUsXHJcbiAgICAgICAgLy8gICAgICAgICB0b2tlbjogdG9rZW4gfSkpO1xyXG4gICAgICAgIC8vICAgdGhpcy5uYW1lID0gdXNlci5uYW1lO1xyXG4gICAgICAgIC8vICAgLy8gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJzaWRlLW5hbWVcIikuaW5uZXJIVE1MPXVzZXIubmFtZTtcclxuICAgICAgICAvLyAgIC8vIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwic2lkZS1tYWlsXCIpLmlubmVySFRNTD0gXCJNYWlsOiBcIit1c2VyLmVtYWlsO1xyXG4gICAgICAgIC8vICAgdGhpcy5pbWFnZSA9IHVzZXIuaW1nX25hbWU7XHJcblx0XHQgICAgLy8gfSk7XHJcbiAgICB9XHJcbiAgICBzaWduT3V0KCl7XHJcbiAgICAgIHRoaXMuc2lkZS5hbmltYXRlKHtcclxuICAgICAgICAgICAgICB0cmFuc2xhdGU6IHsgeDogLTI1MCwgeTogMH0sICAgIFxyXG4gICAgICAgICAgICAgIGR1cmF0aW9uOiA0NTAsXHJcbiAgICAgICAgICAgICAgY3VydmU6IEFuaW1hdGlvbkN1cnZlLmVhc2VJbk91dFxyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4gIl19