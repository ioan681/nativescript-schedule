import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
// import { AuthGuard } from './login/auth.guard';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from "./login/signup.component";

const appRoutes = [
	{ path: 'login', component: LoginComponent },
	{ path: 'signup', component: SignUpComponent },
  //{ path: '', component: MapComponent },
  // { path: '', component: LoginComponent },
  //{ path: '**', redirectTo: '/login' }
  
];

@NgModule({
  imports: [
	NativeScriptRouterModule.forRoot(appRoutes)
  ],
  exports: [
    NativeScriptRouterModule
  ],
  providers: [
  ]
})
export class AppRoutingModule {}