import {Component, ElementRef, ViewChild} from '@angular/core';
import {registerElement} from "nativescript-angular/element-registry";
import { StackLayout } from "ui/layouts/stack-layout";
import { Label } from "ui/label";
var mapsModule = require("nativescript-google-maps-sdk");
var GPlaces = require("./google-places");
import searchBarModule = require("ui/search-bar");
import { GestureTypes, GestureEventData } from "ui/gestures";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
// Important - must register MapView plugin in order to use in Angular templates
registerElement("MapView", () => require("nativescript-google-maps-sdk").MapView);
 import './rxjs-operators';
@Component({
    selector: 'map-component',
    template: `
    <StackLayout id="map-modal">
        <AbsoluteLayout>
            <GridLayout rows="50,*,40" style="width:100%;height:100%;">
                <SearchBar row="0" style="height:50px;" #search (tap)="keyup()">Da</SearchBar>
                <MapView row="1" (mapReady)="onMapReady($event)" zoom="12" style="height:80%;width:100%;"
                latitude="42.663840" longitude="23.314392"></MapView>
                <button row="2" text="submit" (tap)="submit()"></button>
            </GridLayout>
            <StackLayout #container top="50" width="100%">
            
            </StackLayout>
        </AbsoluteLayout>
    </StackLayout>
    `
})
export class MapComponent {
    public search:any;
    public container:any;
    public stack:StackLayout;
    public mapView:any;
    public marker = {
        id:undefined,
        longitude:undefined,
        latitude:undefined
    }
    @ViewChild("MapView") map: ElementRef;
    @ViewChild("search") searchEl: ElementRef;
    @ViewChild("container") cont: ElementRef;
    constructor(private params: ModalDialogParams){

    }
    ngOnInit(){
        this.search = this.searchEl.nativeElement;
        this.container = this.cont.nativeElement;
        //this.mapView = this.map.nativeElement;
        GPlaces.init({
            googleServerApiKey: 'AIzaSyBDV_BgWWAwcsgAl5YnYVTujE_oELsPtmM',
            language: 'es',
            radius: '500',
            location: '42.663840,23.314392',
            errorCallback: function(text){console.log(text)}
        });
        var searchBar = new searchBarModule.SearchBar();
        this.search.on(searchBarModule.SearchBar.submitEvent,(args: any)=>{ 
            this.keyup((<searchBarModule.SearchBar>args.object).text)
        });
        this.search.on(searchBarModule.SearchBar.clearEvent,(args: any)=>{
            if(this.container.getChildAt(0)!=undefined)
            this.container.removeChild(this.container.getChildAt(0));
        });
    }
    keyup(text:string){
        if(this.container.getChildAt(0)!=undefined)
            this.container.removeChild(this.container.getChildAt(0));
        GPlaces.search(text).then((result)=>{
            result = JSON.parse(result);
            var stack = new StackLayout();
            for(let item of result){
                let label = new Label();
                label.className = "google-place";
                label.text = item.description;
                label.textWrap = true;
                stack.addChild(label);
                label.on(GestureTypes.tap,(args: GestureEventData)=> {
                    if(this.container.getChildAt(0)!=undefined)
                        this.container.removeChild(this.container.getChildAt(0));
                    this.getLocation(item.placeId);
                });
            }
            this.container.addChild(stack);
        })
    }
    getLocation(id){
        GPlaces.details(id).then((place)=>{
            // for(let item in place){
            //     console.log(item+"=>"+place[item])
            // }
            var marker = new mapsModule.Marker();
            marker.position = mapsModule.Position.positionFromLatLng(place["latitude"],place["longitude"]);
            marker.title = place["name"];
            marker.userData = { index : 1};
            this.mapView.addMarker(marker); 
            this.marker.id = id;
            this.marker.latitude = place["latitude"];
            this.marker.longitude = place["longitude"];
        })
    }
    //Map event 
    onMapReady = (args) => {
        // console.log("Map Ready");
        this.mapView = args.object;
        // console.log("Setting a marker...");
        // var marker = new mapsModule.Marker();
        // marker.position = mapsModule.Position.positionFromLatLng(42.663840,24.314392);
        // marker.title = "Sydney";
        // marker.snippet = "Australia";
        // marker.userData = { index : 1};
        // mapView.addMarker(marker); 
    }
    submit(){
        this.params.closeCallback(this.marker);
    }
}