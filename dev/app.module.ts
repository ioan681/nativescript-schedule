import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptModule } from "nativescript-angular/platform";
import { NativeScriptHttpModule } from "nativescript-angular/http";

import { ModalDialogService } from "nativescript-angular/modal-dialog";

import { AuthService} 				from './login/auth.service';
import { AuthGuard } from './login/auth.guard';

import { AppComponent } from "./app.component";
import { MapComponent } from "./map.component";
import { LoginComponent } from "./login/login.component";
import { SignUpComponent } from "./login/signup.component";
import { CalendarModule } from './Calendar/calendar.module';
import { AppRoutingModule }        from './app-routing.module';
require( "nativescript-localstorage" );
@NgModule({
    imports: [
        NativeScriptModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        AppRoutingModule,
        CalendarModule,
        ],
    declarations: [
        LoginComponent,
        SignUpComponent,
        AppComponent,
        MapComponent
        ],
    entryComponents: [
        MapComponent,
    ],
    bootstrap: [AppComponent],
    providers: [AuthService,AuthGuard, ModalDialogService],
    schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
