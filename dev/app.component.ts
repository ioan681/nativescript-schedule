import { Component,transition, OnInit, ViewChild, ElementRef }        from "@angular/core";
import { Page }             from "ui/page";
import { AbsoluteLayout } from "ui/layouts/absolute-layout";
import { StackLayout } from "ui/layouts/stack-layout";
import { GestureTypes, SwipeGestureEventData } from "ui/gestures";
import { Transition } from "ui/transition";
import { AnimationCurve } from "ui/enums";
import { Animation } from "ui/animation";
import ImageModule = require("ui/image");
import { AuthService} from './login/auth.service';
import './rxjs-operators';
@Component({
    selector: "my-app",
    templateUrl: "view/app.component.html",
    styleUrls: ['css/nav.component.css']
})
export class AppComponent {
    @ViewChild("side") sideMenu: ElementRef;
    public side:any;
    public image:string;
    public name: string;
    constructor(private auth: AuthService, private page: Page) {}
    ngOnInit(){ 
        let user: any;
        this.side = this.sideMenu.nativeElement;
        var absoluteLayout = new AbsoluteLayout();
        this.side.translateX = -250;
        this.side.on(GestureTypes.swipe,(args: SwipeGestureEventData)=>{
          if(args.direction==2){
            this.side.animate({
              translate: { x: -250, y: 0},    
              duration: 450,
              curve: AnimationCurve.easeInOut
            });
          }
          if(args.direction==1){
            this.side.animate({
              translate: { x: 0, y: 0},    
              duration: 450,
              curve: AnimationCurve.easeInOut
            });
          }
        });
        // this.auth.getUser().subscribe(result => {
        //   user = result.json();
        //   var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        //   let token = currentUser && currentUser.token;
        //   localStorage.removeItem('currentUser');
        //   localStorage.setItem('currentUser', JSON.stringify(
        //     {name: user.name,email: user.email,telephone: user.telephone,
        //       profession: user.profession,about: user.about,img_name: user.img_name,
        //         token: token }));
        //   this.name = user.name;
        //   // document.getElementById("side-name").innerHTML=user.name;
        //   // document.getElementById("side-mail").innerHTML= "Mail: "+user.email;
        //   this.image = user.img_name;
		    // });
    }
    signOut(){
      this.side.animate({
              translate: { x: -250, y: 0},    
              duration: 450,
              curve: AnimationCurve.easeInOut
            });
    }
}
 