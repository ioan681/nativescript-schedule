import { Component, ElementRef, ViewChild, } from "@angular/core";
import { setInterval, setTimeout, clearInterval } from "timer";
import { Color } from "color";
import { Page } from "ui/page";
import { TimePicker } from "ui/time-picker";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
@Component({
    moduleId: module.id,
    template: `
    <StackLayout sdkToggleNavButton id="date">
        <TimePicker id="from" [(ngModel)]='time'></TimePicker>
        <GridLayout columns="130 130" rows="auto">
            <button text="tap" col="0" (tap)="submit()"></button>  
            <button text="tap" col="1" (tap)="submit()"></button>  
        </GridLayout>
    </StackLayout>`,
})
export class DatePickerComponent {
    public time:Date;
    constructor(private params: ModalDialogParams, private page: Page) {
        setTimeout(()=>{
            let val = params.context.split(":");
            let from = <TimePicker> this.page.getViewById("from");
            from.hour = parseInt(val[0]);
            from.minute = parseInt(val[1]);
        });
    }
    public submit() {
        this.params.closeCallback(this.time);
    }
}