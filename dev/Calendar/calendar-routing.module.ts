import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { AuthGuard } from '../login/auth.guard';
import { YearComponent }   from './year.component';
import { MonthComponent }   from './month.component';
import { WeekComponent }   from './week.component';
//import { NavigationComponent } from '../navigation.component';
const CalendarRoutes = [
  
  { path: '',
    //component: NavigationComponent
    canActivate: [AuthGuard],
    children: [
      { path: 'year', component: YearComponent },
      { path: '', component: WeekComponent },
      { path: 'month/:id', component: MonthComponent },
      { path: 'week/:id', component: WeekComponent },
      // { path: '', component: YearComponent },
    ] 
  },
];

@NgModule({
  imports: [
    NativeScriptRouterModule.forChild(CalendarRoutes)
  ],
  exports: [
    NativeScriptRouterModule
  ]
})
export class CalendarRoutingModule { }