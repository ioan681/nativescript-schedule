import { Component, ElementRef, OnInit, ViewChild, ViewContainerRef} from "@angular/core";
import { setInterval, setTimeout, clearInterval } from "timer";
import { Color } from "color";
import { Page }             from "ui/page";
import { Label } from "ui/label";
import { ModalDialogService, ModalDialogOptions, ModalDialogParams } from "nativescript-angular/modal-dialog";
import { MapComponent } from "../map.component";
import { DatePickerComponent } from "./datePicker.component";
@Component({
    moduleId: module.id,
    templateUrl: './../view/calendar/modal.component.html',
})
export class ModalViewComponent {
    public from: Label;
    public to: Label;
    public name: string;
    public label = new Label;
    public colour:any = "#0275d8";
    public id  = ""
    public longitude = "";
    public latitude = "";
    @ViewChild("start") title: ElementRef;
    constructor(private _modalService: ModalDialogService, 
    private params: ModalDialogParams, private page: Page){
        setTimeout(()=>{
            this.from = <Label> this.page.getViewById("from");
            this.to = <Label> this.page.getViewById("to");
            this.from.text = this.params.context.start;
            let val = this.params.context.start.split(":");
            let val2 = val[1];
            val = (parseInt(val[0])+1);
            if(val<10) val = "0"+val;
            val = val +":"+val2;
            this.to.text = val;
            this.label = this.title.nativeElement;
        })
    }
    time(){
        let options: ModalDialogOptions = {
            viewContainerRef: this.params.context.vcRef,
            context: this.from.text,
            fullscreen: false
        };
        this._modalService.showModal(DatePickerComponent, options)
        .then((time: any) => {
            if(time != undefined)
                if(time.getHours()<10)
                    this.from.text = "0"+time.getHours()+":"+time.getMinutes();
                else this.from.text = time.getHours()+":"+time.getMinutes();
    	});
    }
    time2(){
        let options: ModalDialogOptions = {
            viewContainerRef: this.params.context.vcRef,
            context: this.to.text,
            fullscreen: false
        };
        this._modalService.showModal(DatePickerComponent, options)
        .then((time: any) => { 
            if(time != undefined){
                if(time.getHours()<10)
                    this.to.text = "0"+time.getHours()+":"+time.getMinutes();
                else this.to.text = time.getHours()+":"+time.getMinutes();
            }
    	});
    }
    map(){
        let options: ModalDialogOptions = {
            viewContainerRef: this.params.context.vcRef,
            //context: this.to.text,
            fullscreen: true
        };
        this._modalService.showModal(MapComponent, options)
        .then((marker: any) => { 
            if(marker.id != undefined){
                this.id = marker["id"] ||"";
                this.latitude = marker["latitude"]||"";
                this.longitude = marker["longitude"]||"";
            }
    	});
    }
    public submit() {
        let obj = {
            from: this.from.text,
            to: this.from.text,
            name: this.name,
            color: this.colour,
            id: this.id,
            latitude: this.latitude,
            longitude: this.longitude,
        }
        this.params.closeCallback(obj);
    }
    Color(element: any){
        this.label.className = "color";
        this.label = element.object;
        this.colour = this.label.style.backgroundColor;
        this.label.className = "selected";
	}
}