import { Component } from '@angular/core';
import { Router } from "@angular/router";
@Component({
  selector: 'year-calendar',
  templateUrl: 'view/calendar/year.component.html',
  styleUrls: ['css/year.component.css'] 
})
export class YearComponent { 
  constructor(private router: Router) {}
  go(id:number){
    let link = ['/user', id];
    this.router.navigate(link);
  }
}
