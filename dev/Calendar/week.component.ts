import {Component,Input,transition,
    ElementRef, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { GestureTypes, SwipeGestureEventData, GestureEventData } from "ui/gestures";
import { GridLayout, GridUnitType, ItemSpec} from "ui/layouts/grid-layout";
import { StackLayout } from "ui/layouts/stack-layout";
import { CalendarService } from './calendar.service';
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ModalViewComponent } from "./modal.component";
import { Page }             from "ui/page";
import { Label } from "ui/label";
import { AnimationCurve } from "ui/enums";
import { Animation } from "ui/animation";
@Component({
  selector: 'year-calendar',
	template: `
	<ActionBar title="">
		<Label text="tralala" #title></Label>
	</ActionBar>
	<AbsoluteLayout>
		<ScrollView orientation="vertical" width="100%" height="99%">
			<AbsoluteLayout style="width:100%;height:1200;" #container></AbsoluteLayout>
		</ScrollView>
	</AbsoluteLayout>
    `,
  //templateUrl: 'view/calendar/week.component.html',
  //styleUrls: ['css/week.component.css','css/month.component.css'],
})
export class WeekComponent implements OnInit {
  public target = new Label;
  public save = new Label;
	public week:number[];
  public text:any;
	public year:number;
	public year2:number;
	public lastday:number;
	public month:number;
	public month2 = "January";  
	public state:number = 0;
  public hour: GridLayout;
	public cont: any;
	public cont2: any;
	@ViewChild("title") title: ElementRef;
  @ViewChild("container") container: ElementRef;
	constructor(private route: ActivatedRoute,private appoint: CalendarService, 
	private _modalService: ModalDialogService,private vcRef: ViewContainerRef, private page: Page ) {}
	ngOnInit(){
		//console.log(document.getElementById("side-container"))
		this.cont = this.container.nativeElement;
    this.text = this.title.nativeElement;
    this.hour = new GridLayout();
		for(let i=0;i<7;i+=1){
      let secondColumn = new ItemSpec(1, GridUnitType.star);
      this.hour.addColumn(secondColumn);
			let td2 = this.hours();
			GridLayout.setColumn(td2, i);
			this.hour.addChild(td2);
    } 
		
		this.Create(0);
	}
	Days(){
		let mon:number;
		let date = new Date();
		this.month = date.getMonth()+1;
		this.year = date.getFullYear();
		let day = date.getDay();
		let today = date.getDate();	
		if(today-day<0) {
			if(this.month == 1){
				this.month = 12;
				this.year -= 1;
			}
			else{
				this.month -=1;
			}
			let obj = {mon: 0}
			this.GetMonth(obj);
			this.lastday = obj.mon + today - day;
		}
		else this.lastday = today-day;
		return this.Week();
	}
	Week(){
		var obj = {mon: 0}
		this.GetMonth(obj);
		let j = this.lastday;
		let days = new Array;
		if(this.lastday-7 < 0){
			if(this.month == 1){this.month = 12; this.year -=1;}
			else this.month -=1;
		}
		for (var i = 0; i < 7;i+=1) {
			if(j == obj.mon) j = 0;
			j +=1;
			days.push(j)
		}
		this.lastday = j;
		if(days[0] < days[6] && days[0]-7<=0){
			if(this.month == 12){this.month = 1; this.year +=1;}
			else this.month +=1;
		}
		this.Month();
		if(days[0] > days[6] && days[6]-7<=0){
			if(this.month == 12){this.month = 1; this.year +=1;}
			else this.month +=1;
		}
		return days;
	}
	Week1(){
		var obj = {mon: 0}
		if(this.lastday - 14 >= 0){
			this.lastday -=14;
			this.GetMonth(obj);
		}
		else {
			if(this.month == 1){
				this.month = 12;
				this.year -= 1;
				this.GetMonth(obj);
			}
			else{
				this.month -=1;
				this.GetMonth(obj);
			}
			this.lastday = (this.lastday - 14 + obj.mon);
		}
		let days = new Array;
		let j = this.lastday;
		for (var i = 0; i < 7;i+=1) {
			if(j == obj.mon) j = 0;
			j +=1;
			days.push(j)
		}
		this.lastday = j;
		if(this.lastday - 7 < 0){
			if(this.month == 12){
				this.month = 1;
				this.year +=1;
			}
			else this.month +=1;
		}
		this.Month();
		return days;
	}
	GetMonth(obj: any){
		if(this.month <= 7){
			if(this.month % 2 == 1) obj.mon = 31;
			else obj.mon = 30;
			if(this.month == 2){
				if(((this.year % 4 == 0) && (this.year % 100 != 0)) 
					|| (this.year % 400 == 0))obj.mon = 29;
				else obj.mon = 28;
			}
		}
		else{
			if(this.month % 2 == 1) obj.mon = 30;
			else obj.mon = 31;
		}
	}
	Create(n: any){
		this.cont2 = new GridLayout();
 		this.cont2.style.width = "100%";
		this.cont2.style.borderRightColor = "#e7e7e7";
		this.cont2.style.borderWidth = "1px";
    this.cont2.style.backgroundColor = "white"
		var week:any;
		switch (n) {
			case -1:week = this.Week1();break;
			case 0:week = this.Days();break;
			case 1:week = this.Week();break;
		}
		let grid = this.hour;
    grid.id = "contt"
    grid.style.marginTop = 40;
		let i=0;
    let days = new GridLayout();
		week.forEach((element:any) => {
      let secondColumn = new ItemSpec(1, GridUnitType.star);
      days.addColumn(secondColumn);
      let td = new Label();
			td.text = element;
      td.className = "day-week";
      GridLayout.setColumn(td, i);
      days.addChild(td);
			i+=1;
	  });
		days.id="days";
    this.cont2.addChild(days);
	  this.cont2.addChild(grid);
    if(this.state == 1) this.cont2.translateX = -1000;
    if(this.state == 2) this.cont2.translateX = 1000;
    this.cont.addChild(this.cont2);
    this.showAttach(week[0]);
    this.cont2.animate({
      translate: { x: 0, y: 0},    
      duration: 800,
      curve: AnimationCurve.easeOut
    });
    this.hour = new GridLayout();
		for(let i=0;i<7;i+=1){
      let secondColumn = new ItemSpec(1, GridUnitType.star);
      this.hour.addColumn(secondColumn);
			let td2 = this.hours();
			GridLayout.setColumn(td2, i);
			this.hour.addChild(td2);
    }
    this.cont.on(GestureTypes.swipe,(args: SwipeGestureEventData)=>{
			console.log("Swipe Direction: " + args.direction);
				if(args.direction==2){
					this.Next();
				}
				if(args.direction==1){
					this.Prev();
				}
			});
   
	}
  showAttach(day: number){
		let date = this.year+"-"+this.month+"-"+day;
		let arr = new Array;
		this.appoint.getAppointment(date).subscribe(
			result => {
				if(result.length>0){
          let grandParent = this.cont2.getChildById('contt');
          // for(let da in grandParent){
          //    console.log(da)
          // }
					arr = result;
					let d:number,parent:any,children:any,start:any,end:any,i:any,label:any,time:string;
          for(let key in arr){
            d = arr[key]["date"].split("-");
						d = parseInt(d[2])-day;
            parent = grandParent.getChildAt(d)
            start = arr[key]["start"].split(":");
						start = parseInt(start[0]);
            parent.removeChild(parent.getChildById(start));
            label = new Label();
            label.text = arr[key]["name"]+"\r\n"+arr[key]["start"].substring(0, 5);
            label.textWrap = true;
            label.backgroundColor	= arr[key]["color"];
            label.className = "hour-btn attachment-btn";
            GridLayout.setRow(label, start);
			      parent.addChild(label);
          }
				}
        else console.log("not")
			},
      error => {
        console.log(error)
      }); 
	}
  hours(){
		let td = new GridLayout();
		let label:any;
		for(let i=0;i<24;i++){
			var secondRow = new ItemSpec(1, GridUnitType.auto);
			td.addRow(secondRow);
			label = new Label();
			label.className = "hour-btn";
			if(i<10) label.text = "0"+i+":30";
			else label.text = i+":30";
			label.id = i;
      label.on(GestureTypes.tap,  (args: GestureEventData)=> {
        this.clicked(args.object);
      });
      label.on(GestureTypes.swipe,(args: SwipeGestureEventData)=>{
			console.log("Swipe Direction: " + args.direction);
				if(args.direction==2){
					this.Next();
				}
				if(args.direction==1){
					this.Prev();
				}
			});
			//label.style.paddingTop = 20; 
			GridLayout.setRow(label, i);
			td.addChild(label);
		}
		return td;
  }
	Prev(){
    this.state = 1;
		this.Create(-1);
	}
	Next(){
    this.state = 2;
		this.Create(1);
	}
	Month(){
		switch (this.month) {
			case 1: this.month2 = "January";break;
			case 2: this.month2 = "February";break;
			case 3: this.month2 = "March";break;
			case 4: this.month2 = "April";break;
			case 5: this.month2 = "May";break;
			case 6: this.month2 = "June";break;
			case 7: this.month2 = "July";break;
			case 8: this.month2 = "August";break;
			case 9: this.month2 = "September";break;
			case 10:this.month2 = "October";break;
			case 11:this.month2 = "November";break;
			case 12:this.month2 = "December";break;
		}
		this.year2 = this.year;
    setTimeout(()=>{
					this.text.text = this.month2
				},300)
	}
  clicked(event: any){
    if(event == this.target){
			let obj = {
				start: this.save.text,
				vcRef: this.vcRef
			}
			let options: ModalDialogOptions = {
							viewContainerRef: this.vcRef,
							context: obj,
							fullscreen: true
					};
			this._modalService.showModal(ModalViewComponent, options)
			.then((appointment: any) => {
					if(appointment != undefined){
						this.Appointment(appointment);
					} 
    	});
		
		}
		else{
			this.target.text = this.save.text;
			this.target.className = this.save.className
			this.save.text = event.text;
			this.save.className = event.className;
			this.target = event;
			event.className = "hour-btn new-attach";
			event.text="+"
		}
  }
  Appointment(obj:any){
		let grandParent = this.cont2.getChildById('contt')
		let parent = <GridLayout>this.target.parent;
		let days = <GridLayout>this.page.getViewById("days")
		let label = <Label>days.getChildAt(grandParent.getChildIndex(parent));
		let color = obj.color["_hex"];
		let date = this.year+"-"+this.month+"-"+label.text;
		let name = obj.name;
		if(obj.name !=""){
			for(let da in obj){ 
				console.log(da+"=>"+obj[da])
			}
			let from = obj.from;
			let to = obj.to;
			this.appoint.postAppointment(obj.name,obj.from,obj.to,date,color,obj.latitude,obj.longitude,obj.id).subscribe(
				result => {
					if (result === true) {
					let i=1;
					let val = parseInt(to.value);
					let remove:any;
					parent.removeChild(this.target);
					// while(i){
					// 	remove = this.target.nextSibling;
					// 	if(val > parseInt(remove.value)){
					// 	i+=1; 
					// 	this.target.parentElement.removeChild(remove);
					// 	}
					// 	else break;
					// };
					label = new Label();
					label.text = obj["name"]+"\r\n"+obj["from"].substring(0, 5);
					label.textWrap = true;
					label.backgroundColor	= obj["color"];
					label.className = "hour-btn attachment-btn";
					 
					let val2 = obj["from"].split(":");
					let start = (parseInt(val2[0]));
					GridLayout.setRow(label, start);
					parent.addChild(label);
				} else {
					console.error("Something went wrong");
				}
			},
			error =>{console.error(error);});
		}
  }
}
