import {Component,Input,transition,
    ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { GestureTypes, SwipeGestureEventData } from "ui/gestures";
import { GridLayout, GridUnitType, ItemSpec} from "ui/layouts/grid-layout";
import { StackLayout } from "ui/layouts/stack-layout";
import { View } from "ui/core/view";
import { prompt } from "ui/dialogs";
import { Label } from "ui/label";
import { AnimationCurve } from "ui/enums";
import { Animation } from "ui/animation";
import { RouterExtensions } from "nativescript-angular";
@Component({
  selector: 'month-calendar',
  template: `
	<ActionBar title="">
		<Label text="January" #title></Label>
	</ActionBar>

    	<AbsoluteLayout  #container>
				<GridLayout columns="*,*,*,*,*,*,* auto" style="width:100%">
						<Label col="0" text="Mon" class="day"></Label>
						<Label col="1" text="Tue" class="day"></Label>
						<Label col="2" text="Wed" class="day"></Label>
						<Label col="3" text="Thu" class="day"></Label>
						<Label col="4" text="Fri" class="day"></Label>
						<Label col="5" text="Sat" class="day"></Label>
						<Label col="6" text="Sun" class="day"></Label>
					</GridLayout>
					<Label class="border" style="width:100%;margin-top:50px;"></Label>
			</AbsoluteLayout> 

    `, 

		styleUrls:["css/month.component.css"],
})
export class MonthComponent  { 
	public lastday:number; 
	public day:any;
	public month:number;
	public month2:string; 
	public text:any;
	public state:number = 0;
	public year:number;
  public cont:any;
	public cont2:any;
	public cont3:any;
	constructor(private routerExtension: RouterExtensions,private route: ActivatedRoute){}
  goBack(){  
    this.routerExtension.back()
  }
		@ViewChild("title") title: ElementRef;
    @ViewChild("container") container: ElementRef;
    ngOnInit(){
			this.cont = this.container.nativeElement;
			this.text = this.title.nativeElement;
			this.cont.on(GestureTypes.swipe,(args: SwipeGestureEventData)=>{
			console.log("Swipe Direction: " + args.direction);
				if(args.direction==2){
					this.Next();
				}
				if(args.direction==1){
					this.Prev();
				}
			});
			this.cont2 = new StackLayout();
			this.cont.addChild(this.cont2) 
			this.GetCurrentYear();
			this.route.params.forEach((params: Params) => {
		    this.month = +params['id'];
		  });
			this.day = this.month;
	    this.month -=1;
			if(isNaN(this.month))this.Create(0);
	    else this.Create(1);
    }
    GetCurrentYear(){
	    let date = new Date();
	    this.month = date.getMonth()+1;
	    this.year = date.getFullYear();
    }
    Days(){
	    this.lastday = 0;
	      let mon:number;
	    if(this.month <= 7){
	      if(this.month % 2 == 1) mon = 30;
	      else mon = 31;
	      if(this.month == 1) mon = 31;
	      if(this.month == 3){
	        if(((this.year % 4 == 0) && (this.year % 100 != 0)) 
	          || (this.year % 400 == 0))mon = 29;
	        else mon = 28;
	      } 
	    }
	    else{
	      if(this.month % 2 == 1) mon = 31;
	      else mon = 30;
	    }
	    let j=1;
	    let d = new Date(this.year,(this.month-1),1);
	    let day = d.getDay();
	    let today = d.getDate();
	    let dif = today % 7;
	    let days = new Array;
	    if( day - dif <0){
	      dif = day - dif + 8;
	    }
	    else dif = day-dif+1;
	    for (let i = 0; i != 7; i+=1) {
	      if(i < dif-1) days.push(mon-dif+i+2);
	      else {
	        days.push(j);
	        j+=1;
	      }
	    }
	    this.lastday = j-1;
	    return days;
    }
    Week(){
	    let mon:number;
	    if(this.month <= 7){
	      if(this.month % 2 == 1) mon = 31;
	      else mon = 30;
	      if(this.month == 2){
	        if(((this.year % 4 == 0) && (this.year % 100 != 0)) 
	          || (this.year % 400 == 0))mon = 29;
	        else mon = 28;
	      }
	    }
	    else{
	      if(this.month % 2 == 1) mon = 30;
	      else mon = 31;
	    }
	    let j = this.lastday;
	    let days = new Array;
	    for (var i = 0; i < 7;i+=1) {
	      if(j == mon) j = 0;
	      j +=1;
	      days.push(j)
	    }
	    this.lastday = j;
	    return days;
    }
    Create(n:any){
      this.cont2 = new StackLayout();
 			this.cont2.style.width = "100%";
			this.cont2.style.marginTop = "60px";
			this.cont2.style.backgroundColor = "white"
	    switch (n) {
		      case -1:{
		        if(this.month == 1){
		          this.month = 12;
		          this.year -=1;
		        }
		        else this.month-=1;
		        break;
		      }
		      case 0:{
		        this.GetCurrentYear();
		        break;
		      }
		      case 1:{
		        if(this.month == 12){
		          this.month = 1;
		          this.year +=1;
		        }
		        else this.month+=1;
		        break;
		    }
			}
	    var tr = this.Content(this.Days(),"day",1);
	    this.cont2.addChild(tr);
      let div = new Label();
      div.className="border";
      this.cont2.addChild(div);
	    for (var i=0; i < 4;i ++) {
	      tr = this.Content(this.Week(),"day",2);
	      this.cont2.addChild(tr);
        let div = new Label();
        div.className="border";
        this.cont2.addChild(div);
	    }
		tr = this.Content(this.Week(),"day",3);
	    this.cont2.addChild(tr);
			if(this.state == 1) this.cont2.translateX = -1000
			if(this.state == 2) this.cont2.translateX = 1000
	    this.cont.addChild(this.cont2);
			this.cont2.animate({
				translate: { x: 0, y: 0},    
				duration: 400,
				curve: AnimationCurve.easeInOut
			});
	    this.Month();
    }
		Content(week:any,clas:any,type:any){
			let td: any, check:number;
			let tr = new GridLayout();
            var secondRow = new ItemSpec(1, GridUnitType.auto);
            tr.addRow(secondRow);
			if(week[0]>week[6] && type==1) check=1;
			if(week[0]>week[6] && type==2) check=2;
			if(week[0]<week[6] && type==3) check=3; 
            let i=0;
			week.forEach((element:any) => {
          let secondColumn = new ItemSpec(1, GridUnitType.star);
          tr.addColumn(secondColumn);
          td = new Label();
          td.text = element;
          td.className = clas;
          if(check == 1 && element>7){
            td.className = clas +" inactive";
          }
          if(check == 2 && element<7){
            td.className = clas +" inactive";
          }
          if(check == 3){
            td.className = clas +" inactive";
          }
          GridLayout.setColumn(td, i);
          tr.addChild(td);
          i+=1;
			});
			return tr;
		}
	Prev(){
		this.state = 1;
		this.Create(-1); 
	}
	Next(){
		this.state = 2;
		this.Create(1);
	}
    Month(){
    	switch (this.month) {
            case 1: this.month2 = "January";break;
            case 2: this.month2 = "February";break;
            case 3: this.month2 = "March";break;
            case 4: this.month2 = "April";break;
            case 5: this.month2 = "May";break;
            case 6: this.month2 = "June";break;
            case 7: this.month2 = "July";break;
            case 8: this.month2 = "August";break;
            case 9: this.month2 = "September";break;
            case 10:this.month2 = "October";break;
            case 11:this.month2 = "November";break;
            case 12:this.month2 = "December";break;
        }
				setTimeout(()=>{
					this.text.text = this.month2
				},300)
    }
    
  
}
