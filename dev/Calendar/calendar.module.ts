import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptModule } from "nativescript-angular/platform";

import { DatePickerComponent } from "./datePicker.component";
import { ModalViewComponent } from "./modal.component";
import { YearComponent }   from './year.component';
import { MonthComponent }   from './month.component';
import { WeekComponent }   from './week.component';
import { CalendarService} from './calendar.service';
import { CalendarRoutingModule} from './calendar-routing.module';
@NgModule({
  imports: [
    NativeScriptModule,
    NativeScriptFormsModule,
    CalendarRoutingModule,
  ],
  declarations: [
    ModalViewComponent,
    DatePickerComponent,
	  YearComponent,
    MonthComponent,
    WeekComponent,
  ],
  providers: [
    CalendarService
  ],
  entryComponents: [
    ModalViewComponent,
    DatePickerComponent
  ],
})
export class CalendarModule {}